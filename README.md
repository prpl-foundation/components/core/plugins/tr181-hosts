# tr181-hosts
TR181 Hosts manager plugin

[[_TOC_]]

## Introduction
This component implements and manages the Device.Hosts. datamodel described in [here](https://usp-data-models.broadband-forum.org/tr-181-2-12-0-usp.html#D.Device:2.Device.Hosts.)


This object provides information about each of the hosts on the LAN, including those whose IP address was allocated by the CPE using DHCP as well as hosts with statically allocated IP addresses. It can also include non-IP hosts.

## Building, installing and testing

### Docker container

You could install all tools needed for testing and developing on your local machine, but it is easier to just use a pre-configured environment. Such an environment is already prepared for you as a docker container.

1. Install docker

    Docker must be installed on your system.

    If you have no clue how to do this here are some links that could help you:

    - [Get Docker Engine - Community for Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
    - [Get Docker Engine - Community for Debian](https://docs.docker.com/install/linux/docker-ce/debian/)
    - [Get Docker Engine - Community for Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/)
    - [Get Docker Engine - Community for CentOS](https://docs.docker.com/install/linux/docker-ce/centos/)  <br /><br />

    Make sure you user id is added to the docker group:

    ```
    sudo usermod -aG docker $USER
    ```

1. Fetch the container image

    To get access to the pre-configured environment, all you need to do is pull the image and launch a container.

    Pull the image:

    ```bash
    docker pull registry.gitlab.com/soft.at.home/docker/oss-dbg:latest
    ```

    Before launching the container, you should create a directory which will be shared between your local machine and the container.

    ```bash
    mkdir -p ~/amx
    ```

    Launch the container:

    ```bash
    docker run -ti -d --name oss-dbg --restart always --cap-add=SYS_PTRACE --sysctl net.ipv6.conf.all.disable_ipv6=1 -e "USER=$USER" -e "UID=$(id -u)" -e "GID=$(id -g)" -v ~/amx/:/home/$USER/amx/ registry.gitlab.com/soft.at.home/docker/oss-dbg:latest
    ```

    The `-v` option bind mounts the local directory for the ambiorix project in the container, at the exact same place.
    The `-e` options create environment variables in the container. These variables are used to create a user name with exactly the same user id and group id in the container as on your local host (user mapping).

    You can open as many terminals/consoles as you like:

    ```bash
    docker exec -ti --user $USER oss-dbg /bin/bash
    ```

### Building

#### Prerequisites

- [libamxc](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxc)
- [libamxd](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxd)
- [libamxp](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxp)
- [libamxb](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxb)
- [libamxo](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxo)
- [libamxs](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxs)
- [libmnl](https://gitlab.com/soft.at.home/network/libmnl)
- [libgmap-client](https://gitlab.com/prpl-foundation/components/gmap/libraries/libgmap-client)
- [libsahtrace](https://gitlab.com/soft.at.home/network/libsahtrace)

#### Build gmap-mod-ethernet-dev

1. Clone the git repository

    To be able to build it, you need the source code. So make a directory for the gMap project libraries and clone this library in it.

    ```bash
    mkdir -p ~/workspace/amx/plugins/
    cd ~/workspace/amx/plugins/
    git clone git@gitlab.com:prpl-foundation/components/core/plugins/tr181-hosts.git
    ```

1. Install dependencies

    Although the container will contain all tools needed for building, it does not contain the libraries needed for building `tr181-hosts`. To be able to build `tr181-hosts` you need `libamxc`, `libamxj`, `libamxd`, `libamxs`, `libamxb`, `libgmap-client` and `libsahtrace`. These libraries can be installed in the container.

    ```bash
    sudo apt update
    sudo apt install libamxb
    sudo apt install libamxc
    sudo apt install libamxd
    sudo apt install libamxo
    sudo apt install libamxs
    sudo apt install libgmap-client
    sudo apt install mod-dmext
    sudo apt install sah-lib-sahtrace-dev
    ```

1. Build it

   In the container:

    ```bash
    cd ~/workspace/amx/plugins/tr181-hosts
    make
    ```

### Installing

#### Using make target install

You can install your own compiled version easily in the container by running the install target.

```bash
cd ~/amx/plugins/tr181-hosts
sudo -E make install
```


### Testing

#### Prerequisites

No extra components are needed for testing `tr181-hosts`.

#### Run tests

You can run the tests by executing the following command.

```bash
cd ~/amx/plugins/tr181-hosts/test
make
```

Or this command if you also want the coverage tests to run:

```bash
cd ~/amx/plugins/tr181-hosts/test
make run && make coverage
```

#### Coverage reports

The coverage target will generate coverage reports using [gcov](https://gcc.gnu.org/onlinedocs/gcc/Gcov.html) and [gcovr](https://gcovr.com/en/stable/guide.html).

A summary for each file (*.c files) is printed in your console after the tests are run.
A HTML version of the coverage reports is also generated. These reports are available in the output directory of the compiler used.
Example: using native gcc
When the output of `gcc -dumpmachine` is `x86_64-linux-gnu`, the HTML coverage reports can be found at `~/amx/plugins/tr181-hosts/output/x86_64-linux-gnu/coverage/report.`

You can easily access the reports in your browser.
In the container start a python3 http server in background.

```bash
cd ~/amx/
python3 -m http.server 8080 &
```

Use the following url to access the reports `http://<IP ADDRESS OF YOUR CONTAINER>:8080/plugins/tr181-hosts/output/<MACHINE>/coverage/report`
You can find the ip address of your container by using the `ip` command in the container.

Example:

```bash
USER@<CID>:~/amx/plugins/tr181-hosts$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
173: eth0@if174: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:11:00:07 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.7/16 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 2001:db8:1::242:ac11:7/64 scope global nodad 
       valid_lft forever preferred_lft forever
    inet6 fe80::42:acff:fe11:7/64 scope link 
       valid_lft forever preferred_lft forever
```

in this case the ip address of the container is `172.17.0.7`.
So the uri you should use is: `http://172.17.0.7:8080/plugins/tr181-hosts/output/x86_64-linux-gnu/coverage/report/`

## Implementation

This service provides the data model as defined in [USP Device:2.16](https://usp-data-models.broadband-forum.org/tr-181-2-16-0-usp.html#D.Device:2.Device.Hosts.Host.).

Where the BBF specifications are not clear on the expected behavior, some implementation choises has been made.

In some cases the implementation is close to but not 100% compliant with the specification.

These are documented in the following topics.

### AccessControl Object

#### PhysAddress parameter

In the specification is stated:
> Unique physical identifier of the device. For many layer 2 technologies this is typically a MAC address.<br>
> If the value isn’t assigned by the Controller on creation, the Agent MUST choose an initial value that doesn’t conflict with any existing entries.

In this implementation it is expected that the value of this parameter is always a MAC address and is provided by the creator of the access control instance. If no `PhysAddress` value is given it will stay empty, no initial value is chosen.

#### AccessPolicy parameter

> If AccessPolicy is set to “Allow”, device access is allowed based on the Schedule objects. Access is enabled if there is no Schedule object defined.

As the specification is not clear about the meaning of "no Schedule object defined" a configuration option `ignore-disabled` is available that can be used to change the behavior. 

That sentence in the TR181 specification should be clarified, it can be interpreted as follows:

1. When no schedule instances are available (either enabled or disabled) the device is allowed access to the internet.
1. When no enabled instances are available the device is allowed access to the internet.

In the implementation option 1 is chosen as the default behavior. When schedule instances are available but all of them are disabled, the device will not be able to access the internet.

When `ignore-disabled` is defined and set to `true` the disabled schedules will be ignored and the device will be allowed access to the internet when all defined schedules are disabled.

When access policy is set to "Deny" the device will always be blocked, regardless of the defined schedules, as is specified in TR181:
> If AccessPolicy is set to “Deny”, defined Schedule objects are ignored and no access is allowed for the device.

The following table gives an overview of the possible combinations:

|                                   | **AccessPolicy** | **ignore-disabled** |                           **Result**                           |
|-----------------------------------|:----------------:|:-------------------:|:--------------------------------------------------------------:|
|       No schedules available      |       Deny       |  no/yes/not defined | **Device is blocked**                                          |
|    Enabled schedules available    |       Deny       |  no/yes/not defined | **Device is blocked**                                          |
| Only disabled schedules available |       Deny       |  no/yes/not defined | **Device is blocked**                                          |
|       No schedules available      |       Allow      |  no/yes/not defined | **Device is allowed**                                          |
|    Enabled schedules available    |       Allow      |  no/yes/not defined | **Device is allowed according to defined enabled schedule(s)** |
| Only disabled schedules available |       Allow      |    no/not defined   | **Device is blocked**                                          |
| Only disabled schedules available |       Allow      |         yes         | **Device is allowed**                                          |


#### Enable parameter

When the access control instance is disabled (parameter `Enable` set to false), the device will be allowed to access the internet.

When the access control instance is enabled (parameter `Enable` set to true), the device will be allowed to access the internet according to the `AccessPolicy` parameter and the defined schedules.

### Schedule Object

The schedule object definition is mostly clear in the TR181 specification, except when parameter `StartTime` is set to an empty string or when overlapping schedules are defined

#### Empty StartTime

> If the StartTime is not defined, duration is the total time access is allowed during a calendar day.

When start time is an empty string the device will be allowed to access the internet for a maximum duration defined in parameter `Duration` for the day(s) defined in parameter `Day`.

The implementation is best effort estimate. If a schedule with an empty start time becomes active a timer is started when the device itself is detected on the network (see parameter 'Hosts.Host.{i}.Active). As long as the device is active and the timer is not expired the device will be allowed to access the internet. When the device is detected as inactive, the timer will be paused. 

This implementation is best effort and depends on the device active detection. Mostly a device is considered active when it is detected on the local area network.

When a schedule exists with an empty start time for a device on a day, all other schedules for that day are ignored.

Only one schedule with an empty start time can exists for a certain day.

#### Overlapping schedules

When overlapping schedules exist for a certain device, the union of these are taken.

Example:

Consider these schedules:
- Wednesday from 14:00 with a duration of 7200 seconds (2 hours)
- Wednesday,Saterday,Sunday from 15:00 with a duration of 7200 seconds (2 hours)

For Wednesday an overlap exists, from 14:00 - 16:00 overlaps with 15:00 - 17:00. So on Wednesday the device will be able to access the internet from 14:00 until 17:00 that is 3 hours (10800 seconds).




