/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mod_scheduler_amxp.h"

#define ME "scheduler-amxp"

// keep one schedule for each host.
// Use physaddress as key
static amxc_htable_t ac_schedules;

// Callback for amxc_htable_clean. Removes a schedule for a specific host
static void scheduler_remove_schedule(UNUSED const char* key, amxc_htable_it_t* it) {
    ac_schedule_t* ac_schedule = amxc_container_of(it, ac_schedule_t, it);
    amxp_scheduler_clean(&ac_schedule->scheduler);
    free(ac_schedule);
}

static ac_schedule_t* scheduler_create(const char* id) {
    ac_schedule_t* ac_schedule = NULL;

    // first check if a scheduler already exists with this id
    // if available use it, otherwise create a new one
    ac_schedule = scheduler_fetch(id);
    if(ac_schedule == NULL) {
        ac_schedule = (ac_schedule_t*) calloc(1, sizeof(ac_schedule_t));
        when_null(ac_schedule, exit);
        amxp_scheduler_init(&ac_schedule->scheduler);
        amxp_scheduler_use_local_time(&ac_schedule->scheduler, true);
        amxc_htable_insert(&ac_schedules, id, &ac_schedule->it);
    }

exit:
    return ac_schedule;
}

static void callback_enable_disable_address(const char* const sig_name,
                                            const amxc_var_t* const data,
                                            void* const priv) {
    (void) data; // When tracing is not enabled data is not used.
    ac_schedule_t* ac_schedule = (ac_schedule_t*) priv;
    const char* ac_id = amxc_htable_it_get_key(&ac_schedule->it);
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    when_null_trace(priv, exit, ERROR, "No priv data found");

    // only pass the ac id (physaddress) and schedule id back, all other information is available in the data model
    amxc_var_add_key(cstring_t, &args, "AccessControlId", ac_id);
    amxc_var_add_key(cstring_t, &args, "ScheduleId", GET_CHAR(data, "id"));

    SAH_TRACEZ_INFO(ME, "Callback called with id: %s  for address: %s - reason = %s\n", GET_CHAR(data, "id"), ac_id, sig_name);

    if(strncmp(sig_name, "start:", 6) == 0) {
        // When there are overlapping schedule items (in time) multiple start events
        // will be received, only activate when the first is triggered
        // The active counter keeps track of how many times it is started
        ac_schedule->active++;
        if(ac_schedule->active == 1) {
            // transition from not active to active
            if(amxm_execute_function(NULL, MOD_HOST_SCHEDULER, "ac-start-interval", &args, &ret) != 0) {
                SAH_TRACE_ERROR("Hosts accesscontrol -> start interval failed for %s", ac_id);
            }
        }
    } else {
        // When there are overlapping schedule items (in time) multiple stop evens
        // will be received, only de-active when the last is stopped.
        ac_schedule->active--;

        if(ac_schedule->active == 0) {
            // transition from active to not active
            if(amxm_execute_function(NULL, MOD_HOST_SCHEDULER, "ac-end-interval", &args, &ret) != 0) {
                SAH_TRACE_ERROR("Hosts accesscontrol -> end interval failed for %s", ac_id);
            }
        }
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

ac_schedule_t* scheduler_fetch(const char* id) {
    amxc_htable_it_t* it = amxc_htable_get(&ac_schedules, id);
    ac_schedule_t* ac_schedule = NULL;

    when_null(it, exit);
    ac_schedule = amxc_container_of(it, ac_schedule_t, it);

exit:
    return ac_schedule;
}

int scheduler_init_amxp(UNUSED const char* function_name,
                        UNUSED amxc_var_t* args,
                        UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_INFO(ME, "scheduler_init_amxp");
    // nothing to do here.
    // - htable initialization can be done here, but is already done in constructor function

    return 0;
}

int scheduler_clean_amxp(UNUSED const char* function_name,
                         UNUSED amxc_var_t* args,
                         UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_INFO(ME, "scheduler_clean_amxp");
    // nothing to do here
    // - htable cleanup can be done here, but curently is done in desctructor function

    return 0;
}

int scheduler_reset_amxp(UNUSED const char* function_name,
                         UNUSED amxc_var_t* args,
                         UNUSED amxc_var_t* ret) {
    // when resetting a scheduler, one key is needed:
    // 1. the access controle key (is the access control physaddress)
    int rv = -1;
    const char* ac_id = GET_CHAR(args, "AccessControlId");
    ac_schedule_t* ac_schedule = NULL;

    when_str_empty_trace(ac_id, exit, ERROR, "Missing argument 'AccessControlId'");

    ac_schedule = scheduler_fetch(ac_id);
    rv = 0; // if no scheduler found, nothing to remove -> success
    when_null_trace(ac_schedule, exit, WARNING, "Failed to fetch scheduler for '%s', maybe no schedules available", ac_id);

    // remove the scheduler with this id from the table
    amxc_htable_it_clean(&ac_schedule->it, scheduler_remove_schedule);
    rv = 0;

exit:
    return rv;
}

int scheduler_check_schedule_amxp(UNUSED const char* function_name,
                                  amxc_var_t* args, amxc_var_t* ret) {
    int rv = -1;
    const char* ac_id = GET_CHAR(args, "AccessControlId");
    ac_schedule_t* ac_schedule = NULL;

    amxc_var_set(bool, ret, true);
    when_str_empty_trace(ac_id, exit, ERROR, "Missing argument 'AccessControlId'");

    ac_schedule = scheduler_fetch(ac_id);
    rv = 0;
    when_null_trace(ac_schedule, exit, WARNING, "Failed to fetch scheduler for '%s', maybe no schedules available", ac_id);
    if(ac_schedule->active <= 0) {
        amxc_var_set(bool, ret, false);
    }

exit:
    return rv;
}

int scheduler_add_or_change_schedule_amxp(const char* function_name,
                                          amxc_var_t* args,
                                          UNUSED amxc_var_t* ret) {
    // when adding a schedule, one key is mandatory, the other can be optional:
    // 1. the access controle key (is the access control alias) => mandatory
    // 2. the schedule item key - this is the  access control schedule alias => optional
    int rv = -1;
    const char* ac_id = GET_CHAR(args, "AccessControlId");
    const char* schedule_id = GETP_CHAR(args, "schedule.ScheduleId");
    ac_schedule_t* ac_schedule = NULL;

    when_str_empty_trace(ac_id, exit, ERROR, "Missing argument 'AccessControlId'");

    // No need to check additional arguments - it is valid to create an invalid schedule
    ac_schedule = scheduler_create(ac_id);
    when_null_trace(ac_schedule, exit, ERROR, "Failed to fetch or create scheduler for '%s'", ac_id);
    if((schedule_id != NULL) && (*schedule_id != 0)) {
        const char* start_time = GETP_CHAR(args, "schedule.StartTime");
        uint32_t duration = GETP_UINT32(args, "schedule.Duration");
        if((start_time == NULL) || (*start_time == 0)) {
            // empty start time given
            // take the full day starting from 00:00:00 until 23:59:59
            start_time = "00:00:00";
            duration = 23 * 3600 + 59 * 60 + 59;
        }
        rv = amxp_scheduler_set_weekly_item(&ac_schedule->scheduler,
                                            schedule_id,
                                            start_time,
                                            GETP_CHAR(args, "schedule.Day"),
                                            duration);
        amxp_scheduler_enable_item(&ac_schedule->scheduler, schedule_id, GETP_BOOL(args, "schedule.Enable"));
        if(strcmp(function_name, "scheduler-add-schedule") == 0) {
            amxp_scheduler_connect(&ac_schedule->scheduler, schedule_id, callback_enable_disable_address, ac_schedule);
        }
    } else {
        rv = 0;
    }

    // make sure the scheduler is in the correct state (enabled or disabled)
    // the scheduler will be disabled if the "Enable" parameter is false.
    // The "Enable" parameter will be set to false when:
    // - The access control is disabled
    // - The access policy is set to "Deny"
    amxp_scheduler_enable(&ac_schedule->scheduler, GET_BOOL(args, "Enable"));
    if(!GET_BOOL(args, "Enable")) {
        ac_schedule->active = 0;
    }

exit:
    return rv;
}

int scheduler_remove_schedule_amxp(UNUSED const char* function_name,
                                   amxc_var_t* args,
                                   UNUSED amxc_var_t* ret) {
    // when adding or updating a schedule, two keys are needed:
    // 1. the access controle key (is the access control physaddress)
    // 2. the schedule item key - this is the  access control schedule alias
    int rv = -1;
    const char* ac_id = GET_CHAR(args, "AccessControlId");
    const char* schedule_id = GETP_CHAR(args, "schedule.ScheduleId");
    ac_schedule_t* ac_schedule = NULL;

    when_str_empty_trace(ac_id, exit, ERROR, "Missing argument 'AccessControlId'");
    when_str_empty_trace(schedule_id, exit, ERROR, "Missing argument 'ScheduleId'");

    ac_schedule = scheduler_fetch(ac_id);
    when_null_trace(ac_schedule, exit, ERROR, "Failed to fetch scheduler for '%s'", ac_id);

    rv = amxp_scheduler_remove_item(&ac_schedule->scheduler, schedule_id);

exit:
    return rv;
}

int scheduler_recalculate_schedule_amxp(UNUSED const char* function_name,
                                        UNUSED amxc_var_t* args,
                                        UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_INFO(ME, "Recalculating schedules");
    int rv = 0;
    amxc_htable_for_each(it, &ac_schedules) {
        ac_schedule_t* ac_schedule = amxc_htable_it_get_data(it, ac_schedule_t, it);
        SAH_TRACEZ_INFO(ME, "Recalculate schedules for %s", amxc_htable_it_get_key(it));
        rv = amxp_scheduler_update(&ac_schedule->scheduler);
        if(rv != 0) {
            break;
        }
    }
    return rv;
}

static AMXM_CONSTRUCTOR scheduler_amxp_start(void) {
    amxm_module_t* mod = NULL;
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_register(&mod, so, MOD_SCHEDULER_CTRL);
    amxm_module_add_function(mod, "scheduler-init", scheduler_init_amxp);
    amxm_module_add_function(mod, "scheduler-clean", scheduler_clean_amxp);
    amxm_module_add_function(mod, "scheduler-reset", scheduler_reset_amxp);
    amxm_module_add_function(mod, "scheduler-add-schedule", scheduler_add_or_change_schedule_amxp);
    amxm_module_add_function(mod, "scheduler-change-schedule", scheduler_add_or_change_schedule_amxp);
    amxm_module_add_function(mod, "scheduler-remove-schedule", scheduler_remove_schedule_amxp);
    amxm_module_add_function(mod, "scheduler-check-schedule", scheduler_check_schedule_amxp);
    amxm_module_add_function(mod, "scheduler-recalculate-schedule", scheduler_recalculate_schedule_amxp);

    // initialize htable with schedules - start with pre-allocating 10 buckets
    // if more are needed the htable will grow
    amxc_htable_init(&ac_schedules, 10);

    return 0;
}


static AMXM_DESTRUCTOR scheduler_amxp_stop(void) {
    // The shared object is unloaded, so remove and free all stored schedulers
    amxc_htable_clean(&ac_schedules, scheduler_remove_schedule);

    return 0;
}
