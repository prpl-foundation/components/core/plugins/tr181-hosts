/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_module.h"
#include "../../include_priv/mod_scheduler_amxp.h"

#define MOD_NAME "target_module"
#define MOD_PATH "../modules_under_test/" MOD_NAME ".so"

static int callback_start_triggered = 0;
static int callback_end_triggered = 0;

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

static int dummy_func_start(UNUSED const char* function_name,
                            UNUSED UNUSED amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    int rv = 0;
    const char* phys_address = GET_CHAR(args, "PhysAddress");
    printf("Start dummy entered, Physical Address passed: %s\n", phys_address);
    callback_start_triggered++;

    return rv;
}

static int dummy_func_end(UNUSED const char* function_name,
                          UNUSED UNUSED amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    int rv = 0;
    const char* phys_address = GET_CHAR(args, "PhysAddress");
    printf("End dummy entered, Physical Address passed: %s\n", phys_address);
    callback_end_triggered++;

    return rv;
}

int test_setup(UNUSED void** state) {
    amxm_module_t* mod = NULL;
    int rv = -1;

    sahTraceOpen("test", TRACE_TYPE_STDERR);
    sahTraceSetLevel(TRACE_LEVEL_ERROR);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);

    // Load dummy modules
    rv = amxm_module_register(&mod, NULL, "host-scheduler");
    rv |= amxm_module_add_function(mod, "ac-start-interval", dummy_func_start);
    rv |= amxm_module_add_function(mod, "ac-end-interval", dummy_func_end);

    handle_events();
    return rv;
}

int test_teardown(UNUSED void** state) {
    sahTraceClose();
    return 0;
}

void test_can_load_module(UNUSED void** state) {
    amxm_shared_object_t* so = NULL;
    assert_int_equal(amxm_so_open(&so, "target_module", "../modules_under_test/target_module.so"), 0);
}

void test_has_functions(UNUSED void** state) {
    assert_true(amxm_has_function(MOD_NAME, MOD_SCHEDULER_CTRL, "scheduler-init"));
    assert_true(amxm_has_function(MOD_NAME, MOD_SCHEDULER_CTRL, "scheduler-clean"));
    assert_true(amxm_has_function(MOD_NAME, MOD_SCHEDULER_CTRL, "scheduler-reset"));
    assert_true(amxm_has_function(MOD_NAME, MOD_SCHEDULER_CTRL, "scheduler-add-schedule"));
    assert_true(amxm_has_function(MOD_NAME, MOD_SCHEDULER_CTRL, "scheduler-change-schedule"));
    assert_true(amxm_has_function(MOD_NAME, MOD_SCHEDULER_CTRL, "scheduler-remove-schedule"));
}

void test_can_close_module(UNUSED void** state) {
    assert_int_equal(amxm_close_all(), 0);
}

void test_can_init(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);

    assert_int_equal(scheduler_init_amxp("scheduler-init", &var, &ret), 0);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_can_clean(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);

    assert_int_equal(scheduler_clean_amxp("scheduler-clean", &var, &ret), 0);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_can_add_schedule_item(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t* schedule = NULL;
    amxc_var_t ret;
    ac_schedule_t* s = NULL;

    amxc_var_init(&var);
    amxc_var_init(&ret);
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &var, "AccessControlId", "test");
    amxc_var_add_key(bool, &var, "Enable", true);

    schedule = amxc_var_add_key(amxc_htable_t, &var, "schedule", NULL);
    amxc_var_add_key(cstring_t, schedule, "ScheduleId", "schedule1");
    amxc_var_add_key(cstring_t, schedule, "StartTime", "17:10");
    amxc_var_add_key(cstring_t, schedule, "Day", "Monday");
    amxc_var_add_key(uint32_t, schedule, "Duration", 60);

    assert_int_equal(scheduler_add_or_change_schedule_amxp("scheduler-add-schedule", &var, &ret), 0);

    s = scheduler_fetch("test");
    assert_non_null(s);
    assert_non_null(amxc_htable_get(&s->scheduler.items, "schedule1"));

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_can_disable_scheduler(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t ret;
    ac_schedule_t* s = NULL;

    amxc_var_init(&var);
    amxc_var_init(&ret);
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &var, "AccessControlId", "test");
    amxc_var_add_key(bool, &var, "Enable", false);

    assert_int_equal(scheduler_add_or_change_schedule_amxp("scheduler-add-schedule", &var, &ret), 0);

    s = scheduler_fetch("test");
    assert_non_null(s);
    assert_false(s->scheduler.sigmngr.enabled);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_can_enable_scheduler(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t ret;
    ac_schedule_t* s = NULL;

    amxc_var_init(&var);
    amxc_var_init(&ret);
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &var, "AccessControlId", "test");
    amxc_var_add_key(bool, &var, "Enable", true);

    assert_int_equal(scheduler_add_or_change_schedule_amxp("scheduler-add-schedule", &var, &ret), 0);
    s = scheduler_fetch("test");
    assert_non_null(s);
    assert_true(s->scheduler.sigmngr.enabled);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_add_schedule_item_with_invalid_values_fails(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t* schedule = NULL;
    amxc_var_t ret;
    ac_schedule_t* s = NULL;

    amxc_var_init(&var);
    amxc_var_init(&ret);
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &var, "AccessControlId", "test");
    amxc_var_add_key(bool, &var, "Enable", true);

    schedule = amxc_var_add_key(amxc_htable_t, &var, "schedule", NULL);
    amxc_var_add_key(cstring_t, schedule, "ScheduleId", "schedule2");
    assert_int_not_equal(scheduler_add_or_change_schedule_amxp("scheduler-add-schedule", &var, &ret), 0);
    s = scheduler_fetch("test");
    assert_null(amxc_htable_get(&s->scheduler.items, "schedule2"));

    amxc_var_add_key(uint32_t, schedule, "Duration", 60);
    assert_int_not_equal(scheduler_add_or_change_schedule_amxp("scheduler-add-schedule", &var, &ret), 0);
    assert_null(amxc_htable_get(&s->scheduler.items, "schedule2"));

    amxc_var_add_key(cstring_t, schedule, "Day", "Monday");
    assert_int_equal(scheduler_add_or_change_schedule_amxp("scheduler-add-schedule", &var, &ret), 0);
    assert_non_null(amxc_htable_get(&s->scheduler.items, "schedule2"));

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_can_change_schedule_item(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t* schedule = NULL;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &var, "AccessControlId", "test");
    amxc_var_add_key(bool, &var, "Enable", true);

    schedule = amxc_var_add_key(amxc_htable_t, &var, "schedule", NULL);
    amxc_var_add_key(cstring_t, schedule, "ScheduleId", "schedule1");
    amxc_var_add_key(cstring_t, schedule, "StartTime", "15:10");
    amxc_var_add_key(cstring_t, schedule, "Day", "Wednesday,Saturday");
    amxc_var_add_key(uint32_t, schedule, "Duration", 3600);

    assert_int_equal(scheduler_add_or_change_schedule_amxp("scheduler-add-schedule", &var, &ret), 0);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_changing_not_existing_schedule_item_adds_new(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t* schedule = NULL;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &var, "AccessControlId", "test");
    amxc_var_add_key(bool, &var, "Enable", true);

    schedule = amxc_var_add_key(amxc_htable_t, &var, "schedule", NULL);
    amxc_var_add_key(cstring_t, schedule, "ScheduleId", "schedule3");
    amxc_var_add_key(cstring_t, schedule, "StartTime", "15:10");
    amxc_var_add_key(cstring_t, schedule, "Day", "Wednesday,Saturday");
    amxc_var_add_key(uint32_t, schedule, "Duration", 3600);

    assert_int_equal(scheduler_add_or_change_schedule_amxp("scheduler-add-schedule", &var, &ret), 0);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_can_remove_schedule_item(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t* schedule = NULL;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &var, "AccessControlId", "test");
    amxc_var_add_key(bool, &var, "Enable", true);

    schedule = amxc_var_add_key(amxc_htable_t, &var, "schedule", NULL);
    amxc_var_add_key(cstring_t, schedule, "ScheduleId", "schedule1");

    assert_int_equal(scheduler_remove_schedule_amxp("scheduler-remove-schedule", &var, &ret), 0);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_remove_schedule_item_with_invalid_values_fails(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t* schedule = NULL;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);

    assert_int_not_equal(scheduler_remove_schedule_amxp("scheduler-remove-schedule", &var, &ret), 0);

    amxc_var_add_key(cstring_t, &var, "AccessControlId", "test");
    assert_int_not_equal(scheduler_remove_schedule_amxp("scheduler-remove-schedule", &var, &ret), 0);

    schedule = amxc_var_add_key(amxc_htable_t, &var, "schedule", NULL);
    amxc_var_add_key(cstring_t, schedule, "ScheduleId", "schedule2");
    assert_int_equal(scheduler_remove_schedule_amxp("scheduler-remove-schedule", &var, &ret), 0);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_can_remove_not_exising_schedule_item(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t* schedule = NULL;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &var, "AccessControlId", "test");
    schedule = amxc_var_add_key(amxc_htable_t, &var, "schedule", NULL);
    amxc_var_add_key(cstring_t, schedule, "ScheduleId", "NotExisting");
    assert_int_equal(scheduler_remove_schedule_amxp("scheduler-remove-schedule", &var, &ret), 0);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_can_reset_scheduler(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);

    amxc_var_add_new_key_cstring_t(&var, "AccessControlId", "test");
    assert_int_equal(scheduler_reset_amxp("scheduler-reset", &var, &ret), 0);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_reset_scheduler_with_missing_argument_fails(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);

    assert_int_not_equal(scheduler_reset_amxp("scheduler-reset", &var, &ret), 0);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}
