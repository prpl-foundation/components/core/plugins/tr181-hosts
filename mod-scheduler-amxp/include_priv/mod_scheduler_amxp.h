/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__MOD_SCHEDULER_AMXP_H__)
#define __MOD_SCHEDULER_AMXP_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <amxc/amxc_macros.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxp/amxp_scheduler.h>
#include <amxm/amxm.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

// module names
#define MOD_SCHEDULER_CTRL "scheduler-ctrl"
#define MOD_HOST_SCHEDULER "host-scheduler"

typedef struct _ac_schedules {
    amxc_htable_it_t it;
    amxp_scheduler_t scheduler;
    // This counter keeps track of the number of times the schedule is activated
    // When having overlapping schedule items (in time), it should only trigger
    // activation on the first and de-activation on the last.
    // This counter will cause that overlapping schedules will act as an union
    uint32_t active;
} ac_schedule_t;

PRIVATE
ac_schedule_t* scheduler_fetch(const char* id);

PRIVATE
int scheduler_init_amxp(const char* function_name,
                        amxc_var_t* args,
                        amxc_var_t* ret);

PRIVATE
int scheduler_clean_amxp(const char* function_name,
                         amxc_var_t* args,
                         amxc_var_t* ret);

PRIVATE
int scheduler_reset_amxp(const char* function_name,
                         amxc_var_t* args,
                         amxc_var_t* ret);

PRIVATE
int scheduler_add_or_change_schedule_amxp(const char* function_name,
                                          amxc_var_t* args,
                                          amxc_var_t* ret);

PRIVATE
int scheduler_remove_schedule_amxp(const char* function_name,
                                   amxc_var_t* args,
                                   amxc_var_t* ret);

PRIVATE
int scheduler_check_schedule_amxp(const char* function_name,
                                  amxc_var_t* args,
                                  amxc_var_t* ret);

PRIVATE
int scheduler_recalculate_schedule_amxp(const char* function_name,
                                        amxc_var_t* args,
                                        amxc_var_t* ret);

#ifdef __cplusplus
}
#endif

#endif // __MOD_SCHEDULER_AMXP_H__
