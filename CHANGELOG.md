# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.9.11 - 2024-12-11(15:24:24 +0000)

### Other

- [TR181-Hosts][AccessControl] When the Time/date changes, the access schedules must be recalculated.

## Release v0.9.10 - 2024-11-08(12:44:51 +0000)

### Other

- Optimize gmap application query events

## Release v0.9.9 - 2024-11-07(16:29:57 +0000)

### Other

- Alternative devices in gmap cause duplicated hosts

## Release v0.9.8 - 2024-11-07(15:25:22 +0000)

### Other

- Make Host.IPv[46]Address read-only

## Release v0.9.7 - 2024-10-10(15:06:35 +0000)

### Other

- IPv4Address and IPv6Address instance not populated

## Release v0.9.6 - 2024-09-30(07:53:31 +0000)

### Other

- - [Terminating dot][tr181] fix Hosts

## Release v0.9.5 - 2024-09-19(16:55:24 +0000)

### Other

- Bandwidth Monitoring upload and download data is displaying all zeros for the connected devices in LAN

## Release v0.9.4 - 2024-09-16(15:02:41 +0000)

### Other

- - Hosts.Hosts instance is not deleted after Devices.destroyDevice

## Release v0.9.3 - 2024-09-03(07:36:51 +0000)

### Other

- - TR-181: Device.Hosts data model issues 19.03.2024

## Release v0.9.2 - 2024-09-02(07:00:40 +0000)

### Other

- - TR-181: Device.Hosts data model issues 19.03.2024

## Release v0.9.1 - 2024-07-23(07:37:15 +0000)

### Fixes

- Better shutdown script

## Release v0.9.0 - 2024-07-05(10:05:10 +0000)

### New

- amx plugin should not run as root user

## Release v0.8.2 - 2024-06-13(18:53:59 +0000)

### Fixes

- [tr181-hosts] Create the Alias parameter in Hosts.Host.{i}

## Release v0.8.1 - 2024-06-12(15:09:33 +0000)

### Other

- [Hosts] Expose bandwidth statistics in the hosts data model

## Release v0.8.0 - 2024-05-17(20:44:29 +0000)

### New

-  [tr181-hosts] Add AccessControl ScheduleRef

## Release v0.7.5 - 2024-05-10(06:54:50 +0000)

### Fixes

- [Hosts-manager] Disabling should behave the same as removing

## Release v0.7.4 - 2024-04-25(11:02:32 +0000)

### Fixes

- [tr181-hosts] DeviceName, DeviceLocation, and DeviceMobility configurations are lost after reboot.

## Release v0.7.3 - 2024-04-11(07:56:29 +0000)

### Other

- [tr181-hosts] DeviceName, DeviceLocation, and DeviceMobility configurations are lost after reboot.

## Release v0.7.2 - 2024-04-10(09:59:23 +0000)

### Changes

- Make amxb timeouts configurable

## Release v0.7.1 - 2024-03-27(10:24:24 +0000)

### Fixes

- Require Devices.Config.global. instead of Devices.

## Release v0.7.0 - 2024-03-23(13:16:46 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.6.1 - 2024-03-20(21:02:04 +0000)

### Other

- [tr181-hosts] Systematic Crash in hosts-manager when invoking RemoveInactiveDevices() Function

## Release v0.6.0 - 2024-03-12(14:25:48 +0000)

### New

- [TR181-Hosts] Add more configuration parameters for Hosts instance cleanup

## Release v0.5.0 - 2024-03-01(15:56:17 +0000)

### New

- [tr181-Hosts] Provide DM function to clean up inactive Host instances - Device.Hosts part

## Release v0.4.5 - 2024-02-26(11:17:41 +0000)

### Fixes

- [TR181-Hosts] Update AssociatedDevice, Layer1Interface and Layer3Interface correctly

## Release v0.4.4 - 2024-02-08(15:41:22 +0000)

### Fixes

- [Hosts-manager] Schedulers don't seem to work properly after an AccessPolicy change

## Release v0.4.3 - 2024-02-01(10:38:32 +0000)

### Fixes

- [Hosts-manager] Removing access control doesn't remove the flag

## Release v0.4.2 - 2024-01-24(09:31:27 +0000)

### Changes

- - [ACL] Admin cannot delete schedules to Device.Hosts.AccessControl.*.Schedule.

## Release v0.4.1 - 2024-01-12(13:54:50 +0000)

### Fixes

- [Host Access Control] Allow a number of mac address (MAC + MASK) to be blocked for internet access

## Release v0.4.0 - 2024-01-11(09:58:33 +0000)

### New

- [Host Access Control] Allow a number of mac address (MAC + MASK) to be blocked for internet access

## Release v0.3.3 - 2023-11-17(14:52:34 +0000)

### Changes

- Hosts Manager sometimes does not start

## Release v0.3.2 - 2023-11-08(08:19:08 +0000)

### Fixes

- Device.Hosts.Host.{i}.DHCPClient parameter is having invalid DHCP path

## Release v0.3.1 - 2023-10-28(07:15:09 +0000)

### Changes

- CLONE - Include extensions directory in ODL

## Release v0.3.0 - 2023-10-16(10:27:37 +0000)

### New

- [amxrt][no-root-user][capability drop] All amx plugins must be adapted to run as non-root and lmited capabilities

## Release v0.2.4 - 2023-10-13(13:49:42 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v0.2.3 - 2023-09-29(12:12:05 +0000)

### Other

- [TR181-Hosts] Remove RPC methods

## Release v0.2.2 - 2023-09-15(09:05:37 +0000)

### Other

- - [ACL] Admin cannot access to Device.Hosts.AccessControl

## Release v0.2.1 - 2023-09-15(06:50:36 +0000)

### Fixes

- [tr181-pcm] All the plugins that are register to PCM should set the pcm_svc_config

### Other

- [TR181-Hosts] Integrate empty string condition as StartTime

## Release v0.2.0 - 2023-09-07(10:15:09 +0000)

### New

- [TR181-Hosts] Access Denied: Device Blocked Instead of Allowing Access at Scheduled Time

## Release v0.1.23 - 2023-08-04(08:21:18 +0000)

### Fixes

- Tag Remains in Device Tags after Deleting Denying Access Policy

## Release v0.1.22 - 2023-07-10(14:30:54 +0000)

### Changes

- [TR181-Hosts] Remove obsolete crontab implementation files

## Release v0.1.21 - 2023-07-10(09:08:11 +0000)

### Changes

- [TR181-Hosts] Use new amx scheduler library

## Release v0.1.20 - 2023-04-04(12:23:55 +0000)

### Fixes

- Hosts-manager does not update its AccessControl policy when the mac address is updated

## Release v0.1.19 - 2023-03-20(11:27:37 +0000)

### Fixes

- Removing all the firewall system commands and avoid redundancy and race conditions

## Release v0.1.18 - 2023-03-09(09:17:59 +0000)

### Other

- [Config] enable configurable coredump generation

## Release v0.1.17 - 2023-02-27(15:44:19 +0000)

### Fixes

- Change the implementation of WANAccess blocking

## Release v0.1.16 - 2023-01-12(10:43:42 +0000)

### Fixes

- Do not stop hosts manager startup if iptables flush fails

## Release v0.1.15 - 2023-01-06(12:38:20 +0000)

### Other

- Add missing runtime dependencies on gmap

## Release v0.1.14 - 2022-12-16(14:02:39 +0000)

### Other

- Opensource component

## Release v0.1.13 - 2022-12-13(08:44:54 +0000)

### Fixes

- [Hosts] Crontab used outdated functions

## Release v0.1.12 - 2022-12-09(09:29:20 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v0.1.11 - 2022-12-06(10:45:27 +0000)

### Fixes

- Update HostName and DHCP Client sync

## Release v0.1.10 - 2022-12-05(12:38:55 +0000)

### Changes

- [Hosts] Implement missing Device.Hosts.Host. parameters.

## Release v0.1.9 - 2022-11-15(18:21:30 +0000)

### Changes

- [Hosts] AccessControl block hosts directly in ip6tables

## Release v0.1.8 - 2022-11-15(12:21:41 +0000)

### Changes

- [Hosts] AccessControl block hosts directly in iptables

## Release v0.1.7 - 2022-11-07(12:22:56 +0000)

### Other

- [Hosts] Add support for AccessControl to Device.Hosts.(AccessControl) data model. [New]

## Release v0.1.6 - 2022-10-13(08:31:45 +0000)

### Other

- Improve plugin boot order

## Release v0.1.5 - 2022-09-20(14:09:55 +0000)

### Other

- [Hosts] Add Hosts plugin which supports the Device.Hosts. data model. [New]

## Release v0.1.4 - 2022-06-16(10:11:26 +0000)

### Other

- [amxrt] All amx plugins should start with the -D option

## Release v0.1.3 - 2022-03-24(11:00:08 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v0.1.2 - 2022-03-15(14:02:07 +0000)

### Other

- error in hosts start up script

## Release v0.1.1 - 2022-02-25(11:49:08 +0000)

### Other

- Enable core dumps by default

