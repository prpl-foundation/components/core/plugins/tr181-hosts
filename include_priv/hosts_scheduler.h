/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __HOSTS_SCHEDULER_H__
#define __HOSTS_SCHEDULER_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxp/amxp.h>
#include <amxo/amxo.h>

// The time tracker is used to know when a device should be blocked
// Devices with a schedule with empty start time and a duration will
// indicate how much time the device can access the wan during that day.
typedef struct __timer_tracker {
    uint32_t remaining_time; // in seconds - will be set to duration at start of the day (00:00)
    char* schedule_id;       // the id of the schedule containing the flex time definition
    amxp_timer_t* timer;     // When device is active, the timer is started, when inactive it is stopped
} time_tracker_t;

PRIVATE
int hosts_scheduler_init(amxd_dm_t* dm, amxo_parser_t* parser);

PRIVATE
int hosts_scheduler_clean(void);

PRIVATE
int hosts_scheduler_add_all_ac_schedules(amxd_object_t* accesscontrol_object);

PRIVATE
int hosts_scheduler_remove_all_ac_schedules(amxd_object_t* ac_obj);

PRIVATE
int hosts_scheduler_update_ac_schedule(amxd_object_t* ac_obj);

PRIVATE
int hosts_scheduler_change_schedule(amxd_object_t* schedule_object,
                                    amxc_var_t* old_schedule);

PRIVATE
int hosts_scheduler_add_ac_schedule(amxd_object_t* accesscontrol_object,
                                    amxd_object_t* accesscontrol_schedule_object);

PRIVATE
int hosts_scheduler_change_ac_schedule(amxd_object_t* accesscontrol_object,
                                       amxd_object_t* accesscontrol_schedule_object);

PRIVATE
int hosts_scheduler_remove_ac_schedule(amxd_object_t* accesscontrol_object,
                                       const char* schedule_id);

PRIVATE
int hosts_recalculate_all_schedules(void);

PRIVATE
bool hosts_scheduler_is_flex_time(amxd_object_t* ac_sched_obj);

PRIVATE
bool hosts_scheduler_is_flex_active(amxd_object_t* ac_sched_obj);

PRIVATE
void hosts_scheduler_flex_time_start_day(amxd_object_t* ac_sched_obj);

PRIVATE
void hosts_scheduler_flex_time_stop_day(amxd_object_t* ac_sched_obj);

PRIVATE
void hosts_scheduler_flex_start_timer(amxd_object_t* ac_obj);

PRIVATE
void hosts_scheduler_flex_stop_timer(amxd_object_t* ac_obj);

PRIVATE
bool hosts_scheduler_check_ac_schedule(amxd_object_t* ac_obj);

#ifdef __cplusplus
}
#endif

#endif // __HOSTS_SCHEDULER_H__
