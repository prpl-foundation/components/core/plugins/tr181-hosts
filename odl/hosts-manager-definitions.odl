/**
 * Device.Hosts from TR-181 datamodel 2.12
 *
 * @version 1.0
 */

%define {
    /**
     * This object provides information about each of the hosts on the LAN,
     * including those whose IP address was allocated by the CPE using DHCP as well as hosts with statically allocated IP addresses.
     * It can also include non-IP hosts.
     *
     * @version 1.0
     */
    %persistent object Hosts {

        /**
        * Only Scheduler modules that are added to this comma seperated list can be configured
        * in the Controller parameter under the bridge.
        * The name used should be the name of the so file without the extension.
        *
        * @version 1.0
        */
        %protected csv_string SupportedSchedulerControllers = "mod-scheduler-amxp";

        /**
        * Configures the module that should be used to apply this Hosts configuration
        * can only be one of the supported controllers configured in Hosts.SupportedSchedulerControllers
        *
        * @version 1.0
        */
        %persistent %protected string SchedulerController = "mod-scheduler-amxp" {
            on action validate call check_is_empty_or_in "Hosts.SupportedSchedulerControllers";
        }

        /**
         * This object provides additional configuration related to hosts management
         *
         * @version 1.0
         */
        object '${prefix_}HostConfig' {
            /**
             * Determines the max number of Hosts that may exist. This value is
             * mapped to the gmap-server MaxLanDevices parameter.
             *
             * @version 1.0
             */
            uint32 MaxDevices;

            /**
             * Interval (in seconds) between two cleanups of the inactive devices.
             *
             * @version 1.0
             */
            uint32 InactiveCheckInterval;

            /**
             * Number of LAN devices in the Hosts table to reach so that cleanup based on threshold is triggered,
             * meaning that devices inactive for more than MaxInactiveTimeThreshold are removed.
             *
             * @version 1.0
             */
            uint32 InactiveCheckThreshold;

            /**
             * Maximum amount of time (in seconds) a device can be inactive before being removed unconditionally
             * from the Hosts table at each InactiveCheckInterval.
             * A value of 0 means unlimited (inactive devices are not removed when inactive).
             *
             * @version 1.0
             */
            uint32 MaxInactiveTime;

            /**
             * Maximum amount of time (in seconds) a device can be inactive before being removed
             * at each InactiveCheckInterval when InactiveCheckThreshold is reached.
             *
             * @version 1.0
             */
            uint32 MaxInactiveTimeThreshold;

            /**
             * Cleanup function. Removes all inactive (and unprotected) devices
             * and hosts by calling gmap server's removeInactiveDevices().
             */
            bool RemoveInactiveDevices(uint32 minimumInactiveInterval = 0, %out uint32 nrOfRemovedDevices);
        }
    }
}

%populate {
    synchronize 'Devices.Config.global.' <-> 'Hosts.${prefix_}HostConfig.' {
        %batch parameter 'MaxLanDevices' <-> 'MaxDevices';
        %batch parameter 'InactiveCheckInterval' <-> 'InactiveCheckInterval';
        %batch parameter 'InactiveCheckThreshold' <-> 'InactiveCheckThreshold';
        %batch parameter 'MaxInactiveTime' <-> 'MaxInactiveTime';
        %batch parameter 'MaxInactiveTimeThreshold' <-> 'MaxInactiveTimeThreshold';
    }
}

include "hosts-accesscontrol-definitions.odl";
include "hosts-definitions.odl";
