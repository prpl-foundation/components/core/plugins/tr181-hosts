include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C mod-scheduler-amxp/src all
	$(MAKE) -C src all
	$(MAKE) -C odl all

clean:
	$(MAKE) -C mod-scheduler-amxp/src clean
	$(MAKE) -C src clean
	$(MAKE) -C odl clean
	$(MAKE) -C test clean
	$(MAKE) -C doc clean

install: all
	$(INSTALL) -D -p -m 0644 odl/hosts-manager-definitions.odl $(DEST)/etc/amx/$(COMPONENT)/hosts-manager-definitions.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(DEST)/etc/amx/tr181-device/extensions/01_device-hosts_mapping.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_caps.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_caps.odl
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/$(COMPONENT)_defaults
	$(INSTALL) -D -p -m 0644 odl/defaults.d/* $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_defaults/
	$(INSTALL) -D -p -m 0644 odl/hosts-manager.odl $(DEST)/etc/amx/$(COMPONENT)/hosts-manager.odl
	$(INSTALL) -D -p -m 0644 odl/hosts-accesscontrol-definitions.odl $(DEST)/etc/amx/$(COMPONENT)/hosts-accesscontrol-definitions.odl
	$(INSTALL) -D -p -m 0644 odl/hosts-definitions.odl $(DEST)/etc/amx/$(COMPONENT)/hosts-definitions.odl
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)/usr/bin/amxrt $(DEST)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-scheduler-amxp.so $(DEST)/usr/lib/amx/$(COMPONENT)/modules/mod-scheduler-amxp.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(DEST)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(DEST)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(DEST)$(INITDIR)/$(COMPONENT)

package: all
	$(INSTALL) -D -p -m 0644 odl/hosts-manager-definitions.odl $(PKGDIR)/etc/amx/$(COMPONENT)/hosts-manager-definitions.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(PKGDIR)/etc/amx/tr181-device/extensions/01_device-hosts_mapping.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_caps.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_caps.odl
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/$(COMPONENT)_defaults
	$(INSTALL) -D -p -m 0644 odl/defaults.d/* $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_defaults/
	$(INSTALL) -D -p -m 0644 odl/hosts-manager.odl $(PKGDIR)/etc/amx/$(COMPONENT)/hosts-manager.odl
	$(INSTALL) -D -p -m 0644 odl/hosts-accesscontrol-definitions.odl $(PKGDIR)/etc/amx/$(COMPONENT)/hosts-accesscontrol-definitions.odl
	$(INSTALL) -D -p -m 0644 odl/hosts-definitions.odl $(PKGDIR)/etc/amx/$(COMPONENT)/hosts-definitions.odl
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/$(COMPONENT)
	ln -sfr $(PKGDIR)/usr/bin/amxrt $(PKGDIR)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-scheduler-amxp.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/modules/mod-scheduler-amxp.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(PKGDIR)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	$(MAKE) -C doc doc

	$(eval ODLFILES += odl/hosts-manager-definitions.odl)
	$(eval ODLFILES += odl/$(COMPONENT)_mapping.odl)
	$(eval ODLFILES += odl/$(COMPONENT)_caps.odl)
	$(eval ODLFILES += odl/hosts-manager.odl)
	$(eval ODLFILES += odl/hosts-accesscontrol-definitions.odl)
	$(eval ODLFILES += odl/hosts-definitions.odl)

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C test run
	$(MAKE) -C test coverage
	$(MAKE) -C mod-scheduler-amxp/test run
	$(MAKE) -C mod-scheduler-amxp/test coverage

.PHONY: all clean changelog install package doc test