/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdarg.h>
#include <setjmp.h>
#include <unistd.h>
#include <cmocka.h>
#include "hosts_manager.h"
#include "hosts_sync.h"

#include "../common/mock.h"
#include "../common/common_functions.h"
#include "test_start_stop.h"

#include <amxut/amxut_bus.h>
#include <amxut/amxut_timer.h>
#include <amxut/amxut_dm.h>

static gmap_query_t query = {0};
#define MOCK_TEST_ODL "mock_hosts.odl"

#define HOSTS_DEFINITION_ODL "../../odl/hosts-manager-definitions.odl"
#define NETMODEL_TEST_ODL "../common/mock_netmodel_dm.odl"
#define DEVICE_TEST_ODL "../common/mock_devices_dm.odl"

#define HOSTS_QUERY_EXPRESSION "lan and not self and physical and .Master==\"\""

static amxd_dm_t* dm = NULL;
static amxo_parser_t* parser = NULL;
static amxb_bus_ctx_t* bus_ctx = NULL;

int test_setup(void** state) {
    amxd_object_t* root_obj = NULL;

    amxut_bus_setup(state);

    sahTraceOpen("test", TRACE_TYPE_STDERR);
    sahTraceSetLevel(TRACE_LEVEL_ERROR);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);

    dm = amxut_bus_dm();
    parser = amxut_bus_parser();
    bus_ctx = amxut_bus_ctx();
    root_obj = amxd_dm_get_root(dm);
    assert_non_null(root_obj);

    resolver_add_all_functions(amxut_bus_parser());
    test_init_dummy_fn_resolvers(amxut_bus_parser());

    amxo_resolver_import_open(parser, "/usr/lib/amx/modules/mod-dmext.so", "mod-dmext", 0);
    assert_int_equal(amxo_parser_parse_file(parser, HOSTS_DEFINITION_ODL, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(parser, NETMODEL_TEST_ODL, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(parser, DEVICE_TEST_ODL, root_obj), 0);


    amxut_bus_handle_events();

    return 0;
}

int test_teardown(void** state) {
    sahTraceClose();
    dm = NULL;
    parser = NULL;
    bus_ctx = NULL;

    amxut_bus_teardown(state);
    amxo_resolver_import_close_all();

    return 0;
}

void hosts_manager_init_success(void** state) {
    amxc_var_t* check_data = NULL;
    gmap_query_t query = {0};

    expect_string(__wrap_gmap_query_open_ext_2, expression, HOSTS_QUERY_EXPRESSION);
    will_return(__wrap_gmap_query_open_ext_2, &query);

    expect_value(__wrap_gmap_query_close, query, &query);

    amxd_object_t* root_obj = amxd_dm_get_root(dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(parser, MOCK_TEST_ODL, root_obj), 0);

    expect_string(dummy_func, function_name, "scheduler-init");
    check_data = build_check_arg(NULL, NULL);
    expect_check(dummy_func, args, check_args, check_data);
    assert_int_equal(_hosts_manager_main(AMXO_START, dm, parser), 0);
    assert_ptr_equal(hosts_manager_get_dm(), dm);

    expect_string(dummy_func, function_name, "scheduler-clean");
    check_data = build_check_arg(NULL, NULL);
    expect_check(dummy_func, args, check_args, check_data);
    assert_int_equal(_hosts_manager_main(AMXO_STOP, dm, parser), 0);
}

void hosts_manager_init_success_iptables_blocked(void** state) {
    amxc_var_t* check_data = NULL;
    gmap_query_t query = {0};

    expect_string(__wrap_gmap_query_open_ext_2, expression, HOSTS_QUERY_EXPRESSION);
    will_return(__wrap_gmap_query_open_ext_2, &query);

    expect_value(__wrap_gmap_query_close, query, &query);

    amxd_object_t* root_obj = amxd_dm_get_root(dm);
    assert_non_null(root_obj);
    assert_non_null(parser);

    assert_int_equal(amxo_parser_parse_file(parser, MOCK_TEST_ODL, root_obj), 0);

    expect_string(dummy_func, function_name, "scheduler-init");
    check_data = build_check_arg(NULL, NULL);
    expect_check(dummy_func, args, check_args, check_data);
    assert_int_equal(_hosts_manager_main(AMXO_START, dm, parser), 0);
    assert_ptr_equal(hosts_manager_get_dm(), dm);

    expect_string(dummy_func, function_name, "scheduler-clean");
    check_data = build_check_arg(NULL, NULL);
    expect_check(dummy_func, args, check_args, check_data);
    assert_int_equal(_hosts_manager_main(AMXO_STOP, dm, parser), 0);
}
