MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../include_priv ../common)
MOCK_SRCDIR = $(realpath ../common/)

HEADERS = $(wildcard $(INCDIR)/*.h)
SOURCES = $(wildcard $(SRCDIR)/*.c)
SOURCES += $(wildcard $(MOCK_SRCDIR)/*.c)

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
          --std=gnu99 -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. \
		  -fkeep-inline-functions -fkeep-static-functions \
		  -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) -pthread \
		  -Wno-unused-variable -Wno-unused-parameter \
		  -DSAHTRACES_ENABLED -DSAHTRACES_LEVEL=500

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) \
		   -lamxb -lamxc -lamxd -lamxm -lamxo -lamxp -lamxs\
		   -lsahtrace -lgmap-client -lnetmodel  -ltr181-schedules \
		   -ldl -lpthread -lamxut

WRAP_FUNC=-Wl,--wrap=

MOCK_WRAP = gmap_query_open_ext	\
            gmap_query_open_ext_2 \
            gmap_query_close	\
			system	\
			gmap_device_clearTag \
			gmap_device_setTag \
			gmap_devices_findByMac


LDFLAGS += -g $(addprefix $(WRAP_FUNC),$(MOCK_WRAP))
