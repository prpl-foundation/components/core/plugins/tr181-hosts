/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdarg.h>
#include <setjmp.h>
#include <unistd.h>
#include <cmocka.h>
#include "hosts_manager.h"
#include "hosts_sync.h"
#include "hosts_accesscontroller.h"

#include "../common/mock.h"
#include "../common/common_functions.h"
#include "test_sync.h"

#include <amxut/amxut_bus.h>
#include <amxut/amxut_timer.h>
#include <amxut/amxut_dm.h>

#define DEVICE_TEST_ODL "../common/mock_devices_dm.odl"
#define MOCK_TEST_ODL "mock_test.odl"
#define NETMODEL_TEST_ODL "../common/mock_netmodel_dm.odl"
#define HOSTS_DEFINITION_ODL "../../odl/hosts-manager-definitions.odl"

#define HOSTS_QUERY_EXPRESSION "lan and not self and physical and .Master==\"\""

static amxd_dm_t* dm = NULL;
static amxo_parser_t* parser = NULL;
static amxb_bus_ctx_t* bus_ctx = NULL;
static gmap_query_t query = {0};

int test_setup(void** state) {
    amxd_object_t* root_obj = NULL;

    amxut_bus_setup(state);

    sahTraceOpen("test", TRACE_TYPE_STDERR);
    sahTraceSetLevel(TRACE_LEVEL_ERROR);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);

    dm = amxut_bus_dm();
    parser = amxut_bus_parser();
    bus_ctx = amxut_bus_ctx();
    root_obj = amxd_dm_get_root(dm);
    assert_non_null(root_obj);

    resolver_add_all_functions(parser);
    test_init_dummy_fn_resolvers(parser);

    amxo_resolver_import_open(parser, "/usr/lib/amx/modules/mod-dmext.so", "mod-dmext", 0);
    assert_int_equal(amxo_parser_parse_file(parser, DEVICE_TEST_ODL, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(parser, NETMODEL_TEST_ODL, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(parser, MOCK_TEST_ODL, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(parser, HOSTS_DEFINITION_ODL, root_obj), 0);

    _dummy_main(0, dm, parser);

    assert_int_equal(amxo_parser_start_synchronize(parser), 0);

    amxut_bus_handle_events();

    return 0;
}

int test_teardown(UNUSED void** state) {
    _dummy_main(1, dm, parser);
    amxo_parser_stop_synchronize(parser);
    sahTraceClose();

    dm = NULL;
    parser = NULL;
    bus_ctx = NULL;

    amxut_bus_teardown(state);
    amxo_resolver_import_close_all();

    return 0;
}

void hosts_sync_init_success(void** state) {
    gmap_query_t query = {0};

    expect_string(__wrap_gmap_query_open_ext_2, expression, HOSTS_QUERY_EXPRESSION);
    will_return(__wrap_gmap_query_open_ext_2, &query);

    expect_value(__wrap_gmap_query_close, query, &query);


    assert_int_equal(hosts_sync_init(dm, parser), 0);

    assert_int_equal(hosts_sync_cleanup(), 0);
}

void hosts_sync_add_remove_success(void** state) {
    gmap_query_t query = {0};
    amxc_var_t* device = NULL;
    amxd_object_t* host = NULL;
    char* dhcp_path = NULL;

    amxd_object_t* interface = NULL;
    expect_string(__wrap_gmap_query_open_ext_2, expression, HOSTS_QUERY_EXPRESSION);
    will_return(__wrap_gmap_query_open_ext_2, &query);

    expect_value(__wrap_gmap_query_close, query, &query);

    assert_int_equal(hosts_sync_init(dm, parser), 0);
    hosts_query_cb(NULL, "ID-c434b692-66e3-42a8-8c03-5e99a7099d13", NULL, gmap_query_expression_start_matching);
    hosts_query_cb(NULL, "ID-c434b692-66e3-42a8-8c03-5e99a7089d13", NULL, gmap_query_expression_start_matching);
    host = amxd_dm_findf(dm, "Hosts.Host.1.");
    dhcp_path = amxd_object_get_value(cstring_t, host, "DHCPClient", NULL);
    assert_string_equal(dhcp_path, "DHCPv4.Server.Pool.lan.Client.1.");
    hosts_query_cb(NULL, "ID-c434b692-66e3-42a8-8c03-5e99a7099d13", NULL, gmap_query_expression_stop_matching);
    hosts_query_cb(NULL, "ID-c434b692-66e3-42a8-8c03-5e99a7089d13", NULL, gmap_query_expression_stop_matching);

    assert_int_equal(hosts_sync_cleanup(), 0);

    free(dhcp_path);
}

void hosts_sync_add_remove_failure(void** state) {
    gmap_query_t query = {0};
    amxc_var_t* device = NULL;
    amxd_object_t* interface = NULL;

    expect_string(__wrap_gmap_query_open_ext_2, expression, HOSTS_QUERY_EXPRESSION);
    will_return(__wrap_gmap_query_open_ext_2, &query);

    expect_value(__wrap_gmap_query_close, query, &query);


    assert_int_equal(hosts_sync_init(dm, parser), 0);

    hosts_query_cb(NULL, "ID-is-not-there", NULL, gmap_query_expression_start_matching);
    hosts_query_cb(NULL, "ID-is-not-there", NULL, gmap_query_expression_stop_matching);

    assert_int_equal(hosts_sync_cleanup(), 0);
}

void hosts_sync_change_protected(void** state) {
    amxc_var_t* check_data = NULL;
    amxd_object_t* interface = NULL;
    gmap_query_t query = {0};
    amxd_trans_t transaction;
    amxc_string_t param;
    char* key = NULL;
    const char* prefix;

    assert_int_equal(_dummy_main(AMXO_STOP, dm, parser), 0);

    expect_string(__wrap_gmap_query_open_ext_2, expression, HOSTS_QUERY_EXPRESSION);
    will_return(__wrap_gmap_query_open_ext_2, &query);

    assert_int_equal(_hosts_manager_main(AMXO_START, dm, parser), 0);
    hosts_query_cb(NULL, "ID-c434b692-66e3-42a8-8c03-5e99a7099d13", NULL,
                   gmap_query_expression_start_matching);

    /* Change host's X_PRPL-COM_Protected parameter and expect gmap_device_setTag()
     * to be called */
    key = calloc(64, sizeof(char));
    sprintf(key, "ID-c434b692-66e3-42a8-8c03-5e99a7099d13");
    expect_string(__wrap_gmap_devices_findByMac, mac, "80:E8:2C:2E:7D:89");
    will_return(__wrap_gmap_devices_findByMac, key);
    mock_gmap_expect_setTag("ID-c434b692-66e3-42a8-8c03-5e99a7099d13", "protected",
                            NULL, gmap_traverse_this);

    prefix = GET_CHAR(amxo_parser_get_config(parser, "prefix_"), NULL);
    amxc_string_init(&param, 0);
    amxc_string_setf(&param, "%sProtected", prefix);

    amxd_trans_init(&transaction);
    amxd_trans_select_pathf(&transaction, "Hosts.Host.1.");
    amxd_trans_set_value(bool, &transaction, amxc_string_get(&param, 0), true);
    assert_int_equal(amxd_trans_apply(&transaction, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&transaction);
    amxc_string_clean(&param);

    hosts_query_cb(NULL, "ID-c434b692-66e3-42a8-8c03-5e99a7099d13", NULL,
                   gmap_query_expression_stop_matching);
    expect_value(__wrap_gmap_query_close, query, &query);
    assert_int_equal(_hosts_manager_main(AMXO_STOP, dm, parser), 0);
    assert_int_equal(_dummy_main(AMXO_START, dm, parser), 0);
}

void hosts_sync_tag_untag(void** state) {
    gmap_query_t query = {0};
    amxd_object_t* host = NULL;
    amxd_object_t* device = NULL;
    amxd_trans_t transaction;
    bool protected = false;
    amxc_string_t param;
    const char* prefix;
    char* key;
    amxd_object_t* interface = NULL;

    assert_int_equal(_dummy_main(AMXO_STOP, dm, parser), 0);

    expect_string(__wrap_gmap_query_open_ext_2, expression, HOSTS_QUERY_EXPRESSION);
    will_return(__wrap_gmap_query_open_ext_2, &query);

    /* Create a host object. Expect X_PRPL-COM_Protected parameter to be false */
    assert_int_equal(_hosts_manager_main(AMXO_START, dm, parser), 0);
    hosts_query_cb(NULL, "ID-c434b692-66e3-42a8-8c03-5e99a7099d13", NULL,
                   gmap_query_expression_start_matching);

    prefix = GET_CHAR(amxo_parser_get_config(parser, "prefix_"), NULL);
    amxc_string_init(&param, 0);
    amxc_string_setf(&param, "%sProtected", prefix);

    host = amxd_dm_findf(dm, "Hosts.Host.1.");
    protected = amxd_object_get_value(bool, host, amxc_string_get(&param, 0), NULL);
    assert_int_equal((int) protected, 0);

    /* Add "protected" to the Device's tags, expect X_PRPL-COM_Protected to
     * change to true */
    amxd_trans_init(&transaction);
    amxd_trans_select_pathf(&transaction, "Devices.Device.2.");
    amxd_trans_set_value(cstring_t, &transaction, "Tags", "lan edev mac physical eth protected");
    assert_int_equal(amxd_trans_apply(&transaction, dm), 0);

    key = calloc(64, sizeof(char));
    sprintf(key, "ID-c434b692-66e3-42a8-8c03-5e99a7099d13");
    expect_string(__wrap_gmap_devices_findByMac, mac, "80:E8:2C:2E:7D:89");
    will_return(__wrap_gmap_devices_findByMac, key);
    mock_gmap_expect_setTag("ID-c434b692-66e3-42a8-8c03-5e99a7099d13", "protected",
                            NULL, gmap_traverse_this);

    amxut_bus_handle_events();
    amxd_trans_clean(&transaction);

    protected = amxd_object_get_value(bool, host, amxc_string_get(&param, 0), NULL);
    assert_int_equal((int) protected, 1);

    /* Remove "protected" from the Device's tags, expect X_PRPL-COM_Protected to
     * change back to false */
    amxd_trans_init(&transaction);
    amxd_trans_select_pathf(&transaction, "Devices.Device.2.");
    amxd_trans_set_value(cstring_t, &transaction, "Tags", "lan edev mac physical eth");
    assert_int_equal(amxd_trans_apply(&transaction, dm), 0);

    key = calloc(64, sizeof(char));
    sprintf(key, "ID-c434b692-66e3-42a8-8c03-5e99a7099d13");
    expect_string(__wrap_gmap_devices_findByMac, mac, "80:E8:2C:2E:7D:89");
    will_return(__wrap_gmap_devices_findByMac, key);
    mock_gmap_expect_clearTag("ID-c434b692-66e3-42a8-8c03-5e99a7099d13", "protected",
                              NULL, gmap_traverse_this);
    amxut_bus_handle_events();
    amxd_trans_clean(&transaction);

    protected = amxd_object_get_value(bool, host, amxc_string_get(&param, 0), NULL);
    assert_int_equal((int) protected, 0);

    hosts_query_cb(NULL, "ID-c434b692-66e3-42a8-8c03-5e99a7099d13", NULL,
                   gmap_query_expression_stop_matching);
    amxc_string_clean(&param);

    expect_value(__wrap_gmap_query_close, query, &query);
    assert_int_equal(_hosts_manager_main(AMXO_STOP, dm, parser), 0);
    assert_int_equal(_dummy_main(AMXO_START, dm, parser), 0);
}

void hosts_sync_wifi_device(void** state) {
    gmap_query_t query = {0};
    amxc_var_t* device = NULL;
    amxd_object_t* host = NULL;

    expect_string(__wrap_gmap_query_open_ext_2, expression, HOSTS_QUERY_EXPRESSION);
    will_return(__wrap_gmap_query_open_ext_2, &query);
    expect_value(__wrap_gmap_query_close, query, &query);
    assert_int_equal(_hosts_manager_main(0, dm, parser), 0);

    hosts_query_cb(NULL, "ID-c434b692-66e3-42a8-8c03-5e99a7089d14", NULL, gmap_query_expression_start_matching);

    // Verify parameters in Host object
    host = amxd_dm_findf(dm, "Hosts.Host.1.");
    assert_non_null(host);

    assert_string_equal(GET_CHAR(amxd_object_get_param_value(host, "Layer1Interface"), NULL), "Device.WiFi.SSID.1");
    assert_string_equal(GET_CHAR(amxd_object_get_param_value(host, "Layer3Interface"), NULL), "Device.IP.Interface.3");
    assert_string_equal(GET_CHAR(amxd_object_get_param_value(host, "AssociatedDevice"), NULL), "Device.WiFi.AccessPoint.1.AssociatedDevice.1");

    hosts_query_cb(NULL, "ID-c434b692-66e3-42a8-8c03-5e99a7089d14", NULL, gmap_query_expression_stop_matching);

    assert_int_equal(_hosts_manager_main(1, dm, parser), 0);
}

static int compare_params(amxc_var_t* data_a,
                          amxc_var_t* data_b,
                          const char* key_a,
                          const char* key_b) {
    int ret = -1;
    int status = 0;

    amxc_var_t* a = GET_ARG(data_a, key_a);
    amxc_var_t* b = GET_ARG(data_b, key_b);

    when_null(a, exit);
    when_null(b, exit);

    if((amxc_var_compare(a, b, &status) == 0) && (status == 0)) {
        ret = 0;
    }

exit:
    return ret;
}

void hosts_sync_config(void** state) {
    amxc_var_t* prefix_var = NULL;
    amxd_object_t* devices_cfg = NULL;
    amxd_object_t* hosts_cfg = NULL;
    amxc_var_t devices_params;
    amxc_var_t hosts_params;

    amxc_var_init(&devices_params);
    amxc_var_init(&hosts_params);

    expect_string(__wrap_gmap_query_open_ext_2, expression, HOSTS_QUERY_EXPRESSION);
    will_return(__wrap_gmap_query_open_ext_2, &query);
    expect_value(__wrap_gmap_query_close, query, &query);
    assert_int_equal(_hosts_manager_main(0, dm, parser), 0);

    prefix_var = get_prefix_var();
    devices_cfg = amxd_dm_findf(dm, "Devices.Config.global.");
    hosts_cfg = amxd_dm_findf(dm, "Hosts.%sHostConfig.", GET_CHAR(prefix_var, NULL));

    assert_non_null(devices_cfg);
    assert_non_null(hosts_cfg);

    assert_int_equal(amxd_object_get_params(devices_cfg, &devices_params, amxd_dm_access_private), 0);
    assert_int_equal(amxd_object_get_params(hosts_cfg, &hosts_params, amxd_dm_access_private), 0);

    // Verify that the config parameters have the default gmap values
    assert_int_equal(compare_params(&devices_params, &hosts_params, "MaxLanDevices", "MaxDevices"), 0);
    assert_int_equal(compare_params(&devices_params, &hosts_params, "InactiveCheckInterval", "InactiveCheckInterval"), 0);
    assert_int_equal(compare_params(&devices_params, &hosts_params, "InactiveCheckThreshold", "InactiveCheckThreshold"), 0);
    assert_int_equal(compare_params(&devices_params, &hosts_params, "MaxInactiveTime", "MaxInactiveTime"), 0);
    assert_int_equal(compare_params(&devices_params, &hosts_params, "MaxInactiveTimeThreshold", "MaxInactiveTimeThreshold"), 0);

    assert_int_equal(_hosts_manager_main(1, dm, parser), 0);

    amxc_var_clean(&devices_params);
    amxc_var_clean(&hosts_params);
}
