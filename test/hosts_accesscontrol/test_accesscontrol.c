/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdarg.h>
#include <setjmp.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <cmocka.h>
#include "gmap/gmap_devices.h"
#include "gmap/gmap_device.h"
#include "hosts_manager.h"
#include "dm_hosts_accesscontrol.h"

#include "../common/mock.h"
#include "../common/common_functions.h"
#include "test_accesscontrol.h"

#include <amxm/amxm.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_timer.h>
#include <amxut/amxut_dm.h>

#define MOCK_TEST_ODL "mock_test.odl"
#define MOCK_HOSTS_ODL "mock_hosts_scheduler.odl"
#define HOSTS_DEFINITION_ODL "../../odl/hosts-manager-definitions.odl"
#define NETMODEL_TEST_ODL "../common/mock_netmodel_dm.odl"
#define DEVICE_TEST_ODL "../common/mock_devices_dm.odl"

static gmap_query_t query = {0};
static amxd_dm_t* dm = NULL;
static amxo_parser_t* parser = NULL;
static amxb_bus_ctx_t* bus_ctx = NULL;

int test_setup(void** state) {
    amxd_object_t* root_obj = NULL;
    amxc_var_t* check_data = NULL;

    amxut_bus_setup(state);

    expect_string(__wrap_gmap_query_open_ext_2, expression, "lan and not self and physical and .Master==\"\"");
    will_return(__wrap_gmap_query_open_ext_2, &query);

    sahTraceOpen("test", TRACE_TYPE_STDERR);
    sahTraceSetLevel(TRACE_LEVEL_ERROR);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);

    dm = amxut_bus_dm();
    parser = amxut_bus_parser();
    bus_ctx = amxut_bus_ctx();
    root_obj = amxd_dm_get_root(dm);
    assert_non_null(root_obj);

    resolver_add_all_functions(parser);
    test_init_dummy_fn_resolvers(parser);

    amxo_resolver_import_open(parser, "/usr/lib/amx/modules/mod-dmext.so", "mod-dmext", 0);
    assert_int_equal(amxo_parser_parse_file(parser, HOSTS_DEFINITION_ODL, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(parser, MOCK_TEST_ODL, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(parser, MOCK_HOSTS_ODL, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(parser, NETMODEL_TEST_ODL, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(parser, DEVICE_TEST_ODL, root_obj), 0);

    expect_string(dummy_func, function_name, "scheduler-init");
    check_data = build_check_arg(NULL, NULL);
    expect_check(dummy_func, args, check_args, check_data);
    assert_int_equal(_hosts_manager_main(AMXO_START, dm, parser), 0);

    amxut_bus_handle_events();

    return 0;
}

int test_teardown(void** state) {
    amxc_var_t* check_data = NULL;
    expect_value(__wrap_gmap_query_close, query, &query);
    expect_string(dummy_func, function_name, "scheduler-clean");
    check_data = build_check_arg(NULL, NULL);
    expect_check(dummy_func, args, check_args, check_data);
    assert_int_equal(_hosts_manager_main(AMXO_STOP, dm, parser), 0);
    sahTraceClose();

    dm = NULL;
    parser = NULL;
    bus_ctx = NULL;
    amxut_bus_teardown(state);
    amxo_resolver_import_close_all();

    return 0;
}

void hosts_can_add_accesscontrol(void** state) {
    amxc_var_t params;
    amxc_var_t ret;
    amxc_var_t* check_data = NULL;
    amxd_object_t* ac_obj = NULL;

    amxc_var_init(&params);
    amxc_var_init(&ret);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);

    // When adding an accesscontrol with policy allow
    amxc_var_add_key(cstring_t, &params, "PhysAddress", "80:E8:2C:2E:00:00");
    amxc_var_add_key(cstring_t, &params, "PhysAddressMask", "FF:FF:FF:00:00:00");
    amxc_var_add_key(bool, &params, "Enable", true);
    amxc_var_add_key(cstring_t, &params, "AccessPolicy", "Allow");
    assert_int_equal(amxb_add(bus_ctx, "Hosts.AccessControl.", 0, NULL, &params, &ret, 5), 0);

    // 1. The object should be available in the data model
    ac_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:00'].");
    assert_non_null(ac_obj);

    // 2. The add instance event should be handled and
    //    the tag "wanblock" should be removed from gmap for device 80:E8:2C:*:*:*
    expect_string(__wrap_gmap_query_open_ext_2, expression, "mac_in_mask('80:E8:2C:2E:00:00','FF:FF:FF:00:00:00')");
    will_return(__wrap_gmap_query_open_ext_2, &query);
    amxut_bus_handle_events();
    mock_gmap_expect_clearTag("the_key", "wanblock", NULL, gmap_traverse_this);
    //    the schedule module should be updated if needed.
    expect_string(dummy_func, function_name, "scheduler-change-schedule");
    check_data = build_check_arg(ac_obj, NULL);
    expect_check(dummy_func, args, check_args, check_data);
    mock_gmap_resolve_match_devices("the_key", "80:E8:2C:00:00:01", query, true);

    mock_gmap_expect_clearTag("the_key_2", "wanblock", NULL, gmap_traverse_this);
    //    the schedule module should be updated if needed.
    expect_string(dummy_func, function_name, "scheduler-change-schedule");
    check_data = build_check_arg(ac_obj, NULL);
    expect_check(dummy_func, args, check_args, check_data);
    mock_gmap_resolve_match_devices("the_key_2", "80:E8:2C:00:00:02", query, true);

    amxc_var_clean(&params);
    amxc_var_clean(&ret);
}

void hosts_can_not_add_overlapping_accesscontrol(UNUSED void** state) {
    amxc_var_t params;
    amxc_var_t ret;
    amxd_object_t* ac_obj = NULL;
    amxc_var_t* check_data = NULL;

    amxc_var_init(&params);
    amxc_var_init(&ret);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);

    // When adding an accesscontrol with policy that overlap, it should fail
    amxc_var_add_key(cstring_t, &params, "PhysAddress", "80:E8:2C:2E:00:50");
    amxc_var_add_key(cstring_t, &params, "PhysAddressMask", "FF:FF:FF:FF:00:00");
    amxc_var_add_key(bool, &params, "Enable", true);
    amxc_var_add_key(cstring_t, &params, "AccessPolicy", "Allow");
    assert_int_equal(amxb_add(bus_ctx, "Hosts.AccessControl.", 0, NULL, &params, &ret, 5), 10);
    amxut_bus_handle_events();

    // 1. The object should not be available in the data model
    ac_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:50'].");
    assert_null(ac_obj);

    amxc_var_clean(&params);
    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    // When adding an accesscontrol with policy that overlap, it should fail
    amxc_var_add_key(cstring_t, &params, "PhysAddress", "11:22:33:44:55:66");
    amxc_var_add_key(cstring_t, &params, "PhysAddressMask", "FF:FF:FF:FF:FF:FF");
    amxc_var_add_key(bool, &params, "Enable", true);
    amxc_var_add_key(cstring_t, &params, "AccessPolicy", "Allow");
    assert_int_equal(amxb_add(bus_ctx, "Hosts.AccessControl.", 10, NULL, &params, &ret, 5), 0);
    expect_string(__wrap_gmap_query_open_ext_2, expression, "mac_in_mask('11:22:33:44:55:66','FF:FF:FF:FF:FF:FF')");
    will_return(__wrap_gmap_query_open_ext_2, &query);
    amxut_bus_handle_events();

    ac_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '11:22:33:44:55:66'].");
    assert_non_null(ac_obj);

    amxc_var_clean(&params);
    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &params, "PhysAddress", "11:22:33:44:55:66");
    amxc_var_add_key(cstring_t, &params, "PhysAddressMask", "FF:FF:FF:FF:FF:FF");
    amxc_var_add_key(bool, &params, "Enable", true);
    amxc_var_add_key(cstring_t, &params, "AccessPolicy", "Allow");
    assert_int_equal(amxb_add(bus_ctx, "Hosts.AccessControl.", 0, NULL, &params, &ret, 5), 10);
    amxut_bus_handle_events();

    ac_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '11:22:33:44:55:66'].");
    assert_non_null(ac_obj);

    // an other single device should be able to be added
    amxc_var_clean(&params);
    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &params, "PhysAddress", "11:22:33:66:55:66");
    amxc_var_add_key(cstring_t, &params, "PhysAddressMask", "FF:FF:FF:FF:FF:FF");
    amxc_var_add_key(bool, &params, "Enable", true);
    amxc_var_add_key(cstring_t, &params, "AccessPolicy", "Allow");
    assert_int_equal(amxb_add(bus_ctx, "Hosts.AccessControl.", 11, NULL, &params, &ret, 5), 0);
    expect_string(__wrap_gmap_query_open_ext_2, expression, "mac_in_mask('11:22:33:66:55:66','FF:FF:FF:FF:FF:FF')");
    will_return(__wrap_gmap_query_open_ext_2, &query);
    amxut_bus_handle_events();

    ac_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '11:22:33:44:55:66'].");
    assert_non_null(ac_obj);

    expect_string(dummy_func, function_name, "scheduler-reset");
    check_data = build_check_arg(ac_obj, NULL);

    expect_check(dummy_func, args, check_args, check_data);
    expect_value(__wrap_gmap_query_close, query, &query);
    assert_int_equal(amxb_del(bus_ctx, "Hosts.AccessControl.", 10, NULL, &ret, 5), 0);

    ac_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '11:22:33:66:55:66'].");
    assert_non_null(ac_obj);
    expect_string(dummy_func, function_name, "scheduler-reset");
    check_data = build_check_arg(ac_obj, NULL);
    expect_check(dummy_func, args, check_args, check_data);
    expect_value(__wrap_gmap_query_close, query, &query);
    assert_int_equal(amxb_del(bus_ctx, "Hosts.AccessControl.", 11, NULL, &ret, 5), 0);

    amxc_var_clean(&params);
    amxc_var_clean(&ret);
}

void hosts_can_change_accesscontrol_policy(void** state) {
    amxc_var_t params;
    amxc_var_t ret;
    amxc_var_t* check_data = NULL;
    amxd_object_t* ac_obj = NULL;
    amxc_var_t* var = NULL;

    amxc_var_init(&params);
    amxc_var_init(&ret);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);

    // When changing policy to deny
    ac_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:00'].");
    expect_string(dummy_func, function_name, "scheduler-change-schedule");
    check_data = build_check_arg(ac_obj, NULL);
    var = amxc_var_get_key(check_data, "AccessPolicy", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_cstring_t(var, "Deny");
    var = amxc_var_get_key(check_data, "Enable", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_bool(var, false);
    expect_check(dummy_func, args, check_args, check_data);

    amxc_var_add_key(cstring_t, &params, "AccessPolicy", "Deny");
    assert_int_equal(amxb_set(bus_ctx, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:00'].", &params, &ret, 5), 0);

    // The change-event should be handled and
    // the tag "wanblock" should be added to gmap for device 80:E8:2C:*:*:*
    expect_value(__wrap_gmap_query_close, query, &query);
    expect_string(__wrap_gmap_query_open_ext_2, expression, "mac_in_mask('80:E8:2C:2E:00:00','FF:FF:FF:00:00:00')");
    will_return(__wrap_gmap_query_open_ext_2, &query);
    amxut_bus_handle_events();

    mock_gmap_expect_setTag("the_key", "wanblock", NULL, gmap_traverse_this);
    // the schedule module should be updated if needed.
    expect_string(dummy_func, function_name, "scheduler-change-schedule");
    check_data = build_check_arg(ac_obj, NULL);
    expect_check(dummy_func, args, check_args, check_data);
    mock_gmap_resolve_match_devices("the_key", "80:E8:2C:00:00:01", query, true);

    mock_gmap_expect_setTag("the_key_2", "wanblock", NULL, gmap_traverse_this);
    // the schedule module should be updated if needed.
    expect_string(dummy_func, function_name, "scheduler-change-schedule");
    check_data = build_check_arg(ac_obj, NULL);
    expect_check(dummy_func, args, check_args, check_data);
    mock_gmap_resolve_match_devices("the_key_2", "80:E8:2C:00:00:02", query, true);

    amxc_var_clean(&params);
    amxc_var_clean(&ret);
}


void hosts_can_change_accesscontrol_origin(void** state) {
    amxc_var_t params;
    amxc_var_t ret;
    amxc_var_t* check_data = NULL;
    amxd_object_t* ac_obj = NULL;

    amxc_var_init(&params);
    amxc_var_init(&ret);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);

    // When changing origin to system
    amxc_var_add_key(cstring_t, &params, "Origin", "System");
    assert_int_equal(amxb_set(bus_ctx, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:00'].", &params, &ret, 5), 0);
    amxut_bus_handle_events();

    ac_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:00'].");
    assert_non_null(ac_obj);

    // the object should not have the flags upc and the attribute reboot persistent

    assert_false(IS_BIT_SET(amxd_object_get_attrs(ac_obj), amxd_pattr_persistent));

    amxd_object_get_params(ac_obj, &params, amxd_dm_access_protected);

    amxc_var_for_each(param_var, &params) {
        if(amxc_var_type_of(param_var) != AMXC_VAR_ID_HTABLE) {
            amxd_param_t* param = amxd_object_get_param_def(ac_obj, amxc_var_key(param_var));
            assert_false(amxd_param_has_flag(param, "upc"));
        }
    }

    amxc_var_clean(&params);
    amxc_var_clean(&ret);
}

void hosts_can_change_accesscontrol_physaddress(void** state) {
    amxc_var_t params;
    amxc_var_t ret;
    amxc_var_t* check_data = NULL;
    amxc_var_t* var = NULL;
    amxd_object_t* ac_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:00'].");

    amxc_var_init(&params);
    amxc_var_init(&ret);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);

    check_data = build_check_arg(ac_obj, NULL);
    var = amxc_var_get_key(check_data, "PhysAddress", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_cstring_t(var, "80:E8:2C:2E:00:02");
    expect_check(dummy_func, args, check_args, check_data);
    expect_string(dummy_func, function_name, "scheduler-change-schedule");
    // When changing the phys address
    amxc_var_add_key(cstring_t, &params, "PhysAddress", "80:E8:2C:2E:00:02");
    assert_int_equal(amxb_set(bus_ctx, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:00'].", &params, &ret, 5), 0);

    mock_gmap_expect_clearTag("the_key", "wanblock", NULL, gmap_traverse_this);
    mock_gmap_expect_clearTag("the_key_2", "wanblock", NULL, gmap_traverse_this);

    expect_value(__wrap_gmap_query_close, query, &query);
    expect_string(__wrap_gmap_query_open_ext_2, expression, "mac_in_mask('80:E8:2C:2E:00:02','FF:FF:FF:00:00:00')");
    will_return(__wrap_gmap_query_open_ext_2, &query);
    amxut_bus_handle_events();
    mock_gmap_expect_setTag("the_key", "wanblock", NULL, gmap_traverse_this);
    //    the schedule module should be updated if needed.
    expect_string(dummy_func, function_name, "scheduler-change-schedule");
    check_data = build_check_arg(ac_obj, NULL);
    expect_check(dummy_func, args, check_args, check_data);
    mock_gmap_resolve_match_devices("the_key", "80:E8:2C:00:00:01", query, true);

    mock_gmap_expect_setTag("the_key_2", "wanblock", NULL, gmap_traverse_this);
    //    the schedule module should be updated if needed.
    expect_string(dummy_func, function_name, "scheduler-change-schedule");
    check_data = build_check_arg(ac_obj, NULL);
    expect_check(dummy_func, args, check_args, check_data);
    mock_gmap_resolve_match_devices("the_key_2", "80:E8:2C:00:00:02", query, true);

    amxc_var_clean(&params);
    amxc_var_clean(&ret);
}

void hosts_can_disable_accesscontrol(void** state) {
    amxc_var_t params;
    amxc_var_t ret;
    amxc_var_t* check_data = NULL;
    amxc_var_t* var = NULL;
    amxd_object_t* ac_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:02'].");

    amxc_var_init(&params);
    amxc_var_init(&ret);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);

    expect_string(dummy_func, function_name, "scheduler-change-schedule");
    check_data = build_check_arg(ac_obj, NULL);
    var = amxc_var_get_key(check_data, "Enable", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_bool(var, false);
    expect_check(dummy_func, args, check_args, check_data);

    amxc_var_add_key(bool, &params, "Enable", false);
    assert_int_equal(amxb_set(bus_ctx, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:02'].", &params, &ret, 5), 0);

    expect_value(__wrap_gmap_query_close, query, &query);

    mock_gmap_expect_clearTag("the_key", "wanblock", NULL, gmap_traverse_this);
    mock_gmap_expect_clearTag("the_key_2", "wanblock", NULL, gmap_traverse_this);

    amxut_bus_handle_events();

    amxc_var_clean(&params);
    amxc_var_clean(&ret);
}

void hosts_can_enable_accesscontrol(void** state) {
    amxc_var_t params;
    amxc_var_t ret;
    amxc_var_t* check_data = NULL;
    amxd_object_t* ac_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:02'].");

    amxc_var_init(&params);
    amxc_var_init(&ret);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);

    expect_string(dummy_func, function_name, "scheduler-change-schedule");
    check_data = build_check_arg(ac_obj, NULL);
    expect_check(dummy_func, args, check_args, check_data);
    amxc_var_add_key(bool, &params, "Enable", true);
    assert_int_equal(amxb_set(bus_ctx, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:02'].", &params, &ret, 5), 0);

    expect_string(__wrap_gmap_query_open_ext_2, expression, "mac_in_mask('80:E8:2C:2E:00:02','FF:FF:FF:00:00:00')");
    will_return(__wrap_gmap_query_open_ext_2, &query);
    amxut_bus_handle_events();
    //    the schedule module should be updated if needed.
    expect_string(dummy_func, function_name, "scheduler-change-schedule");
    check_data = build_check_arg(ac_obj, NULL);
    expect_check(dummy_func, args, check_args, check_data);

    expect_string(dummy_func, function_name, "scheduler-change-schedule");
    check_data = build_check_arg(ac_obj, NULL);
    expect_check(dummy_func, args, check_args, check_data);

    mock_gmap_expect_setTag("the_key", "wanblock", NULL, gmap_traverse_this);
    mock_gmap_resolve_match_devices("the_key", "80:E8:2C:00:00:01", query, true);

    mock_gmap_expect_setTag("the_key_2", "wanblock", NULL, gmap_traverse_this);
    mock_gmap_resolve_match_devices("the_key_2", "80:E8:2C:00:00:02", query, true);

    amxc_var_clean(&params);
    amxc_var_clean(&ret);
}

void hosts_can_remove_accesscontrol(void** state) {
    amxc_var_t ret;
    amxc_var_t* check_data = NULL;
    amxd_object_t* ac_obj = NULL;

    amxc_var_init(&ret);

    ac_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:02'].");
    assert_non_null(ac_obj);

    expect_value(__wrap_gmap_query_close, query, &query);

    expect_string(dummy_func, function_name, "scheduler-reset");
    check_data = build_check_arg(ac_obj, NULL);
    expect_check(dummy_func, args, check_args, check_data);

    mock_gmap_expect_clearTag("the_key", "wanblock", NULL, gmap_traverse_this);
    mock_gmap_expect_clearTag("the_key_2", "wanblock", NULL, gmap_traverse_this);
    assert_int_equal(amxb_del(bus_ctx, "Hosts.AccessControl.", 1, NULL, &ret, 5), 0);

    ac_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:02'].");
    assert_null(ac_obj);
    amxut_bus_handle_events();

    amxc_var_clean(&ret);
}
