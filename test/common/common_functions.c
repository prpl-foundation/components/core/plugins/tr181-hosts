/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <stdio.h>
#include <string.h>

#include "hosts_manager.h"
#include "dm_hosts_accesscontrol.h"
#include "hosts_scheduler.h"

#include "../common/mock.h"
#include "../common/common_functions.h"

static const char* value = "";

void resolver_add_all_functions(amxo_parser_t* parser) {
    assert_int_equal(amxo_resolver_ftab_add(parser, "check_schedule", AMXO_FUNC(_check_schedule)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ac_removed", AMXO_FUNC(_ac_removed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "check_overlapping_rules", AMXO_FUNC(_check_overlapping_rules)), 0);

    assert_int_equal(amxo_resolver_ftab_add(parser, "ac_event_ac_added", AMXO_FUNC(_ac_event_ac_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ac_event_ac_changed", AMXO_FUNC(_ac_event_ac_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ac_event_schedule_added", AMXO_FUNC(_ac_event_schedule_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ac_event_schedule_changed", AMXO_FUNC(_ac_event_schedule_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ac_event_scheduleref_changed", AMXO_FUNC(_ac_event_scheduleref_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ac_event_origin_changed", AMXO_FUNC(_ac_event_origin_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ac_event_schedule_removed", AMXO_FUNC(_ac_event_schedule_removed)), 0);

    assert_int_equal(amxo_resolver_ftab_add(parser, "hosts_active_changed", AMXO_FUNC(_hosts_active_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "hosts_protected_changed", AMXO_FUNC(_hosts_protected_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "hosts_added", AMXO_FUNC(_hosts_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "hosts_removed", AMXO_FUNC(_hosts_removed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "RemoveInactiveDevices", AMXO_FUNC(_RemoveInactiveDevices)), 0);
}

amxc_var_t* build_check_arg_ac_id(amxd_object_t* ac_obj) {
    amxc_var_t* check_arg = NULL;
    amxc_var_t* var = NULL;
    amxc_var_new(&check_arg);

    amxc_var_set_type(check_arg, AMXC_VAR_ID_HTABLE);

    if(ac_obj != NULL) {
        amxd_object_get_params(ac_obj, check_arg, amxd_dm_access_protected);
        var = amxc_var_get_key(check_arg, "Alias", AMXC_VAR_FLAG_DEFAULT);
        amxc_var_take_it(var);
        amxc_var_clean(check_arg);
        amxc_var_set_type(check_arg, AMXC_VAR_ID_HTABLE);
        amxc_var_set_key(check_arg, "AccessControlId", var, AMXC_VAR_FLAG_DEFAULT);
    }

    return check_arg;
}

amxc_var_t* build_check_arg(amxd_object_t* ac_obj, amxd_object_t* ac_sched_obj) {
    amxc_var_t* check_arg = NULL;
    amxc_var_t* schedule = NULL;
    amxc_var_t* var = NULL;
    amxc_var_new(&check_arg);
    amxc_var_set_type(check_arg, AMXC_VAR_ID_HTABLE);

    if(ac_obj != NULL) {
        amxd_object_get_params(ac_obj, check_arg, amxd_dm_access_protected);
        var = amxc_var_get_key(check_arg, "CurrentAccess", AMXC_VAR_FLAG_DEFAULT);
        amxc_var_delete(&var);
        schedule = amxc_var_add_new_key(check_arg, "schedule");
        amxc_var_set_type(schedule, AMXC_VAR_ID_HTABLE);

        if(ac_sched_obj != NULL) {
            amxd_object_get_params(ac_sched_obj, schedule, amxd_dm_access_protected);
            var = amxc_var_get_key(schedule, "Alias", AMXC_VAR_FLAG_DEFAULT);
            amxc_var_take_it(var);
            amxc_var_set_key(schedule, "ScheduleId", var, AMXC_VAR_FLAG_DEFAULT);
        }

        var = amxc_var_get_key(check_arg, "Alias", AMXC_VAR_FLAG_DEFAULT);
        amxc_var_take_it(var);
        amxc_var_set_key(check_arg, "AccessControlId", var, AMXC_VAR_FLAG_DEFAULT);

        // if ac policy is set to deny - the scheduler for this physaddress must be disabled
        if(strcmp(GET_CHAR(check_arg, "AccessPolicy"), "Deny") == 0) {
            var = GET_ARG(check_arg, "Enable");
            amxc_var_set(bool, var, false);
        }
    }

    return check_arg;
}

int check_args(const LargestIntegralType value, const LargestIntegralType check_value_data) {
    amxc_var_t* in_args = (void*) value;
    amxc_var_t* check_args = (void*) check_value_data;
    int result = 0;
    amxc_var_t* tmp = NULL;

    tmp = GET_ARG(in_args, "ScheduleNumberOfEntries");
    amxc_var_delete(&tmp);
    tmp = GET_ARG(check_args, "ScheduleNumberOfEntries");
    amxc_var_delete(&tmp);
    amxc_var_dump(in_args, STDOUT_FILENO);
    amxc_var_dump(check_args, STDOUT_FILENO);

    amxc_var_compare(in_args, check_args, &result);
    amxc_var_delete(&check_args);

    return result == 0 ? 1 : 0; // must return 1 (true) when valid.
}

void mock_gmap_expect_setTag(const char* the_key,
                             const char* the_tags,
                             const char* the_expression,
                             gmap_traverse_mode_t the_mode) {
    expect_string(__wrap_gmap_device_setTag, key, the_key);
    expect_string(__wrap_gmap_device_setTag, tags, the_tags);
    expect_value(__wrap_gmap_device_setTag, expression, the_expression);
    expect_value(__wrap_gmap_device_setTag, mode, the_mode);
}

void mock_gmap_expect_clearTag(const char* the_key,
                               const char* the_tags,
                               const char* the_expression,
                               gmap_traverse_mode_t the_mode) {
    expect_string(__wrap_gmap_device_clearTag, key, the_key);
    expect_string(__wrap_gmap_device_clearTag, tags, the_tags);
    expect_value(__wrap_gmap_device_clearTag, expression, the_expression);
    expect_value(__wrap_gmap_device_clearTag, mode, the_mode);
}

void mock_gmap_resolve_match_devices(const char* key, const char* physaddr, gmap_query_t query, bool start) {
    amxc_var_t device;

    amxc_var_init(&device);
    amxc_var_set_type(&device, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &device, "PhysAddress", physaddr);
    query.fn(&query, key, &device, start ? gmap_query_expression_start_matching : gmap_query_expression_stop_matching);

    amxc_var_clean(&device);
}
