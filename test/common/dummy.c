/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include <errno.h>
#include <fcntl.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_function.h>
#include <amxd/amxd_path.h>

#include <amxo/amxo.h>

#include "mock.h"

#define UNUSED __attribute__((unused))

typedef struct _dummy_app {
    amxd_dm_t* dm;
    amxo_parser_t* parser;
} dummy_app_t;

typedef struct {
    amxc_htable_it_t it;
    char* subscriber;
    char* method;
    char* path;
} dummy_query_t;

static dummy_app_t app;

static amxc_htable_t* query_table_n = NULL;

static amxd_status_t _getResult(amxd_object_t* object,
                                UNUSED amxd_function_t* func,
                                UNUSED amxc_var_t* args,
                                amxc_var_t* ret) {
    amxc_htable_it_t* it = NULL;
    dummy_query_t* q = NULL;
    char* path = NULL;
    char* end_of_path = NULL;

    path = amxd_object_get_path(object, AMXD_OBJECT_NAMED);
    end_of_path = strstr(path, ".Query");
    if(end_of_path != NULL) {
        *end_of_path = '\0';
    }

    it = amxc_htable_get(query_table_n, path);
    q = amxc_htable_it_get_data(it, dummy_query_t, it);
    assert_non_null(q);

    if(strcmp(q->method, "getIntfs") == 0) {
        amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
        amxc_var_add(cstring_t, ret, "ip-lan");
    } else if(strcmp(q->method, "getFirstParameter") == 0) {
        amxc_var_set(cstring_t, ret, "Device.IP.Interface.3.");
    }

    free(path);

    return amxd_status_ok;
}

static amxd_status_t _openQuery(UNUSED amxd_object_t* object,
                                UNUSED amxd_function_t* func,
                                amxc_var_t* args,
                                amxc_var_t* ret) {
    char* path = NULL;
    dummy_query_t* q = NULL;

    if(query_table_n == NULL) {
        amxc_htable_new(&query_table_n, 5);
    }

    q = calloc(1, sizeof(*q));
    q->subscriber = strdup(GETP_CHAR(args, "subscriber"));
    q->method = strdup(GETP_CHAR(args, "class"));
    path = amxd_object_get_path(object, AMXD_OBJECT_NAMED);
    amxc_htable_insert(query_table_n, path, &q->it);

    amxc_var_set(uint32_t, ret, 1);
    free(path);
    return amxd_status_ok;
}

static amxd_status_t _closeQuery(amxd_object_t* object,
                                 UNUSED amxd_function_t* func,
                                 UNUSED amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    char* path = NULL;
    amxc_htable_it_t* it = NULL;
    dummy_query_t* q = NULL;

    path = amxd_object_get_path(object, AMXD_OBJECT_NAMED);
    it = amxc_htable_take(query_table_n, path);
    q = amxc_htable_it_get_data(it, dummy_query_t, it);
    assert_non_null(q);

    amxc_htable_it_clean(&q->it, NULL);
    free(q->subscriber);
    free(q->path);
    free(q->method);
    free(q);

    if(amxc_htable_is_empty(query_table_n)) {
        amxc_htable_delete(&query_table_n, NULL);
    }

    free(path);

    return amxd_status_ok;
}

static void resolve_path(const char* const path,
                         amxc_var_t* list) {
    amxc_var_t data;
    amxc_llist_t nm_list;

    amxc_var_init(&data);
    amxc_llist_init(&nm_list);

    amxd_dm_resolve_pathf(app.dm,
                          &nm_list,
                          "NetModel.Intf.['Device.%s' in InterfacePath]",
                          path);

    amxc_llist_iterate(it, &nm_list) {
        amxd_object_t* obj = amxd_dm_findf(app.dm, "%s", amxc_string_get(amxc_string_from_llist_it(it), 0));
        amxc_var_add(cstring_t, list, amxd_object_get_name(obj, AMXD_OBJECT_NAMED));
    }
    amxc_llist_clean(&nm_list, amxc_string_list_it_free);

    amxc_var_clean(&data);
}

static amxd_status_t _resolvePath(UNUSED amxd_object_t* object,
                                  UNUSED amxd_function_t* func,
                                  amxc_var_t* args,
                                  amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t list;
    amxd_path_t path;
    char* first = NULL;

    amxc_var_init(&list);
    amxc_var_set_type(&list, AMXC_VAR_ID_LIST);

    status = amxd_path_init(&path, GET_CHAR(args, "path"));
    when_failed(status, exit);

    first = amxd_path_get_first(&path, false);
    if((first != NULL) && (strcmp("Device.", first) == 0)) {
        free(first);
        first = amxd_path_get_first(&path, true);
    }

    resolve_path(amxd_path_get(&path, AMXD_OBJECT_TERMINATE), &list);

    amxc_var_copy(ret, &list);
    status = amxd_status_ok;

exit:
    free(first);
    amxc_var_clean(&list);
    amxd_path_clean(&path);
    return status;
}

void test_init_dummy_fn_resolvers(amxo_parser_t* parser) {
    // NetModel
    amxo_resolver_ftab_add(parser, "getResult", AMXO_FUNC(_getResult));
    amxo_resolver_ftab_add(parser, "openQuery", AMXO_FUNC(_openQuery));
    amxo_resolver_ftab_add(parser, "closeQuery", AMXO_FUNC(_closeQuery));
    amxo_resolver_ftab_add(parser, "resolvePath", AMXO_FUNC(_resolvePath));
}

int _dummy_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser) {

    switch(reason) {
    case 0:     // START
        app.dm = dm;
        app.parser = parser;
        break;
    case 1:     // STOP
        app.dm = NULL;
        app.parser = NULL;
        break;
    }

    return 0;
}

