/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <amxc/amxc.h>

#include <amxp/amxp.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_types.h>
#include <string.h>

#include <amxb/amxb_types.h>
#include <linux/netlink.h>
#include <unistd.h>
#include <stdio.h>

#include "mock.h"

gmap_query_t* __wrap_gmap_query_open_ext(const char* expression, UNUSED const char* name, gmap_query_cb_t fn, void* user_data) {
    gmap_query_t* retval = NULL;
    __wrap_gmap_query_open_ext_2(&retval, expression, name, 0, fn, user_data);
    return retval;
}

bool __wrap_gmap_query_open_ext_2(gmap_query_t** query,
                                  const char* expression,
                                  UNUSED const char* name,
                                  UNUSED gmap_query_flags_t flags,
                                  gmap_query_cb_t fn,
                                  void* user_data) {
    gmap_query_t* retval = (gmap_query_t*) mock();
    assert_non_null(query);
    check_expected(expression);
    if(retval != NULL) {
        retval->fn = fn;
        retval->data = user_data;
    }
    *query = retval;
    return retval != NULL;
}

void __wrap_gmap_query_close(gmap_query_t* query) {
    check_expected(query);
}

int __wrap_system(const char* __command) {
    check_expected(__command);
    printf("System CALL: %s \n", __command);
    return mock();
}

int __wrap_amxc_ts_now(amxc_ts_t* tsp) {
    tsp->sec = 0;
    tsp->nsec = 0;
    tsp->offset = 0;
    return 0;
}

bool __wrap_gmap_device_clearTag(const char* key, const char* tags, const char* expression, gmap_traverse_mode_t mode) {
    check_expected(key);
    check_expected(tags);
    check_expected(expression);
    check_expected(mode);
    return true;
}

bool __wrap_gmap_device_setTag(const char* key, const char* tags, const char* expression, gmap_traverse_mode_t mode) {
    check_expected(key);
    check_expected(tags);
    check_expected(expression);
    check_expected(mode);
    return true;
}

char* __wrap_gmap_devices_findByMac(const char* mac) {
    check_expected(mac);
    return (char*) mock();
}
