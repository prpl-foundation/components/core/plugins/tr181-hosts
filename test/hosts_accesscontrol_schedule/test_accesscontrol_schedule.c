/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdarg.h>
#include <setjmp.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <cmocka.h>
#include "gmap/gmap_devices.h"
#include "gmap/gmap_device.h"
#include "hosts_manager.h"
#include "dm_hosts_accesscontrol.h"
#include "dm_hosts_accesscontrol.h"
#include "hosts_scheduler.h"
#include "hosts_accesscontroller_info.h"

#include "../common/mock.h"
#include "../common/common_functions.h"
#include "test_accesscontrol_schedule.h"

#include <amxm/amxm.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_timer.h>
#include <amxut/amxut_dm.h>

#define MOCK_TEST_ODL "mock_test.odl"
#define MOCK_HOSTS_ODL "mock_hosts_scheduler.odl"
#define HOSTS_DEFINITION_ODL "../../odl/hosts-manager-definitions.odl"
#define NETMODEL_TEST_ODL "../common/mock_netmodel_dm.odl"
#define DEVICE_TEST_ODL "../common/mock_devices_dm.odl"
#define SCHEDULES_TEST_ODL "../common/mock_schedules_dm.odl"

static gmap_query_t query = {0};
static amxd_dm_t* dm = NULL;
static amxo_parser_t* parser = NULL;
static amxb_bus_ctx_t* bus_ctx = NULL;

static void go_to_datetime_unchecked(const char* datetime_str) {
    amxc_ts_t datetime;
    int status = amxc_ts_parse(&datetime, datetime_str, strlen(datetime_str));
    if(status != 0) {
        fail_msg("Cannot parse time '%s'", datetime_str);
    }
    amxut_timer_go_to_datetime(&datetime);
}

int test_setup(void** state) {
    amxd_object_t* root_obj = NULL;
    amxc_var_t* check_data = NULL;
    amxd_object_t* ac_obj = NULL;

    amxut_bus_setup(state);

    expect_string(__wrap_gmap_query_open_ext_2, expression, "lan and not self and physical and .Master==\"\"");
    will_return(__wrap_gmap_query_open_ext_2, &query);

    sahTraceOpen("test", TRACE_TYPE_STDERR);
    sahTraceSetLevel(TRACE_LEVEL_ERROR);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);

    dm = amxut_bus_dm();
    parser = amxut_bus_parser();
    bus_ctx = amxut_bus_ctx();
    root_obj = amxd_dm_get_root(dm);
    assert_non_null(root_obj);

    resolver_add_all_functions(parser);
    test_init_dummy_fn_resolvers(parser);

    amxo_resolver_import_open(parser, "/usr/lib/amx/modules/mod-dmext.so", "mod-dmext", 0);
    assert_int_equal(amxo_parser_parse_file(parser, HOSTS_DEFINITION_ODL, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(parser, MOCK_TEST_ODL, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(parser, MOCK_HOSTS_ODL, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(parser, NETMODEL_TEST_ODL, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(parser, SCHEDULES_TEST_ODL, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(parser, DEVICE_TEST_ODL, root_obj), 0);

    expect_string(dummy_func, function_name, "scheduler-init");
    check_data = build_check_arg(NULL, NULL);
    expect_check(dummy_func, args, check_args, check_data);
    assert_int_equal(_hosts_manager_main(AMXO_START, dm, parser), 0);

    expect_string(__wrap_gmap_query_open_ext_2, expression, "mac_in_mask('80:E8:2C:2E:00:01','FF:FF:FF:FF:FF:FF')");
    will_return(__wrap_gmap_query_open_ext_2, &query);
    amxut_bus_handle_events();
    mock_gmap_expect_clearTag("the_key", "wanblock", NULL, gmap_traverse_this);
    ac_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].");
    expect_string(dummy_func, function_name, "scheduler-change-schedule");
    check_data = build_check_arg(ac_obj, NULL);
    expect_check(dummy_func, args, check_args, check_data);
    mock_gmap_resolve_match_devices("the_key", "80:E8:2C:00:00:01", query, true);

    return 0;
}

int test_teardown(void** state) {
    amxc_var_t* check_data = NULL;
    expect_value(__wrap_gmap_query_close, query, &query);
    expect_value(__wrap_gmap_query_close, query, &query);
    mock_gmap_expect_clearTag("the_key", "wanblock", NULL, gmap_traverse_this);
    expect_string(dummy_func, function_name, "scheduler-clean");
    check_data = build_check_arg(NULL, NULL);
    expect_check(dummy_func, args, check_args, check_data);
    assert_int_equal(_hosts_manager_main(AMXO_STOP, dm, parser), 0);
    sahTraceClose();

    dm = NULL;
    parser = NULL;
    bus_ctx = NULL;
    amxut_bus_teardown(state);
    amxo_resolver_import_close_all();

    return 0;
}

void hosts_can_add_schedule(void** state) {
    amxc_var_t params;
    amxc_var_t ret;
    amxd_object_t* ac_obj = NULL;
    amxd_object_t* ac_sched_obj = NULL;
    amxc_var_t* check_data = NULL;

    amxc_var_init(&params);
    amxc_var_init(&ret);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);

    ac_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].");
    // When adding an accesscontrol schedule on accesscontrol with policy allow
    amxc_var_add_key(bool, &params, "Enable", true);
    amxc_var_add_key(cstring_t, &params, "Day", "Monday");
    amxc_var_add_key(cstring_t, &params, "StartTime", "17:00");
    amxc_var_add_key(uint32_t, &params, "Duration", 60);
    assert_int_equal(amxb_add(bus_ctx, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].Schedule.", 0, NULL, &params, &ret, 5), 0);

    // 1. The schedule object should be available in the data model
    ac_sched_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].Schedule.1.");
    assert_non_null(ac_sched_obj);

    // 2. the device will be blocked by default, the scheduler will unblock if needed.
    mock_gmap_expect_setTag("the_key", "wanblock", NULL, gmap_traverse_this);

    // 3. The add instance event should be handled and the scheduler add function must be called
    expect_string(dummy_func, function_name, "scheduler-add-schedule");
    check_data = build_check_arg(ac_obj, ac_sched_obj);
    expect_check(dummy_func, args, check_args, check_data);
    amxut_bus_handle_events();

    amxc_var_clean(&params);
    amxc_var_clean(&ret);
}

void hosts_can_change_schedule(void** state) {
    amxc_var_t params;
    amxc_var_t ret;
    amxd_object_t* ac_obj = NULL;
    amxd_object_t* ac_sched_obj = NULL;
    amxc_var_t* check_data = NULL;

    amxc_var_init(&params);
    amxc_var_init(&ret);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);

    ac_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].");
    ac_sched_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].Schedule.1.");

    // When changing an accesscontrol schedule on accesscontrol with policy allow
    amxc_var_add_key(bool, &params, "Enable", true);
    amxc_var_add_key(cstring_t, &params, "Day", "Monday");
    amxc_var_add_key(cstring_t, &params, "StartTime", "16:00");
    amxc_var_add_key(uint32_t, &params, "Duration", 3600);
    assert_int_equal(amxb_set(bus_ctx, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].Schedule.1.", &params, &ret, 5), 0);

    // 1. The schedule object should be changed

    // 2. the scheduler change function must be called
    expect_string(dummy_func, function_name, "scheduler-change-schedule");
    check_data = build_check_arg(ac_obj, ac_sched_obj);
    expect_check(dummy_func, args, check_args, check_data);

    mock_gmap_expect_setTag("the_key", "wanblock", NULL, gmap_traverse_this);
    expect_string(dummy_func, function_name, "scheduler-change-schedule");
    check_data = build_check_arg(ac_obj, NULL);
    expect_check(dummy_func, args, check_args, check_data);

    amxut_bus_handle_events();
    amxc_var_clean(&params);
    amxc_var_clean(&ret);
}

static void remove_schedule_data_from_check_args(amxc_var_t* check_data, amxc_var_t* var) {
    var = GETP_ARG(check_data, "schedule.Day");
    amxc_var_delete(&var);
    var = GETP_ARG(check_data, "schedule.Duration");
    amxc_var_delete(&var);
    var = GETP_ARG(check_data, "schedule.Enable");
    amxc_var_delete(&var);
    var = GETP_ARG(check_data, "schedule.StartTime");
    amxc_var_delete(&var);
}

void hosts_can_remove_schedule(void** state) {
    amxc_var_t ret;
    amxc_var_t* var = NULL;
    amxd_object_t* ac_obj = NULL;
    amxd_object_t* ac_sched_obj = NULL;
    amxc_var_t* check_data = NULL;

    amxc_var_init(&ret);

    ac_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].");
    ac_sched_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].Schedule.1.");
    check_data = build_check_arg(ac_obj, ac_sched_obj);
    // only the schedule-id is passed - so remove all others
    remove_schedule_data_from_check_args(check_data, var);

    // When changing an accesscontrol schedule on accesscontrol with policy allow
    assert_int_equal(amxb_del(bus_ctx, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].Schedule.", 1, NULL, &ret, 5), 0);

    // 1. The schedule object should be removed
    ac_sched_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].Schedule.1.");
    assert_null(ac_sched_obj);

    // 2. the scheduler change function must be called
    mock_gmap_expect_clearTag("the_key", "wanblock", NULL, gmap_traverse_this);
    expect_string(dummy_func, function_name, "scheduler-remove-schedule");
    expect_check(dummy_func, args, check_args, check_data);
    expect_string(dummy_func, function_name, "scheduler-change-schedule");
    check_data = build_check_arg(ac_obj, NULL);
    expect_check(dummy_func, args, check_args, check_data);
    amxut_bus_handle_events();

    amxc_var_clean(&ret);
}

void hosts_can_add_schedule_with_empty_starttime(void** state) {
    amxc_var_t params;
    amxc_var_t ret;
    amxd_object_t* ac_obj = NULL;
    amxd_object_t* ac_sched_obj = NULL;
    amxc_var_t* check_data = NULL;

    amxc_var_init(&params);
    amxc_var_init(&ret);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);

    ac_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].");
    // When adding an accesscontrol schedule on accesscontrol with policy allow
    amxc_var_add_key(bool, &params, "Enable", true);
    amxc_var_add_key(cstring_t, &params, "Day", "Monday");
    amxc_var_add_key(uint32_t, &params, "Duration", 60);
    assert_int_equal(amxb_add(bus_ctx, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].Schedule.", 0, NULL, &params, &ret, 5), 0);

    // 1. The schedule object should be available in the data model
    ac_sched_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].Schedule.2.");
    assert_non_null(ac_sched_obj);

    // 2. the device will be blocked by default, the scheduler will unblock if needed.
    mock_gmap_expect_setTag("the_key", "wanblock", NULL, gmap_traverse_this);

    // 3. The add instance event should be handled and the scheduler add function must be called
    expect_string(dummy_func, function_name, "scheduler-add-schedule");
    check_data = build_check_arg(ac_obj, ac_sched_obj);
    expect_check(dummy_func, args, check_args, check_data);
    amxut_bus_handle_events();

    amxc_var_clean(&params);
    amxc_var_clean(&ret);
}

void hosts_can_not_add_second_schedule_with_empty_starttime(void** state) {
    amxc_var_t params;
    amxc_var_t ret;

    amxc_var_init(&params);
    amxc_var_init(&ret);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(bool, &params, "Enable", true);
    amxc_var_add_key(cstring_t, &params, "Day", "Monday");
    amxc_var_add_key(uint32_t, &params, "Duration", 60);
    assert_int_not_equal(amxb_add(bus_ctx, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].Schedule.", 0, NULL, &params, &ret, 5), 0);

    amxc_var_clean(&params);
    amxc_var_clean(&ret);
}

void hosts_can_add_schedule_with_empty_starttime_for_other_day(void** state) {
    amxc_var_t params;
    amxc_var_t ret;
    amxd_object_t* ac_obj = NULL;
    amxd_object_t* ac_sched_obj = NULL;
    amxc_var_t* check_data = NULL;

    amxc_var_init(&params);
    amxc_var_init(&ret);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);

    ac_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].");
    // When adding an accesscontrol schedule on accesscontrol with policy allow
    amxc_var_add_key(bool, &params, "Enable", true);
    amxc_var_add_key(cstring_t, &params, "Day", "Saturday");
    amxc_var_add_key(uint32_t, &params, "Duration", 60);
    assert_int_equal(amxb_add(bus_ctx, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].Schedule.", 0, NULL, &params, &ret, 5), 0);

    // 1. The schedule object should be available in the data model
    ac_sched_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].Schedule.4.");
    assert_non_null(ac_sched_obj);

    // 2. the device will be blocked by default, the scheduler will unblock if needed.
    mock_gmap_expect_setTag("the_key", "wanblock", NULL, gmap_traverse_this);

    // 3. The add instance event should be handled and the scheduler add function must be called
    expect_string(dummy_func, function_name, "scheduler-add-schedule");
    check_data = build_check_arg(ac_obj, ac_sched_obj);
    expect_check(dummy_func, args, check_args, check_data);
    amxut_bus_handle_events();

    amxc_var_clean(&params);
    amxc_var_clean(&ret);
}

void hosts_can_start_schedule_with_empty_starttime(void** state) {
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    mock_gmap_expect_clearTag("the_key", "wanblock", NULL, gmap_traverse_this);

    amxc_var_add_key(cstring_t, &args, "AccessControlId", "cpe-AccessControl-1");
    amxc_var_add_key(cstring_t, &args, "ScheduleId", "cpe-Schedule-2");

    assert_int_equal(amxm_execute_function(NULL, "host-scheduler", "ac-start-interval", &args, &ret), 0);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void hosts_add_incative_host_for_active_schedule_with_empty_starttime(void** state) {
    amxd_trans_t transaction;
    amxd_trans_init(&transaction);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_select_pathf(&transaction, "Hosts.Host.");
    amxd_trans_add_inst(&transaction, 0, NULL);
    amxd_trans_set_value(cstring_t, &transaction, "PhysAddress", "80:E8:2C:2E:00:01");
    amxd_trans_set_value(cstring_t, &transaction, "DeviceReference", "Devices.Device.80:E8:2C:2E:00:01.");
    amxd_trans_set_value(bool, &transaction, "Active", false);

    assert_int_equal(amxd_trans_apply(&transaction, dm), 0);
    amxd_trans_clean(&transaction);

    amxut_bus_handle_events();
}

void hosts_activate_host_for_active_schedule_with_empty_starttime(void** state) {
    amxd_object_t* ac_obj = NULL;

    amxd_trans_t transaction;
    amxd_trans_init(&transaction);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_select_pathf(&transaction, "Hosts.Host.[PhysAddress=='80:E8:2C:2E:00:01'].");
    amxd_trans_set_value(bool, &transaction, "Active", true);

    assert_int_equal(amxd_trans_apply(&transaction, dm), 0);
    amxd_trans_clean(&transaction);

    amxut_bus_handle_events();

    ac_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].");
    assert_non_null(ac_obj);
    assert_non_null(ac_obj->priv);
}

void hosts_can_change_schedule_with_empty_starttime(void** state) {
    amxc_var_t* var = NULL;
    amxd_object_t* ac_obj = NULL;
    amxd_object_t* ac_sched_obj = NULL;
    amxc_var_t* check_data = NULL;
    amxd_trans_t transaction;
    time_tracker_t* tr = NULL;
    host_ac_info_t* info = NULL;
    amxc_var_t args;
    amxc_var_t ret;

    go_to_datetime_unchecked("1970-01-01T10:00:00Z");
    amxut_bus_handle_events();

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxd_trans_init(&transaction);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_select_pathf(&transaction, "Hosts.AccessControl.[PhysAddress=='80:E8:2C:2E:00:01'].Schedule.2.");
    amxd_trans_set_value(uint32_t, &transaction, "Duration", 5 * 60);

    assert_int_equal(amxd_trans_apply(&transaction, dm), 0);
    amxd_trans_clean(&transaction);

    ac_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].");
    ac_sched_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].Schedule.2.");
    check_data = build_check_arg(ac_obj, ac_sched_obj);

    // only the schedule-id is passed - so remove all others
    remove_schedule_data_from_check_args(check_data, var);
    expect_string(dummy_func, function_name, "scheduler-remove-schedule");
    expect_check(dummy_func, args, check_args, check_data);

    check_data = build_check_arg(ac_obj, ac_sched_obj);
    expect_string(dummy_func, function_name, "scheduler-add-schedule");
    expect_check(dummy_func, args, check_args, check_data);

    amxut_bus_handle_events();

    amxc_var_add_key(cstring_t, &args, "AccessControlId", "cpe-AccessControl-1");
    amxc_var_add_key(cstring_t, &args, "ScheduleId", "cpe-Schedule-2");

    mock_gmap_expect_setTag("the_key", "wanblock", NULL, gmap_traverse_this);
    assert_int_equal(amxm_execute_function(NULL, "host-scheduler", "ac-end-interval", &args, &ret), 0);

    mock_gmap_expect_clearTag("the_key", "wanblock", NULL, gmap_traverse_this);
    assert_int_equal(amxm_execute_function(NULL, "host-scheduler", "ac-start-interval", &args, &ret), 0);

    amxp_timers_calculate();
    amxp_timers_check();

    assert_non_null(ac_obj->priv);
    info = (host_ac_info_t*) ac_obj->priv;
    assert_non_null(info);
    tr = info->tr;
    assert_non_null(tr->timer);
    assert_int_equal(tr->remaining_time, 5 * 60);
    assert_string_equal(tr->schedule_id, "cpe-Schedule-2");
    assert_int_equal(amxp_timer_get_state(tr->timer), amxp_timer_running);

    go_to_datetime_unchecked("1970-01-01T10:04:59Z");
    amxut_bus_handle_events();

    assert_non_null(tr->timer);
    assert_int_equal(amxp_timer_remaining_time(tr->timer), 1000);
    assert_int_equal(tr->remaining_time, 5 * 60);
    assert_string_equal(tr->schedule_id, "cpe-Schedule-2");
    assert_int_equal(amxp_timer_get_state(tr->timer), amxp_timer_running);

    go_to_datetime_unchecked("1970-01-01T10:05:00Z");
    amxut_bus_handle_events();

    assert_non_null(tr->timer);
    assert_int_equal(amxp_timer_remaining_time(tr->timer), 0);
    assert_int_equal(tr->remaining_time, 0);
    assert_string_equal(tr->schedule_id, "cpe-Schedule-2");
    assert_int_equal(amxp_timer_get_state(tr->timer), amxp_timer_off);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void hosts_can_stop_schedule_with_empty_starttime(void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* ac_obj = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, "AccessControlId", "cpe-AccessControl-1");
    amxc_var_add_key(cstring_t, &args, "ScheduleId", "cpe-Schedule-2");

    mock_gmap_expect_setTag("the_key", "wanblock", NULL, gmap_traverse_this);

    assert_int_equal(amxm_execute_function(NULL, "host-scheduler", "ac-end-interval", &args, &ret), 0);

    ac_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].");
    assert_non_null(ac_obj);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void hosts_can_start_other_schedule_with_empty_starttime(void** state) {
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    mock_gmap_expect_clearTag("the_key", "wanblock", NULL, gmap_traverse_this);

    amxc_var_add_key(cstring_t, &args, "AccessControlId", "cpe-AccessControl-1");
    amxc_var_add_key(cstring_t, &args, "ScheduleId", "cpe-Schedule-4");

    assert_int_equal(amxm_execute_function(NULL, "host-scheduler", "ac-start-interval", &args, &ret), 0);

    amxut_bus_handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void hosts_ignore_schedule_since_schedule_ref_present(void** state) {
    amxc_var_t ac_params;
    amxc_var_t sched_params;
    amxc_var_t ret;

    amxc_var_t* var = NULL;
    amxd_object_t* ac_obj = NULL;
    amxc_var_t* check_data = NULL;
    amxc_var_t* check_data2 = NULL;

    amxc_var_init(&ac_params);
    amxc_var_init(&sched_params);
    amxc_var_init(&ret);
    amxc_var_set_type(&ac_params, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&sched_params, AMXC_VAR_ID_HTABLE);

    mock_gmap_expect_setTag("the_key", "wanblock", NULL, gmap_traverse_this);
    go_to_datetime_unchecked("1970-03-01T10:00:00Z");
    amxut_bus_handle_events();

    ac_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].");
    assert_non_null(ac_obj);

    // When setting the ScheduleRef on accesscontrol with policy allow
    amxc_var_add_key(cstring_t, &ac_params, "ScheduleRef", "Schedules.Schedule.1");
    assert_int_equal(amxb_set(bus_ctx, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].", &ac_params, &ret, 5), 0);

    // 1. The scheduler module is no longer used, so the 2 schedules of this accesscontrol object are removed from the module
    mock_gmap_expect_setTag("the_key", "wanblock", NULL, gmap_traverse_this);
    expect_string(dummy_func, function_name, "scheduler-remove-schedule");
    check_data = build_check_arg(ac_obj, amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].Schedule.[Alias == 'cpe-Schedule-2']."));
    // only the schedule - id is passed - so remove all others
    remove_schedule_data_from_check_args(check_data, var);
    expect_check(dummy_func, args, check_args, check_data);
    expect_string(dummy_func, function_name, "scheduler-remove-schedule");
    check_data2 = build_check_arg(ac_obj, amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].Schedule.[Alias == 'cpe-Schedule-4']."));
    // only the schedule - id is passed - so remove all others
    remove_schedule_data_from_check_args(check_data2, var);
    expect_check(dummy_func, args, check_args, check_data2);
    amxut_bus_handle_events();

    // 2. The libtr181-schedules library is now watching the Schedules object referenced by the ScheduleRef, once it becomes active the AccessControl is allowed again
    mock_gmap_expect_clearTag("the_key", "wanblock", NULL, gmap_traverse_this);
    amxc_var_add_key(cstring_t, &sched_params, "Status", "Active");
    assert_int_equal(amxb_set(bus_ctx, "Schedules.Schedule.1.", &sched_params, &ret, 5), 0);
    amxut_bus_handle_events();

    amxc_var_clean(&sched_params);
    amxc_var_clean(&ac_params);
    amxc_var_clean(&ret);
}

void hosts_start_schedule_since_schedule_ref_is_removed(void** state) {
    amxc_var_t params;
    amxc_var_t ret;

    amxc_var_t* var = NULL;
    amxd_object_t* ac_obj = NULL;
    amxc_var_t* check_data = NULL;
    amxc_var_t* check_data2 = NULL;

    amxc_var_init(&params);
    amxc_var_init(&ret);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);

    ac_obj = amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].");
    assert_non_null(ac_obj);

    amxc_var_add_key(cstring_t, &params, "ScheduleRef", "");
    assert_int_equal(amxb_set(bus_ctx, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].", &params, &ret, 5), 0);

    mock_gmap_expect_setTag("the_key", "wanblock", NULL, gmap_traverse_this);
    expect_string(dummy_func, function_name, "scheduler-add-schedule");
    check_data = build_check_arg(ac_obj, amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].Schedule.[Alias == 'cpe-Schedule-2']."));
    expect_check(dummy_func, args, check_args, check_data);

    mock_gmap_expect_setTag("the_key", "wanblock", NULL, gmap_traverse_this);
    expect_string(dummy_func, function_name, "scheduler-add-schedule");
    check_data2 = build_check_arg(ac_obj, amxd_dm_findf(dm, "Hosts.AccessControl.[PhysAddress == '80:E8:2C:2E:00:01'].Schedule.[Alias == 'cpe-Schedule-4']."));
    expect_check(dummy_func, args, check_args, check_data2);


    amxut_bus_handle_events();
    amxc_var_clean(&params);
    amxc_var_clean(&ret);
}
