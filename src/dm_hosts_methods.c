/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>
#include <debug/sahtrace_macros.h>
#include "hosts_manager.h"

#define ME "hosts-mngr"

amxd_status_t _RemoveInactiveDevices(UNUSED amxd_object_t* object,
                                     UNUSED amxd_function_t* func, amxc_var_t* args,
                                     amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    amxb_bus_ctx_t* ctx;
    int rv;

    ctx = amxb_be_who_has("Devices");
    when_null_trace(ctx, exit, ERROR, "gMap data model not found 'Devices.Device' - is gmap-server running?");
    rv = amxb_call(ctx, "Devices.", "removeInactiveDevices", args, ret, 5);
    when_false_trace(rv == amxd_status_ok, exit, ERROR, "Call to removeInactiveDevices failed (%d)", rv);
    status = amxd_status_ok;

exit:
    return status;
}

amxd_status_t _dm_read_stats(amxd_object_t* const object,
                             amxd_param_t* param,
                             amxd_action_t reason,
                             UNUSED const amxc_var_t* const args,
                             amxc_var_t* const retval,
                             UNUSED void* priv) {
    amxd_status_t ret = amxd_status_unknown_error;
    amxc_var_t result;
    amxc_var_t devicepath;
    amxb_bus_ctx_t* ctx = NULL;
    amxc_string_t path;
    amxd_object_t* parent = NULL;
    const char* prm = NULL;
    int rv = 0;

    amxc_var_init(&result);
    amxc_var_init(&devicepath);
    amxc_string_init(&path, 0);
    when_true_status(reason != action_param_read, exit, ret = amxd_status_function_not_implemented);

    parent = amxd_object_get_parent(object);
    when_null_trace(parent, exit, ERROR, "Couldn't retreive parent node");
    amxd_object_get_param(parent, "DeviceReference", &devicepath);

    ctx = amxb_be_who_has("Devices");
    when_null_trace(ctx, exit, ERROR, "gMap data model not found 'Devices.Device' - is gmap-server running?");
    prm = amxd_param_get_name(param);
    when_str_empty(prm, exit);
    amxc_string_setf(&path, "%sStats.%s", GET_CHAR(&devicepath, NULL), prm);
    rv = amxb_get(ctx, amxc_string_get(&path, 0), 0, &result, 1);
    if(rv != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "Couldn't retrieve Stats for Device: %s - is 'stats' tag present?", GET_CHAR(&devicepath, NULL));
        amxc_var_set(uint64_t, retval, 0);
    } else {
        amxc_var_set(uint64_t, retval, amxc_var_dyncast(uint64_t, GETP_ARG(&result, "0.0.0")));
    }
    ret = amxd_status_ok;

exit:
    amxc_string_clean(&path);
    amxc_var_clean(&devicepath);
    amxc_var_clean(&result);

    return ret;
}
