/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "hosts_accesscontroller.h"
#include "hosts_manager.h"

#define ME "hosts-utils"

uint64_t convert_str2mac(const char* mac_str) {
    uint8_t values[6] = { 0, 0, 0, 0, 0, 0};
    int i = 0;
    uint64_t mac = 0;

    i = sscanf(mac_str, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx%*c",
               &values[5], &values[4], &values[3],
               &values[2], &values[1], &values[0]);
    when_false(i == 6, exit);
    for(i = 5; i >= 0; i--) {
        mac |= values[i];
        mac = mac << 8;
    }
exit:
    return mac;
}

bool check_overlapping(const char* mac_addr_1,
                       const char* mac_mask_1,
                       const char* mac_addr_2,
                       const char* mac_mask_2) {
    uint64_t macaddr_tmp = 0;
    uint64_t macmask_tmp = 0;
    uint64_t macaddr_tmp_2 = 0;
    uint64_t macmask_tmp_2 = 0;
    uint64_t range_1 = -1;
    uint64_t range_1_reverse = -1;
    uint64_t range_2 = -1;
    uint64_t range_2_reverse = -1;

    when_str_empty_trace(mac_addr_1, exit, ERROR, "First MAC address is empty");
    when_str_empty_trace(mac_mask_1, exit, ERROR, "First MAC mask is empty");
    when_str_empty_trace(mac_addr_2, exit, ERROR, "Second MAC address is empty");
    when_str_empty_trace(mac_mask_2, exit, ERROR, "Second MAC mask is empty");

    macaddr_tmp = convert_str2mac(mac_addr_1);
    macaddr_tmp_2 = convert_str2mac(mac_addr_2);
    macmask_tmp = convert_str2mac(mac_mask_1);
    macmask_tmp_2 = convert_str2mac(mac_mask_2);

    range_1 = macaddr_tmp & macmask_tmp;
    range_1_reverse = macaddr_tmp & macmask_tmp_2;
    range_2 = macaddr_tmp_2 & macmask_tmp;
    range_2_reverse = macaddr_tmp_2 & macmask_tmp_2;
exit:
    return (range_1 == range_2) || (range_1_reverse == range_2_reverse);
}

amxc_var_t* get_prefix_var(void) {
    return amxo_parser_get_config(hosts_manager_get_parser(), "prefix_");
}
