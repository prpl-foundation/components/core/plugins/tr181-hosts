/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "hosts_manager.h"
#include "dm_hosts_accesscontrol.h"
#include "hosts_accesscontroller.h"
#include "hosts_scheduler.h"
#include "hosts_accesscontroller_info.h"

#include <gmap/gmap.h>
#include <debug/sahtrace_macros.h>

#define ME "hosts-dm-ac"

amxd_status_t _check_overlapping_rules(UNUSED amxd_object_t* const object,
                                       amxd_param_t* const p,
                                       UNUSED amxd_action_t reason,
                                       UNUSED const amxc_var_t* const args,
                                       UNUSED amxc_var_t* const retval,
                                       UNUSED void* priv) {
    amxd_status_t ret = amxd_status_unknown_error;
    amxd_object_t* hosts_tmpl = NULL;
    amxc_var_t* prefix = get_prefix_var();
    const char* mac_addr = NULL;
    const char* mac_mask = NULL;
    amxc_string_t phys_mask_name;

    amxc_string_init(&phys_mask_name, 0);

    when_true_status(reason != action_param_validate, exit, ret = amxd_status_function_not_implemented);
    when_null_trace(object, exit, ERROR, "Host access object is not given");
    hosts_tmpl = amxd_object_get_parent(object);
    when_null_trace(hosts_tmpl, exit, ERROR, "Cannot get all hosts control access object");

    amxc_string_setf(&phys_mask_name, "%sPhysAddressMask", amxc_var_constcast(cstring_t, prefix));

    if(strcmp(amxd_param_get_name(p), "PhysAddress") == 0) {
        mac_addr = GET_CHAR(args, NULL);
    } else {
        mac_addr = GET_CHAR(amxd_object_get_param_value(object, "PhysAddress"), NULL);
    }

    if(strcmp(amxd_param_get_name(p), amxc_string_get(&phys_mask_name, 0)) == 0) {
        mac_mask = GET_CHAR(args, NULL);
    } else {
        mac_mask = GET_CHAR(amxd_object_get_param_value(object, amxc_string_get(&phys_mask_name, 0)), NULL);
    }

    // accept empty string for validation, but the user need to change them to have a working access control
    when_str_empty_status(mac_addr, exit, ret = amxd_status_ok);
    when_str_empty_status(mac_mask, exit, ret = amxd_status_ok);

    ret = amxd_status_invalid_value;
    amxd_object_for_each(instance, host_it, hosts_tmpl) {
        amxd_object_t* host = amxc_container_of(host_it, amxd_object_t, it);
        if(host != object) {
            const char* host_mac_addr = GET_CHAR(amxd_object_get_param_value(host, "PhysAddress"), NULL);
            const char* host_mac_mask = GET_CHAR(amxd_object_get_param_value(host, amxc_string_get(&phys_mask_name, 0)), NULL);
            when_true_trace(check_overlapping(mac_addr, mac_mask, host_mac_addr, host_mac_mask), exit, WARNING, "Not allowing rule as overlap exist");
        }
    }
    ret = amxd_status_ok;
exit:
    amxc_string_clean(&phys_mask_name);
    return ret;
}

amxd_status_t _ac_removed(UNUSED amxd_object_t* const object,
                          UNUSED amxd_param_t* const p,
                          amxd_action_t reason,
                          UNUSED const amxc_var_t* const args,
                          UNUSED amxc_var_t* const retval,
                          UNUSED void* priv) {
    amxd_status_t ret = amxd_status_unknown_error;
    bool currect_access = amxd_object_get_value(bool, object, "CurrentAccess", NULL);
    host_ac_info_t* info = NULL;

    when_true_status(reason != action_object_destroy, exit, ret = amxd_status_function_not_implemented);
    when_null_trace(object, exit, ERROR, "Cannot get host access object.");
    info = (host_ac_info_t*) object->priv;
    when_null_trace(info, exit, ERROR, "No private info for access control host given");
    info->zombie = true;
    hosts_scheduler_remove_all_ac_schedules(info->obj);
    if(!currect_access) {
        host_ac_info_disable(info);
    }
    host_ac_info_clear(info);
    ret = amxd_status_ok;
exit:
    return ret;
}