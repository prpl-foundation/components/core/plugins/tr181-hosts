/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "hosts_manager.h"
#include "dm_hosts_accesscontrol.h"
#include "hosts_accesscontroller.h"
#include "hosts_scheduler.h"

#include <gmap/gmap.h>
#include <debug/sahtrace_macros.h>

#define ME "hosts-dm-ac"

typedef struct _check_data {
    amxd_object_t* current;
    uint32_t count;
    amxc_set_t input_set;
} check_data_t;

static void build_set_from_var(const amxc_var_t* var, amxc_set_t* set) {
    amxc_var_t tmp;
    amxc_var_init(&tmp);
    amxc_var_convert(&tmp, var, AMXC_VAR_ID_CSV_STRING);
    amxc_var_cast(&tmp, AMXC_VAR_ID_LIST);
    amxc_var_cast(&tmp, AMXC_VAR_ID_SSV_STRING);
    amxc_set_parse(set, GET_CHAR(&tmp, NULL));
    amxc_var_clean(&tmp);
}

static int check_schedule_instances(UNUSED amxd_object_t* object,
                                    amxd_object_t* mobject,
                                    void* priv) {
    check_data_t* data = (check_data_t*) priv;
    amxc_set_t set;
    amxd_param_t* param = NULL;
    when_null_trace(data, exit, ERROR, "No priv data found");

    amxc_set_init(&set, false);
    when_true(mobject == data->current, exit);

    param = amxd_object_get_param_def(mobject, "Day");
    build_set_from_var(&param->value, &set);
    amxc_set_intersect(&set, &data->input_set);

    if(!amxc_set_is_equal(&set, NULL)) {
        data->count++;
    }

exit:
    amxc_set_clean(&set);
    return 0;
}

amxd_status_t _check_schedule(amxd_object_t* const object,
                              UNUSED amxd_param_t* const p,
                              amxd_action_t reason,
                              UNUSED const amxc_var_t* const args,
                              UNUSED amxc_var_t* const retval,
                              UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_param_t* param = NULL;
    check_data_t data;

    amxc_set_init(&data.input_set, false);

    when_false_status(reason == action_object_validate, exit, status = amxd_status_function_not_implemented);
    when_true_status(amxd_object_get_type(object) == amxd_object_template, exit, status = amxd_status_ok);

    param = amxd_object_get_param_def(object, "Day");
    data.current = object;
    data.count = 0;

    build_set_from_var(&param->value, &data.input_set);
    amxd_object_for_all(amxd_object_get_parent(object), "[StartTime==''].", check_schedule_instances, &data);

    status = data.count > 0 ? amxd_status_invalid_value : amxd_status_ok;

exit:
    amxc_set_clean(&data.input_set);
    return status;
}

amxd_status_t _recalculateSchedules(UNUSED amxd_object_t* object,
                                    UNUSED amxd_function_t* func,
                                    UNUSED amxc_var_t* args,
                                    UNUSED amxc_var_t* ret) {
    amxd_status_t rc = amxd_status_ok;

    rc = hosts_recalculate_all_schedules();

    return rc;
}