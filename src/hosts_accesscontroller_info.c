/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "amxc/amxc_macros.h"
#include "hosts_accesscontroller.h"
#include "hosts_accesscontroller_info.h"

#define ME "host_ac_info"

static void host_ac_query_updated(gmap_query_t* query, const char* key, amxc_var_t* device, gmap_query_action_t action) {
    host_ac_info_t* info = NULL;
    amxc_var_t* lan_keys_htable = NULL;
    const char* phys_addr = NULL;
    amxc_var_t* ht_entry = NULL;

    when_str_empty_trace(key, exit, ERROR, "No devices key given");
    when_null_trace(query, exit, ERROR, "No query given");
    info = (host_ac_info_t*) query->data;
    when_null_trace(info, exit, ERROR, "No private info for Host access given");
    lan_keys_htable = info->lan_devices_ht;
    when_null_trace(lan_keys_htable, exit, ERROR, "No list for lan devices given");
    when_null_trace(device, exit, ERROR, "No devices given for query");

    phys_addr = GET_CHAR(device, "PhysAddress");

    switch(action) {
    case gmap_query_expression_start_matching:
        hosts_accesscontrol_enforce(info->obj, key, phys_addr);
        amxc_var_add_key(cstring_t, lan_keys_htable, key, phys_addr);
        break;
    case gmap_query_expression_stop_matching:
        hosts_accesscontrol_enforce(info->obj, key, phys_addr);
        ht_entry = amxc_var_take_key(lan_keys_htable, key);
        when_null_trace(ht_entry, exit, INFO, "No entry found in devices htable, nothing will be done");
        amxc_var_delete(&ht_entry);
        break;
    default:
        SAH_TRACEZ_INFO(ME, "Device already registered");
    }
exit:
    return;
}

static void host_ac_info_clean_all_devices(host_ac_info_t* info) {
    amxc_var_t* lan_keys_htable = NULL;
    when_null_trace(info, exit, ERROR, "No priv data given to clean wanblock devices' tags");
    lan_keys_htable = info->lan_devices_ht;
    when_null_trace(lan_keys_htable, exit, ERROR, "No list for lan devices given");

    amxc_var_for_each(dev_entry, lan_keys_htable) {
        const char* dev_key = amxc_var_key(dev_entry);
        if(dev_key == NULL) {
            SAH_TRACEZ_ERROR(ME, "No key given for an entry in htable");
            continue;
        }
        if(hosts_accesscontroller_allow_host(info->obj, dev_key, info->zombie)) {
            SAH_TRACEZ_ERROR(ME, "Unable to allow host for %s", dev_key);
        }
    }
exit:
    return;
}

/**
 * @brief
 * Create the query to get all LAN devices for the access control
 *
 * @param info the host access control private data
 * @return int 0 if success
 */
static int host_ac_open_gmap_devices_query(host_ac_info_t* info, const char* phys_addr, const char* mask) {
    int ret = 1;
    amxc_string_t regex;
    amxc_string_t name;

    amxc_string_init(&regex, 0);
    amxc_string_init(&name, 0);

    when_null_trace(info, exit, ERROR, "No private information found for host access");

    when_str_empty_trace(phys_addr, exit, ERROR, "No MAC address given");
    when_str_empty_trace(mask, exit, ERROR, "No mask given");

    amxc_string_appendf(&regex, "mac_in_mask('%s','%s')", phys_addr, mask);
    amxc_string_appendf(&name, "Host_ac-%s", amxd_object_get_name(info->obj, AMXD_OBJECT_NAMED));

    SAH_TRACEZ_INFO(ME, "expression : %s", amxc_string_get(&regex, 0));

    if(info->lan_devices_query != NULL) {
        gmap_query_close(info->lan_devices_query);
        info->lan_devices_query = NULL;
    }
    gmap_query_open_ext_2(&info->lan_devices_query, amxc_string_get(&regex, 0),
                          amxc_string_get(&name, 0), GMAP_QUERY_IGNORE_DEVICE_UPDATED,
                          host_ac_query_updated, info);
    when_null_trace(info->lan_devices_query, exit, ERROR, "Unable to open gmap query");
    ret = 0;
exit:
    amxc_string_clean(&regex);
    amxc_string_clean(&name);
    return ret;
}

/**
 * @brief
 * Allocates and populates the memory used by a access control private data.
 *
 * @param host_ac The related host access controller object.
 * @return host_ac_info* Pointer to pool item, NULL elsewhere
 */
host_ac_info_t* host_ac_info_create(amxd_object_t* host_ac) {
    host_ac_info_t* info = (host_ac_info_t*) calloc(1, sizeof(host_ac_info_t));
    when_null_trace(info, exit, ERROR, "Cannot allocate memory for pool private information.");

    info->obj = host_ac;
    amxc_var_new(&(info->lan_devices_ht));
    amxc_var_set_type(info->lan_devices_ht, AMXC_VAR_ID_HTABLE);
    info->lan_devices_query = NULL;
    info->zombie = false;
    info->schedule_status = SCHEDULE_STATUS_EMPTY;
exit:
    return info;
}

/**
 * @brief
 * Cleans up the memory used by a host access controller private data.
 * Closes the gmap query if needed.
 *
 * @param info The host access controller private info to clean
 */
void host_ac_info_clear(host_ac_info_t* info) {
    when_null_trace(info, exit, WARNING, "No host access controller private data to free.");
    if(info->lan_devices_query != NULL) {
        gmap_query_close(info->lan_devices_query);
        info->lan_devices_query = NULL;
    }
    if(info->tr != NULL) {
        amxp_timer_delete(&info->tr->timer);
        free(info->tr->schedule_id);
        free(info->tr);
        info->tr = NULL;
    }
    amxc_var_delete(&(info->lan_devices_ht));
    info->lan_devices_ht = NULL;
    info->obj = NULL;

    schedule_close(&(info->schedule));
    free(info);
exit:
    return;
}

/**
 * @brief
 * Subscribe to gmap queries.
 *
 * @param info the host access control private data.
 * @return int 0 if success
 */
int host_ac_info_enable(host_ac_info_t* info, const char* phys_addr, const char* mask) {
    int ret = 1;
    when_null_trace(info, exit, ERROR, "No private data given for host access control");
    when_str_empty_trace(phys_addr, exit, WARNING, "No MAC address given");
    when_str_empty_trace(mask, exit, WARNING, "No mask given");

    ret = host_ac_open_gmap_devices_query(info, phys_addr, mask);
exit:
    return ret;
}

/**
 * @brief
 * UNsubscribe to gmap queries.
 *
 * @param info the host access control private data.
 */
void host_ac_info_disable(host_ac_info_t* info) {
    when_null_trace(info, exit, INFO, "No private data given for host access control, nothing is done");
    if(info->lan_devices_query != NULL) {
        gmap_query_close(info->lan_devices_query);
        info->lan_devices_query = NULL;
    }
    host_ac_info_clean_all_devices(info);
    amxc_var_delete(&(info->lan_devices_ht));
    info->lan_devices_ht = NULL;
    amxc_var_new(&(info->lan_devices_ht));
    amxc_var_set_type(info->lan_devices_ht, AMXC_VAR_ID_HTABLE);
exit:
    return;
}
