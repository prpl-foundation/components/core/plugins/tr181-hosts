/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>

#include "hosts_manager.h"
#include "hosts_scheduler.h"
#include "hosts_accesscontroller.h"
#include "hosts_accesscontroller_info.h"
#include "hosts_time_sync.h"

#include <amxm/amxm.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>

#include <debug/sahtrace_macros.h>

#define ME "hosts-scheduler"

// module names
#define MOD_SCHEDULER_CTRL "scheduler-ctrl"

typedef struct _scheduler {
    amxd_dm_t* host_dm;
    amxo_parser_t* parser;
    bool mod_are_loaded;
    const char* controller;
} scheduler_t;

static scheduler_t scheduler;

static int get_scheduler_info(amxd_object_t* ac_obj, amxd_object_t* ac_schedule_obj, amxc_var_t* ac) {
    amxc_var_t* schedule = NULL;
    amxc_var_t* var = NULL;

    amxd_object_get_params(ac_obj, ac, amxd_dm_access_protected);
    var = amxc_var_get_key(ac, "CurrentAccess", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_delete(&var);
    schedule = amxc_var_add_new_key(ac, "schedule");
    amxc_var_set_type(schedule, AMXC_VAR_ID_HTABLE);

    if(ac_schedule_obj != NULL) {
        amxd_object_get_params(ac_schedule_obj, schedule, amxd_dm_access_protected);
        var = amxc_var_get_key(schedule, "Alias", AMXC_VAR_FLAG_DEFAULT);
        amxc_var_take_it(var);
        amxc_var_set_key(schedule, "ScheduleId", var, AMXC_VAR_FLAG_DEFAULT);
    }

    var = amxc_var_get_key(ac, "Alias", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_take_it(var);
    amxc_var_set_key(ac, "AccessControlId", var, AMXC_VAR_FLAG_DEFAULT);

    // if ac policy is set to deny - the scheduler for this physaddress must be disabled
    if(strcmp(GET_CHAR(ac, "AccessPolicy"), "Deny") == 0) {
        var = GET_ARG(ac, "Enable");
        amxc_var_set(bool, var, false);
    }
    return 0;
}

static int add_schedule(UNUSED amxd_object_t* hosts_obj, amxd_object_t* schedule_obj, UNUSED void* priv) {
    int rv = -1;
    amxd_object_t* ac_obj = NULL;

    ac_obj = amxd_object_findf(schedule_obj, ".^.^");
    when_null_trace(ac_obj, exit, ERROR, "no access control object  given");

    rv = hosts_scheduler_add_ac_schedule(ac_obj, schedule_obj);
    when_false_trace(rv, exit, ERROR, "Unable to add ac schedule for %s", amxd_object_get_name(ac_obj, AMXD_OBJECT_NAMED));
exit:
    return rv;
}

int hosts_scheduler_init(amxd_dm_t* dm, amxo_parser_t* parser) {
    int rv = 0;
    amxm_shared_object_t* so = NULL;
    amxc_string_t mod_path;
    const amxc_var_t* controllers = NULL;
    amxc_var_t lcontrollers;
    amxc_var_t var;
    amxc_var_t ret;
    amxm_module_t* mod = NULL;
    amxd_object_t* hosts = NULL;

    const char* mod_dir = GETP_CHAR(&parser->config, "hosts-manager.mod-dir");

    amxc_string_init(&mod_path, 0);
    amxc_var_init(&lcontrollers);
    amxc_var_init(&var);
    amxc_var_init(&ret);

    hosts = amxd_dm_findf(dm, "Hosts.");
    scheduler.controller = GET_CHAR(amxd_object_get_param_value(hosts, "SchedulerController"), NULL);
    scheduler.mod_are_loaded = true;
    scheduler.host_dm = dm;
    scheduler.parser = parser;

    controllers = amxd_object_get_param_value(hosts, "SupportedSchedulerControllers");

    amxc_var_convert(&lcontrollers, controllers, AMXC_VAR_ID_LIST);
    amxc_var_for_each(controller, &lcontrollers) {
        const char* name = GET_CHAR(controller, NULL);
        amxc_string_setf(&mod_path, "%s/%s.so", mod_dir, name);
        SAH_TRACEZ_INFO(ME, "Loading controller '%s' (file = %s)", name, amxc_string_get(&mod_path, 0));
        rv = amxm_so_open(&so, name, amxc_string_get(&mod_path, 0));
        if(rv != 0) {
            SAH_TRACEZ_ERROR(ME, "Loading controller '%s' failed", name);
        }
    }

    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);

    rv = amxm_execute_function(scheduler.controller, MOD_SCHEDULER_CTRL, "scheduler-init", &var, &ret);
    when_failed_trace(rv, exit, ERROR, "scheduler-init from '%s' failed", scheduler.controller);

    amxd_object_for_all(hosts, ".AccessControl.*.Schedule.*", add_schedule, NULL);

    // Load modules
    amxm_module_register(&mod, NULL, "host-scheduler");
    amxm_module_add_function(mod, "ac-start-interval", hosts_accesscontroller_start_interval_wrapper);
    amxm_module_add_function(mod, "ac-end-interval", hosts_accesscontroller_end_interval_wrapper);

    //Wait for Time.Status to get to Synchronized
    wait_for_time();

    // Init Library for ScheduleRef
    schedule_initialize();

exit:
    amxc_string_clean(&mod_path);
    amxc_var_clean(&lcontrollers);
    amxc_var_clean(&var);
    amxc_var_clean(&ret);
    return rv;
}

int hosts_scheduler_clean(void) {
    int rv = -1;
    amxc_var_t var;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);

    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);

    rv = amxm_execute_function(scheduler.controller, MOD_SCHEDULER_CTRL, "scheduler-clean", &var, &ret);
    when_failed_trace(rv, exit, ERROR, "scheduler-clean from '%s' failed", scheduler.controller);

    schedule_cleanup();

    scheduler.host_dm = NULL;
    scheduler.parser = NULL;
    scheduler.controller = NULL;
    scheduler.mod_are_loaded = false;

    amxm_close_all();
exit:
    amxc_var_clean(&var);
    amxc_var_clean(&ret);
    return rv;
}

int hosts_scheduler_add_all_ac_schedules(amxd_object_t* ac_obj) {
    amxd_object_for_all(ac_obj, ".Schedule.*", add_schedule, NULL);
    return 0;
}

int hosts_scheduler_update_ac_schedule(amxd_object_t* ac_obj) {
    int rv = -1;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_null_trace(ac_obj, exit, ERROR, "Access control object is null to update scheduler");

    get_scheduler_info(ac_obj, NULL, &args);

    when_false_trace(scheduler.mod_are_loaded, exit, ERROR, "MOD is not loaded");
    rv = amxm_execute_function(scheduler.controller, MOD_SCHEDULER_CTRL, "scheduler-change-schedule", &args, &ret);
    when_failed_trace(rv, exit, ERROR, "scheduler-change-schedule from '%s' failed", scheduler.controller);

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    return rv;
}

/* Check if there is a schedule that is currently active. */
bool hosts_scheduler_check_ac_schedule(amxd_object_t* ac_obj) {
    int rv = -1;
    bool act = true;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_null_trace(ac_obj, exit, ERROR, "Access control object is null");
    get_scheduler_info(ac_obj, NULL, &args);
    when_false_trace(scheduler.mod_are_loaded, exit, ERROR, "MOD is not loaded");
    rv = amxm_execute_function(scheduler.controller, MOD_SCHEDULER_CTRL, "scheduler-check-schedule", &args, &ret);
    when_failed_trace(rv, exit, ERROR, "scheduler-check-schedule from '%s' failed", scheduler.controller);
    act = amxc_var_constcast(bool, &ret);

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&args);

    return act;
}

int hosts_scheduler_remove_all_ac_schedules(amxd_object_t* ac_obj) {
    int rv = -1;
    amxc_var_t var;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);

    when_null(ac_obj, exit);

    get_scheduler_info(ac_obj, NULL, &var);

    when_false_trace(scheduler.mod_are_loaded, exit, ERROR, "MOD is not loaded");
    rv = amxm_execute_function(scheduler.controller, MOD_SCHEDULER_CTRL, "scheduler-reset", &var, &ret);
    when_failed_trace(rv, exit, ERROR, "scheduler-reset from '%s' failed", scheduler.controller);

exit:
    amxc_var_clean(&var);
    amxc_var_clean(&ret);

    return rv;
}

int hosts_scheduler_add_ac_schedule(amxd_object_t* ac_obj,
                                    amxd_object_t* ac_schedule_obj) {
    int rv = 0;
    amxc_var_t ret;
    amxc_var_t args;

    amxc_var_init(&ret);
    amxc_var_init(&args);

    when_null_trace(ac_obj, exit, WARNING, "No access controller object link to schedule object");
    when_null_trace(ac_schedule_obj, exit, WARNING, "No schedule object given");

    get_scheduler_info(ac_obj, ac_schedule_obj, &args);

    when_false_trace(scheduler.mod_are_loaded, exit, ERROR, "MOD is not loaded");
    rv = amxm_execute_function(scheduler.controller, MOD_SCHEDULER_CTRL, "scheduler-add-schedule", &args, &ret);
    when_failed_trace(rv, exit, ERROR, "scheduler-add-schedule from '%s' failed", scheduler.controller);

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    return rv;
}

int hosts_scheduler_change_ac_schedule(amxd_object_t* ac_obj,
                                       amxd_object_t* ac_schedule_obj) {
    int rv = 0;
    amxc_var_t ret;
    amxc_var_t args;

    amxc_var_init(&ret);
    amxc_var_init(&args);

    get_scheduler_info(ac_obj, ac_schedule_obj, &args);

    when_false_trace(scheduler.mod_are_loaded, exit, ERROR, "MOD is not loaded");
    rv = amxm_execute_function(scheduler.controller, MOD_SCHEDULER_CTRL, "scheduler-change-schedule", &args, &ret);
    when_failed_trace(rv, exit, ERROR, "scheduler-change-schedule from '%s' failed", scheduler.controller);

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    return rv;
}

int hosts_scheduler_remove_ac_schedule(amxd_object_t* ac_obj,
                                       const char* schedule_id) {
    int rv = -1;
    amxc_var_t ret;
    amxc_var_t args;
    amxc_var_t* schedule = NULL;

    amxc_var_init(&ret);
    amxc_var_init(&args);

    get_scheduler_info(ac_obj, NULL, &args);
    schedule = amxc_var_get_key(&args, "schedule", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_add_key(cstring_t, schedule, "ScheduleId", schedule_id);

    when_false_trace(scheduler.mod_are_loaded, exit, ERROR, "MOD is not loaded");
    rv = amxm_execute_function(scheduler.controller, MOD_SCHEDULER_CTRL, "scheduler-remove-schedule", &args, &ret);
    when_failed_trace(rv, exit, ERROR, "scheduler-remove-schedule from '%s' failed", scheduler.controller);

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&args);

    return rv;
}

int hosts_recalculate_all_schedules(void) {
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    rv = amxm_execute_function(scheduler.controller, MOD_SCHEDULER_CTRL, "scheduler-recalculate-schedule", &args, &ret);
    when_failed_trace(rv, exit, WARNING, "Mod %s, func 'scheduler-recalculate' returned %d", scheduler.controller, rv);

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&args);

    return rv;
}
