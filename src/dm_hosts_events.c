/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "hosts_manager.h"
#include "dm_hosts_accesscontrol.h"
#include "hosts_accesscontroller.h"
#include "hosts_scheduler.h"

#include <gmap/gmap.h>
#include "gmap/gmap_devices.h"
#include <debug/sahtrace_macros.h>

#define ME "hosts-dm-ac"

static amxd_object_t* find_ac_by_host(amxd_object_t* host_obj) {
    amxd_object_t* acs_obj = amxd_object_findf(host_obj, ".^.^.AccessControl.");
    amxc_var_t* prefix = get_prefix_var();
    amxc_string_t phys_mask_name;
    amxd_object_t* ac_obj = NULL;
    const char* host_mac = NULL;
    uint64_t host_mac_byte = 0;

    amxc_string_init(&phys_mask_name, 0);

    when_null_trace(acs_obj, exit, WARNING, "Cannot get list of access controller object");

    when_null_trace(prefix, exit, ERROR, "No prefix given");
    amxc_string_setf(&phys_mask_name, "%sPhysAddressMask", amxc_var_constcast(cstring_t, prefix));

    host_mac = GET_CHAR(amxd_object_get_param_value(host_obj, "PhysAddress"), NULL);
    when_str_empty_trace(host_mac, exit, ERROR, "Cannot get the phys address of the host");

    host_mac_byte = convert_str2mac(host_mac);

    amxd_object_for_each(instance, it, acs_obj) {
        amxd_object_t* obj = amxc_llist_it_get_data(it, amxd_object_t, it);
        const char* macaddr = GET_CHAR(amxd_object_get_param_value(obj, "PhysAddress"), NULL);
        const char* macaddr_mask = GET_CHAR(amxd_object_get_param_value(obj, amxc_string_get(&phys_mask_name, 0)), NULL);
        uint64_t macaddr_byte = 0;
        uint64_t macaddr_mask_byte = 0;
        uint64_t intersect = 1;

        if(!str_empty(macaddr)) {
            when_str_empty_trace(macaddr_mask, exit, ERROR, "Cannot get the PhysAddressMask of one AccessControl object");

            macaddr_byte = convert_str2mac(macaddr);
            macaddr_mask_byte = convert_str2mac(macaddr_mask);

            intersect = host_mac_byte & (macaddr_byte & macaddr_mask_byte);
            if(intersect != 0) {
                ac_obj = obj;
                goto exit;
            }
        }
    }
exit:
    amxc_string_clean(&phys_mask_name);
    return ac_obj;
}

static cstring_t find_gmapkey_from_host(amxd_object_t* host_obj) {
    const amxc_var_t* tmp = amxd_object_get_param_value(host_obj, "PhysAddress");
    const cstring_t hostmac = amxc_var_constcast(cstring_t, tmp);
    cstring_t key = NULL;
    when_null_trace(hostmac, exit, ERROR, "Couldn't parse PhysAddress");
    key = gmap_devices_findByMac(hostmac);

exit:
    return key;
}

void _hosts_active_changed(UNUSED const cstring_t const event_name,
                           const amxc_var_t* const event_data,
                           UNUSED void* const priv) {
    amxd_object_t* host_obj = NULL;
    amxd_object_t* ac_obj = NULL;
    bool active = GETP_BOOL(event_data, "parameters.Active.to");

    host_obj = amxd_dm_signal_get_object(hosts_manager_get_dm(), event_data);
    when_null_trace(host_obj, exit, ERROR, "No Host object found");

    ac_obj = find_ac_by_host(host_obj);
    when_null_trace(ac_obj, exit, INFO, "No access controller bound to the host %s",
                    amxd_object_get_name(host_obj, AMXD_OBJECT_NAMED))

    if(active) {
        hosts_scheduler_flex_start_timer(ac_obj);
    } else {
        hosts_scheduler_flex_stop_timer(ac_obj);
    }

exit:
    return;
}

void _hosts_protected_changed(UNUSED const cstring_t const event_name,
                              const amxc_var_t* const event_data,
                              UNUSED void* const priv) {
    amxd_object_t* host_obj = NULL;
    amxc_string_t param;
    bool protected;
    cstring_t dev_key = NULL;
    const char* prefix = GET_CHAR(amxo_parser_get_config(hosts_manager_get_parser(), "prefix_"), NULL);

    amxc_string_init(&param, 0);
    amxc_string_setf(&param, "parameters.%sProtected.to", prefix);

    protected = GETP_BOOL(event_data, amxc_string_get(&param, 0));
    host_obj = amxd_dm_signal_get_object(hosts_manager_get_dm(), event_data);
    when_null_trace(host_obj, exit, ERROR, "No Host object found");
    dev_key = find_gmapkey_from_host(host_obj);
    when_null_trace(dev_key, exit, ERROR, "No gmap device key found");

    if(protected) {
        gmap_device_setTag(dev_key, "protected", NULL, gmap_traverse_this);
    } else {
        gmap_device_clearTag(dev_key, "protected", NULL, gmap_traverse_this);
    }
    free(dev_key);
exit:
    amxc_string_clean(&param);

    return;
}

void _hosts_added(UNUSED const cstring_t const event_name,
                  const amxc_var_t* const event_data,
                  UNUSED void* const priv) {
    amxd_object_t* host_obj = NULL;
    amxd_object_t* ac_obj = NULL;
    bool active = GETP_BOOL(event_data, "parameters.Active");

    host_obj = amxd_dm_signal_get_object(hosts_manager_get_dm(), event_data);
    when_null_trace(host_obj, exit, ERROR, "No Host object found");

    ac_obj = find_ac_by_host(host_obj);

    if(active) {
        hosts_scheduler_flex_start_timer(ac_obj);
    } else {
        hosts_scheduler_flex_stop_timer(ac_obj);
    }
exit:
    return;
}

void _hosts_removed(UNUSED const cstring_t const event_name,
                    const amxc_var_t* const event_data,
                    UNUSED void* const priv) {
    amxd_object_t* host_obj = NULL;
    amxd_object_t* ac_obj = NULL;
    host_obj = amxd_dm_signal_get_object(hosts_manager_get_dm(), event_data);
    when_null_trace(host_obj, exit, ERROR, "No Host object found");

    ac_obj = find_ac_by_host(host_obj);

    hosts_scheduler_flex_stop_timer(ac_obj);
exit:
    return;
}
