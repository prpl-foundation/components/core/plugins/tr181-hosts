/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdlib.h>
#include <string.h>

#include "hosts_manager.h"
#include "hosts_scheduler.h"
#include "hosts_accesscontroller.h"
#include "hosts_accesscontroller_info.h"

#include <amxm/amxm.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>

#include <debug/sahtrace_macros.h>

#define ME "hosts-scheduler"

static void hosts_scheduler_flex_time_expired(UNUSED amxp_timer_t* timer, void* priv) {
    amxd_object_t* ac_obj = (amxd_object_t*) priv;
    host_ac_info_t* info = (host_ac_info_t*) ac_obj->priv;

    when_null_trace(info, exit, ERROR, "No private data initialised");

    // time is up - block device
    amxc_var_for_each(entry, info->lan_devices_ht) {
        SAH_TRACEZ_NOTICE(ME, "Flex time is up - blocking host physaddress '%s'", GET_CHAR(entry, NULL));
        hosts_accesscontroller_block_host(ac_obj, amxc_var_key(entry));
    }
    info->tr->remaining_time = 0;
exit:
    return;
}

bool hosts_scheduler_is_flex_time(amxd_object_t* ac_sched_obj) {
    const amxc_var_t* var_start_time = amxd_object_get_param_value(ac_sched_obj, "StartTime");
    const char* start_time = GET_CHAR(var_start_time, NULL);

    return (start_time == NULL || *start_time == 0);
}

bool hosts_scheduler_is_flex_active(amxd_object_t* ac_sched_obj) {
    bool retval = false;
    amxd_object_t* ac_obj = amxd_object_findf(ac_sched_obj, ".^.^.");
    host_ac_info_t* info = NULL;
    when_null(ac_obj, exit);
    info = (host_ac_info_t*) ac_obj->priv;
    when_null(info, exit);
    retval = (info->tr != NULL);

exit:
    return retval;
}

void hosts_scheduler_flex_time_start_day(amxd_object_t* ac_sched_obj) {
    amxd_object_t* ac_obj = amxd_object_findf(ac_sched_obj, ".^.^.");
    host_ac_info_t* info = NULL;
    time_tracker_t* tr = NULL;
    const char* schedule_id = amxd_object_get_name(ac_sched_obj, AMXD_OBJECT_NAMED);

    when_null_trace(ac_obj, exit, ERROR, "No access controller object found for schedule object");
    info = (host_ac_info_t*) ac_obj->priv;
    when_null_trace(info, exit, ERROR, "No access controller private data initialised")

    // The day started, allocate the time tracker and set it on the access control object
    tr = (time_tracker_t*) calloc(1, sizeof(time_tracker_t));
    when_null_trace(tr, exit, ERROR, "Cannot initialise time tracker");

    amxp_timer_new(&tr->timer, hosts_scheduler_flex_time_expired, ac_obj);
    tr->remaining_time = amxd_object_get_value(uint32_t, ac_sched_obj, "Duration", NULL);
    tr->schedule_id = strdup(schedule_id);
    info->tr = tr;

    SAH_TRACEZ_INFO(ME, "Day started or schedule just added/changed '%s' - initiate flex timer",
                    schedule_id);

    // A access control schedule without a starttime will allow the devices all day
    // when the timer expires the devices is blocked for the rest of the day active or inactive
    amxp_timer_start(tr->timer, tr->remaining_time * 1000);

    amxc_var_for_each(entry, info->lan_devices_ht) {
        hosts_accesscontroller_allow_host(ac_obj, amxc_var_key(entry), info->zombie);
    }

exit:
    return;
}

void hosts_scheduler_flex_time_stop_day(amxd_object_t* ac_sched_obj) {
    amxd_object_t* ac_obj = amxd_object_findf(ac_sched_obj, ".^.^.");
    host_ac_info_t* info = NULL;
    time_tracker_t* tr = NULL;

    when_null_trace(ac_obj, exit, ERROR, "No access controller object found for schedule object");
    info = (host_ac_info_t*) ac_obj->priv;
    when_null_trace(info, exit, ERROR, "No access controller private data initialised");


    tr = info->tr;
    when_null_trace(tr, exit, WARNING, "Stopping a flex timer that did not start or was already stopped");

    // The day is over, so remove the time tracker.
    amxp_timer_delete(&tr->timer);
    free(tr->schedule_id);
    free(tr);
    info->tr = NULL;

    // And default back to blocking the device.
    amxc_var_for_each(entry, info->lan_devices_ht) {
        const char* phy_address = GET_CHAR(entry, NULL);
        SAH_TRACEZ_INFO(ME, "Flex time schedule available for '%s' - day ended", phy_address);
        hosts_accesscontroller_block_host(ac_obj, amxc_var_key(entry));
    }

exit:
    return;
}

void hosts_scheduler_flex_start_timer(amxd_object_t* ac_obj) {
    host_ac_info_t* info = NULL;
    time_tracker_t* tr = NULL;

    when_null_trace(ac_obj, exit, ERROR, "Access controller object null to start a flex timer")
    info = (host_ac_info_t*) ac_obj->priv;
    when_null_trace(info, exit, ERROR, "No access controller private data initialised");
    tr = info->tr;
    when_null_trace(tr, exit, ERROR, "No time tracker to start");

    SAH_TRACEZ_INFO(ME, "Access controller '%s' is active - start/continue flex timer - time remaining = %d",
                    amxd_object_get_name(ac_obj, AMXD_OBJECT_NAMED), tr->remaining_time);

    if(tr->remaining_time > 0) {
        amxp_timer_start(tr->timer, tr->remaining_time * 1000);
    }

exit:
    return;
}

void hosts_scheduler_flex_stop_timer(amxd_object_t* ac_obj) {
    host_ac_info_t* info = NULL;
    time_tracker_t* tr = NULL;

    when_null_trace(ac_obj, exit, WARNING, "Access controller object null to stop flex timer");
    info = (host_ac_info_t*) ac_obj->priv;
    when_null_trace(info, exit, ERROR, "No access controller private data initialised");
    tr = info->tr;
    when_null_trace(tr, exit, WARNING, "No time tracker to stop");

    if(amxp_timer_get_state(tr->timer) == amxp_timer_running) {
        amxp_timers_calculate();
        tr->remaining_time = (amxp_timer_remaining_time(tr->timer) / 1000);
    }
    SAH_TRACEZ_INFO(ME, "Device '%s' is inactive - stop flex timer - time remaining = %d",
                    amxd_object_get_name(ac_obj, AMXD_OBJECT_NAMED), tr->remaining_time);

    amxp_timer_stop(tr->timer);

exit:
    return;
}
