/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "hosts_manager.h"
#include "dm_hosts_accesscontrol.h"
#include "hosts_accesscontroller.h"
#include "hosts_scheduler.h"
#include "hosts_accesscontroller_info.h"

#include <gmap/gmap.h>
#include <debug/sahtrace_macros.h>

#define ME "hosts-dm-ac"

static void ac_object_flag(amxd_object_t* object, const char* flag, bool set) {
    amxc_var_t params;

    amxc_var_init(&params);

    when_null_trace(object, exit, ERROR, "No object to unset flag given");
    when_str_empty_trace(flag, exit, ERROR, "No flag to unset given");

    amxd_object_get_params(object, &params, amxd_dm_access_protected);

    amxc_var_for_each(param_var, &params) {
        if(amxc_var_type_of(param_var) != AMXC_VAR_ID_HTABLE) {
            amxd_param_t* param = amxd_object_get_param_def(object, amxc_var_key(param_var));
            if(set) {
                amxd_param_set_flag(param, flag);
            } else {
                amxd_param_unset_flag(param, flag);
            }
        } else {
            amxc_var_for_each(entry, param_var) {
                amxd_object_t* child = amxd_object_findf(object, "%s.", amxc_var_key(entry));
                ac_object_flag(child, flag, set);
            }
        }
    }

exit:
    amxc_var_clean(&params);
    return;
}

static void ac_object_persistent_attr(amxd_object_t* const object, bool set) {
    amxc_var_t params;

    amxc_var_init(&params);

    when_null_trace(object, exit, ERROR, "No object to unset flag given");

    amxd_object_get_params(object, &params, amxd_dm_access_protected);

    amxd_object_set_attr(object, amxd_oattr_persistent, set);

    amxc_var_for_each(param_var, &params) {
        if(amxc_var_type_of(param_var) != AMXC_VAR_ID_HTABLE) {
            amxd_param_t* param = amxd_object_get_param_def(object, amxc_var_key(param_var));
            amxd_param_set_attr(param, amxd_pattr_persistent, set);
        } else {
            amxc_var_for_each(entry, param_var) {
                amxd_object_t* const child = amxd_object_findf(object, "%s.", amxc_var_key(entry));
                ac_object_persistent_attr(child, set);
            }
        }
    }

exit:
    amxc_var_clean(&params);
    return;
}

void _ac_event_ac_added(UNUSED const cstring_t const event_name,
                        const amxc_var_t* const event_data,
                        UNUSED void* const priv) {
    amxd_dm_t* dm = NULL;
    amxd_object_t* templ = NULL;
    amxd_object_t* host_ac = NULL;
    amxc_var_t* prefix = get_prefix_var();
    const char* name = NULL;
    const char* phy_address = GETP_CHAR(event_data, "parameters.PhysAddress");
    const char* phy_address_mask = NULL;
    const char* origin = NULL;
    amxc_string_t phys_mask_name;
    amxc_string_t origin_name;

    amxc_string_init(&phys_mask_name, 0);
    amxc_string_init(&origin_name, 0);

    when_null_trace(prefix, exit, ERROR, "No prefix given");
    amxc_string_setf(&phys_mask_name, "parameters.%sPhysAddressMask", amxc_var_constcast(cstring_t, prefix));
    amxc_string_setf(&origin_name, "parameters.%sOrigin", amxc_var_constcast(cstring_t, prefix));

    when_null_trace(event_data, exit, ERROR, "No data given for the event");
    name = GET_CHAR(event_data, "name");
    when_null_trace(name, exit, ERROR, "Cannot get object name.");

    dm = hosts_manager_get_dm();
    templ = amxd_dm_signal_get_object(dm, event_data);
    when_null_trace(templ, exit, ERROR, "Cannot get template for host access object.");
    host_ac = amxd_object_get_instance(templ, name, 0);
    when_null_trace(host_ac, exit, ERROR, "Cannot get host access object.");

    host_ac->priv = host_ac_info_create(host_ac);
    when_null_trace(host_ac->priv, exit, ERROR, "Unable to creat priv data for host access");

    phy_address_mask = GETP_CHAR(event_data, amxc_string_get(&phys_mask_name, 0));
    origin = GETP_CHAR(event_data, amxc_string_get(&origin_name, 0));
    host_ac_info_enable(host_ac->priv, phy_address, phy_address_mask);

    if(strcmp(origin, "User") != 0) {
        ac_object_flag(host_ac, "upc", false);
        ac_object_persistent_attr(host_ac, strcmp(origin, "Controller") == 0);
    }
exit:
    amxc_string_clean(&phys_mask_name);
    amxc_string_clean(&origin_name);
    return;
}

void _ac_event_ac_changed(UNUSED const cstring_t const event_name,
                          const amxc_var_t* const event_data,
                          UNUSED void* const priv) {
    amxd_object_t* ac_obj = NULL;
    host_ac_info_t* info = NULL;
    amxc_var_t* prefix = get_prefix_var();
    cstring_t new_phy_address = NULL;
    cstring_t new_phy_address_mask = NULL;
    amxc_string_t phys_mask_name;

    amxc_string_init(&phys_mask_name, 0);

    when_null_trace(prefix, exit, ERROR, "No prefix given");
    amxc_string_setf(&phys_mask_name, "%sPhysAddressMask", amxc_var_constcast(cstring_t, prefix));

    ac_obj = amxd_dm_signal_get_object(hosts_manager_get_dm(), event_data);
    info = (host_ac_info_t*) ac_obj->priv;
    when_null_trace(info, exit, ERROR, "No private info given for object");

    new_phy_address = amxd_object_get_value(cstring_t, ac_obj, "PhysAddress", NULL);
    new_phy_address_mask = amxd_object_get_value(cstring_t, ac_obj, amxc_string_get(&phys_mask_name, 0), NULL);

    if(!(amxd_object_get_value(bool, ac_obj, "CurrentAccess", NULL))) {
        host_ac_info_disable(info);
    }

    if(amxd_object_get_value(bool, ac_obj, "Enable", NULL)) {
        host_ac_info_enable(info, new_phy_address, new_phy_address_mask);
    }

    hosts_scheduler_update_ac_schedule(ac_obj);
exit:
    free(new_phy_address);
    free(new_phy_address_mask);
    amxc_string_clean(&phys_mask_name);
    return;
}

void _ac_event_origin_changed(UNUSED const char* const event_name,
                              const amxc_var_t* const event_data,
                              UNUSED void* const priv) {
    const char* origin = NULL;
    amxd_dm_t* dm = hosts_manager_get_dm();
    amxc_var_t* prefix = get_prefix_var();
    amxd_object_t* const ac_object = amxd_dm_signal_get_object(dm, event_data);
    amxc_string_t origin_name;

    amxc_string_init(&origin_name, 0);

    when_null_trace(ac_object, exit, ERROR, "No object given for access controller");
    when_null_trace(prefix, exit, ERROR, "No prefix given");

    amxc_string_setf(&origin_name, "parameters.%sOrigin.to", amxc_var_constcast(cstring_t, prefix));

    origin = GETP_CHAR(event_data, amxc_string_get(&origin_name, 0));

    when_str_empty_trace(origin, exit, ERROR, "No Origin change in Origin change call back");

    if(strcmp(origin, "User") != 0) {
        ac_object_flag(ac_object, "upc", false);
        ac_object_persistent_attr(ac_object, strcmp(origin, "Controller") == 0);
    } else {
        ac_object_flag(ac_object, "upc", true);
        ac_object_persistent_attr(ac_object, true);
    }
exit:
    amxc_string_clean(&origin_name);
    return;
}
