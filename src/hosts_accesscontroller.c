/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "hosts_manager.h"
#include "hosts_accesscontroller.h"
#include "hosts_accesscontroller_info.h"
#include "hosts_scheduler.h"
#include "gmap/gmap_devices.h"
#include "gmap/gmap_device.h"

#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace_macros.h>

#define ME "hosts-ac"

static int hosts_accesscontroller_update_in_gmap(const char* dev_key, bool allow) {
    int rv = -1;
    bool ok = false;

    when_null_trace(dev_key, exit, ERROR, "No device key given");

    if(allow) {
        ok = gmap_device_clearTag(dev_key, "wanblock", NULL, gmap_traverse_this);
    } else {
        ok = gmap_device_setTag(dev_key, "wanblock", NULL, gmap_traverse_this);
    }
    rv = ok? 0:1;

exit:
    return rv;
}

int hosts_accesscontrol_enforce(amxd_object_t* ac_obj, const char* dev_key, UNUSED const char* phys_addr) {
    int rv = 0;
    bool allow_access = false;
    bool use_schedule_ref = false;
    const cstring_t policy = NULL;
    amxc_var_t* config = hosts_manager_get_config();

    when_null_trace(ac_obj, exit, ERROR, "AccessControl for %s does not exist.", dev_key);

    policy = GET_CHAR(amxd_object_get_param_value(ac_obj, "AccessPolicy"), NULL);
    use_schedule_ref = !str_empty(GET_CHAR(amxd_object_get_param_value(ac_obj, "ScheduleRef"), NULL));

    if(!amxd_object_get_value(bool, ac_obj, "Enable", NULL)) {
        // If accesscontrol is disabled -> always allow
        allow_access = true;
    } else if(strcmp(policy, "Deny") == 0) {
        // When AccessPolicy is "Deny" -> always block
        allow_access = false;
    } else if(strcmp(policy, "Allow") == 0) {
        if(use_schedule_ref) {
            host_ac_info_t* info = NULL;

            allow_access = true;
            info = (host_ac_info_t*) ac_obj->priv;

            if(info->schedule_status != SCHEDULE_STATUS_ACTIVE) {
                allow_access = false;
            }
        } else {
            amxd_object_t* schedule_templ = amxd_object_findf(ac_obj, "Schedule.");
            // When AccessPolicy is "Allow"
            //    -> allow access when no schedules available
            //    -> block access when a schedule is available, unless schedule items are available
            allow_access = true;
            // currently only check if an schedule instance is available
            if(!GET_BOOL(config, "ignore-disabled")) {
                if(amxd_object_first_instance(schedule_templ) != NULL) {
                    allow_access = false;
                }
            } else {
                /* More logical would be to check if an enabled instance is
                 * available AND that we are currently out of all enabled instances
                 * windows. The code below implements that check */
                amxc_llist_t paths;
                amxc_llist_init(&paths);
                amxd_object_resolve_pathf(schedule_templ, &paths, "[Enable==true].");
                if(!amxc_llist_is_empty(&paths) && !hosts_scheduler_check_ac_schedule(ac_obj)) {
                    allow_access = false;
                }
                amxc_llist_clean(&paths, amxc_string_list_it_free);
            }
        }
    }

    // Allow or block access based on previously determined conditions
    if(allow_access) {
        rv = hosts_accesscontroller_allow_host(ac_obj, dev_key, false);
        SAH_TRACE_INFO("Allow Host key : %s - %i!", dev_key, rv);
    } else {
        rv = hosts_accesscontroller_block_host(ac_obj, dev_key);
        SAH_TRACE_INFO("Block Host key: %s - %i!", dev_key, rv);
    }

    if(!use_schedule_ref) {
        // this will activate or deactivate the schedules if needed.
        hosts_scheduler_update_ac_schedule(ac_obj);
    }

exit:
    return rv;
}

int hosts_accesscontroller_allow_host(amxd_object_t* ac_obj, const char* dev_key,
                                      bool destroy) {
    int rv = -1;

    if((ac_obj != NULL) && !destroy) {
        SAH_TRACEZ_INFO(ME, "Change CurrentAccess to true");
        rv = amxd_object_set_value(bool, ac_obj, "CurrentAccess", true);
        when_failed_trace(rv, exit, ERROR, "Allow host key '%s' failed: %i!", dev_key, rv);
    }

    SAH_TRACEZ_INFO(ME, "Allowing host key '%s'", dev_key);
    rv = hosts_accesscontroller_update_in_gmap(dev_key, true);
    when_failed_trace(rv, exit, ERROR, "Allow host key '%s' failed: %i!", dev_key, rv);
exit:
    return rv;
}

int hosts_accesscontroller_block_host(amxd_object_t* ac_obj, const char* dev_key) {
    int rv = -1;

    if(ac_obj != NULL) {
        SAH_TRACEZ_INFO(ME, "Change CurrentAccess to false");
        rv = amxd_object_set_value(bool, ac_obj, "CurrentAccess", false);
        when_failed_trace(rv, exit, ERROR, "Block host key '%s' failed: %i!", dev_key, rv);
    }

    SAH_TRACEZ_INFO(ME, "Blocking host key '%s'", dev_key);
    rv = hosts_accesscontroller_update_in_gmap(dev_key, false);
    when_failed_trace(rv, exit, ERROR, "Block host key '%s' failed: %i!", dev_key, rv);

exit:
    return rv;
}

int hosts_accesscontroller_start_interval_wrapper(UNUSED const char* function_name,
                                                  amxc_var_t* args,
                                                  UNUSED amxc_var_t* ret) {
    int retval = 0;
    amxd_dm_t* dm = hosts_manager_get_dm();
    const char* ac_alias = GET_CHAR(args, "AccessControlId");
    const char* schedule_id = GET_CHAR(args, "ScheduleId");
    amxd_object_t* ac_sched_obj = NULL;
    ac_sched_obj = amxd_dm_findf(dm, "Hosts.AccessControl.%s.Schedule.[Alias=='%s'].",
                                 ac_alias, schedule_id);

    if((ac_sched_obj != NULL) && hosts_scheduler_is_flex_time(ac_sched_obj)) {
        hosts_scheduler_flex_time_start_day(ac_sched_obj);
    } else {
        if(!hosts_scheduler_is_flex_active(ac_sched_obj)) {
            amxd_object_t* ac_obj = amxd_object_findf(ac_sched_obj, ".^.^");
            host_ac_info_t* info = NULL;
            when_null_trace(ac_obj, exit, ERROR, "No access controller link to schedule object");
            info = (host_ac_info_t*) ac_obj->priv;
            when_null_trace(info, exit, ERROR, "No private data from controller");
            amxc_var_for_each(entry, info->lan_devices_ht) {
                retval = hosts_accesscontroller_allow_host(ac_obj, amxc_var_key(entry), info->zombie);
                when_failed_trace(retval, exit, ERROR, "Cannot allow host %s", amxc_var_key(entry));
            }
        }
    }
exit:
    return retval;
}

int hosts_accesscontroller_end_interval_wrapper(UNUSED const char* function_name,
                                                amxc_var_t* args,
                                                UNUSED amxc_var_t* ret) {
    int retval = 0;
    amxd_dm_t* dm = hosts_manager_get_dm();
    const char* ac_alias = GET_CHAR(args, "AccessControlId");
    const char* schedule_id = GET_CHAR(args, "ScheduleId");
    amxd_object_t* ac_sched_obj = NULL;

    ac_sched_obj = amxd_dm_findf(dm, "Hosts.AccessControl.%s.Schedule.[Alias=='%s'].",
                                 ac_alias, schedule_id);
    if((ac_sched_obj != NULL) && hosts_scheduler_is_flex_time(ac_sched_obj)) {
        hosts_scheduler_flex_time_stop_day(ac_sched_obj);
    } else {
        if(!hosts_scheduler_is_flex_active(ac_sched_obj)) {
            amxd_object_t* ac_obj = amxd_object_findf(ac_sched_obj, ".^.^");
            host_ac_info_t* info = NULL;
            when_null_trace(ac_obj, exit, ERROR, "No access controller link to schedule object");
            info = (host_ac_info_t*) ac_obj->priv;
            when_null_trace(info, exit, ERROR, "No private data from controller");
            amxc_var_for_each(entry, info->lan_devices_ht) {
                retval = hosts_accesscontrol_enforce(ac_obj, amxc_var_key(entry), GET_CHAR(entry, NULL));
                when_failed_trace(retval, exit, ERROR, "Cannot block host %s", amxc_var_key(entry));
            }
        }
    }
exit:
    return retval;
}
