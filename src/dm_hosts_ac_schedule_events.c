/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "hosts_manager.h"
#include "dm_hosts_accesscontrol.h"
#include "hosts_accesscontroller.h"
#include "hosts_scheduler.h"
#include "hosts_accesscontroller_info.h"

#include <gmap/gmap.h>
#include <debug/sahtrace_macros.h>

#define ME "hosts-dm-ac"

static bool schedule_ref_used(amxd_object_t* ac_obj) {
    bool res = false;
    const char* schedule_ref = NULL;

    when_null(ac_obj, exit);

    schedule_ref = GET_CHAR(amxd_object_get_param_value(ac_obj, "ScheduleRef"), NULL);
    res = !str_empty(schedule_ref);

exit:
    return res;
}

static void schedule_updated_cb(schedule_status_t new_status, void* userdata) {
    host_ac_info_t* info = NULL;

    when_null(userdata, exit);
    info = (host_ac_info_t*) userdata;
    when_null(info, exit);

    if(info->schedule_status != new_status) {
        info->schedule_status = new_status;

        amxc_var_for_each(entry, info->lan_devices_ht) {
            hosts_accesscontrol_enforce(info->obj, amxc_var_key(entry), GET_CHAR(entry, NULL));
        }
    }

exit:
    return;
}

void _ac_event_schedule_added(UNUSED const cstring_t const event_name,
                              const amxc_var_t* const event_data,
                              UNUSED void* const priv) {
    const char* path = GET_CHAR(event_data, "path");
    uint32_t index = GET_INT32(event_data, "index");
    amxd_dm_t* dm = hosts_manager_get_dm();
    amxd_object_t* schedule_obj = amxd_dm_findf(dm, "%s%i.", path, index);
    amxd_object_t* ac_obj = amxd_object_findf(schedule_obj, ".^.^");
    host_ac_info_t* info = NULL;

    when_null_trace(ac_obj, exit, ERROR, "No access controller object given for schedule");
    when_null_trace(schedule_obj, exit, ERROR, "No schedule object found");

    info = (host_ac_info_t*) ac_obj->priv;
    when_null_trace(info, exit, ERROR, "No private data given for access control");
    when_null_trace(info->lan_devices_ht, exit, ERROR, "No devices attached to the access control object");
    when_true_trace(schedule_ref_used(ac_obj), exit, WARNING, "Not using Schedule since ScheduleRef is filled in");

    // if enabled block by default, timer will reverse if needed
    if(amxd_object_get_value(bool, schedule_obj, "Enable", NULL)) {
        amxc_var_for_each(entry, info->lan_devices_ht) {
            when_failed_trace(hosts_accesscontroller_block_host(ac_obj, amxc_var_key(entry)), exit, ERROR, "Cannot block host %s", amxc_var_key(entry));
        }
    }

    hosts_scheduler_add_ac_schedule(ac_obj, schedule_obj);
exit:
    return;
}

void _ac_event_scheduleref_changed(UNUSED const char* const event_name,
                                   const amxc_var_t* const event_data,
                                   UNUSED void* const priv) {
    const char* old_schedule_ref = NULL;
    const char* new_schedule_ref = NULL;
    amxd_object_t* ac_obj = amxd_dm_signal_get_object(hosts_manager_get_dm(), event_data);
    host_ac_info_t* info = NULL;

    when_null(event_data, exit);
    when_null(ac_obj, exit);
    old_schedule_ref = GETP_CHAR(event_data, "parameters.ScheduleRef.from");
    new_schedule_ref = GETP_CHAR(event_data, "parameters.ScheduleRef.to");

    info = (host_ac_info_t*) ac_obj->priv;
    when_null(info, exit);

    if(str_empty(old_schedule_ref) && !str_empty(new_schedule_ref)) {
        // Remove schedules
        amxd_object_for_each(instance, schedule_it, amxd_object_findf(ac_obj, ".Schedule.")) {
            amxd_object_t* schedule_obj = amxc_container_of(schedule_it, amxd_object_t, it);
            hosts_scheduler_remove_ac_schedule(ac_obj, GET_CHAR(amxd_object_get_param_value(schedule_obj, "Alias"), NULL));
        }

        // Start using ScheduleRef parameter
        info->schedule = schedule_open(new_schedule_ref, schedule_updated_cb, (void*) info);
    } else if(!str_empty(old_schedule_ref) && str_empty(new_schedule_ref)) {
        // Stop using ScheduleRef parameter
        schedule_close(&info->schedule);

        // Add schedules
        amxd_object_for_each(instance, schedule_it, amxd_object_findf(ac_obj, ".Schedule.")) {
            amxd_object_t* schedule_obj = amxc_container_of(schedule_it, amxd_object_t, it);
            const char* schedule_id = amxd_object_get_name(schedule_obj, AMXD_OBJECT_NAMED);

            if((info->tr != NULL) && (schedule_id != NULL) && (info->tr->schedule_id != NULL) && (strcmp(info->tr->schedule_id, schedule_id) == 0)) {
                SAH_TRACEZ_INFO(ME, "Current active flex time schedule changed for '%s'", schedule_id);
                amxp_timer_delete(&info->tr->timer);
                free(info->tr->schedule_id);
                free(info->tr);
                info->tr = NULL;
            }

            // if enabled block by default, timer will reverse if needed
            if(amxd_object_get_value(bool, schedule_obj, "Enable", NULL)) {
                amxc_var_for_each(entry, info->lan_devices_ht) {
                    when_failed_trace(hosts_accesscontroller_block_host(ac_obj, amxc_var_key(entry)), exit, ERROR, "Cannot block host %s", amxc_var_key(entry));
                }
            }

            hosts_scheduler_add_ac_schedule(ac_obj, schedule_obj);
        }

    } else {
        schedule_close(&(info->schedule));
        info->schedule = schedule_open(new_schedule_ref, schedule_updated_cb, (void*) info);
    }

exit:
    return;
}

void _ac_event_schedule_changed(UNUSED const cstring_t const event_name,
                                const amxc_var_t* const event_data,
                                UNUSED void* const priv) {
    amxd_object_t* schedule_obj = amxd_dm_signal_get_object(hosts_manager_get_dm(), event_data);
    amxd_object_t* ac_obj = amxd_object_findf(schedule_obj, ".^.^");
    host_ac_info_t* info = NULL;
    time_tracker_t* tr = NULL;
    const char* schedule_id = amxd_object_get_name(schedule_obj, AMXD_OBJECT_NAMED);

    when_null(ac_obj, exit);
    when_null(schedule_obj, exit);

    info = (host_ac_info_t*) ac_obj->priv;
    when_null(info, exit);
    when_true_trace(schedule_ref_used(ac_obj), exit, WARNING, "Not using Schedule since ScheduleRef is filled in");
    tr = info->tr;

    // When the duration is changed of the current active flex time schedule
    // it will not be seen as a change by the schedule module as the start time is empty
    // it will take a full day as the duration. To be sure that the schedule is correctly
    // adapted, it will be removed and re-added.
    // Should only be done when the duration changes, but for code simplicity it is
    // easier to just remove the schedule and re-add it.
    if((tr != NULL) && (schedule_id != NULL) && (tr->schedule_id != NULL) && (strcmp(tr->schedule_id, schedule_id) == 0)) {
        SAH_TRACEZ_INFO(ME, "Current active flex time schedule changed for '%s'", schedule_id);
        amxp_timer_delete(&tr->timer);
        free(tr->schedule_id);
        free(tr);
        info->tr = NULL;

        hosts_scheduler_remove_ac_schedule(ac_obj, schedule_id);
        hosts_scheduler_add_ac_schedule(ac_obj, schedule_obj);
    } else {
        hosts_scheduler_change_ac_schedule(ac_obj, schedule_obj);
        amxc_var_for_each(entry, info->lan_devices_ht) {
            hosts_accesscontrol_enforce(info->obj, amxc_var_key(entry), GET_CHAR(entry, NULL));
        }
    }

exit:
    return;
}

void _ac_event_schedule_removed(UNUSED const cstring_t const event_name,
                                const amxc_var_t* const event_data,
                                UNUSED void* const priv) {
    amxd_object_t* schedule_obj_templ = amxd_dm_signal_get_object(hosts_manager_get_dm(), event_data);
    amxd_object_t* ac_obj = amxd_object_findf(schedule_obj_templ, ".^");
    const cstring_t schedule_id = GETP_CHAR(event_data, "parameters.Alias");
    host_ac_info_t* info = NULL;

    when_null(ac_obj, exit);
    info = (host_ac_info_t*) ac_obj->priv;
    when_null(info, exit);
    when_null(info->lan_devices_ht, exit);
    when_true_trace(schedule_ref_used(ac_obj), exit, WARNING, "Not using Schedule since ScheduleRef is filled in");

    hosts_scheduler_remove_ac_schedule(ac_obj, schedule_id);

    amxc_var_for_each(entry, info->lan_devices_ht) {
        hosts_accesscontrol_enforce(info->obj, amxc_var_key(entry), GET_CHAR(entry, NULL));
    }

exit:
    return;
}
