/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "hosts_manager.h"
#include "hosts_sync.h"
#include "hosts_accesscontroller.h"

#include <netmodel/common_api.h>
#include "netmodel/client.h"

#include <gmap/gmap.h>
#include <stdlib.h>
#include <string.h>

#define ME "hosts-sync"

#define HOSTS_QUERY_EXPRESSION "lan and not self and physical and .Master==\"\""

typedef struct hosts_sync_info {
    char* device_path;           // Named path to the Devices.Device. instance
    char* host_path;             // Index path to the Hosts.Host instance
    int host_index;              // Index of the Hosts.Host. instance
    amxs_sync_ctx_t* ctx;        // Synchronization context
    netmodel_query_t* query;     // NetModel query to find the layer 3 interface
    amxc_llist_it_t it;          // Linked list iterator
} hosts_sync_info_t;

typedef struct _hosts_gmap_handles {
    amxd_dm_t* hosts_dm;            /** the gmap_eth_dev data model object */
    amxo_parser_t* parser;          /** the odl parser */
    amxb_bus_ctx_t* ctx;
} _hosts_gmap_handles_t;

static _hosts_gmap_handles_t hosts_gmap_handles;
static amxc_llist_t sync_info;
static gmap_query_t* query = NULL;
static amxp_timer_t* retry_timer = NULL;

/**
 *  Get the corresponding sync information entry for the object
 */
static hosts_sync_info_t* hosts_get_sync_info(const cstring_t path, bool host) {
    hosts_sync_info_t* retv = NULL;
    hosts_sync_info_t* current = NULL;

    amxc_llist_for_each(it, &sync_info) {
        current = amxc_container_of(it, hosts_sync_info_t, it);
        const char* current_path = host ? current->host_path : current->device_path;
        if(strcmp(current_path, path) == 0) {
            retv = current;
            break;
        }
    }
    return retv;
}

static int hosts_delete_object(int index) {
    amxd_trans_t trans;
    int rv = 0;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    amxd_trans_select_pathf(&trans, "Hosts.Host.");
    amxd_trans_del_inst(&trans, index, NULL);

    rv = amxd_trans_apply(&trans, hosts_gmap_handles.hosts_dm);
    when_failed_trace(rv, exit, ERROR, "Problem applying data model transaction - %i for host %d", rv, index);

exit:
    amxd_trans_clean(&trans);
    return rv;
}

static int hosts_create_object(amxc_string_t* aliaspath, amxc_string_t* device_path) {
    int rv = 0;
    int host_index = 0;
    amxd_trans_t trans;
    amxd_object_t* hostobject = NULL;
    const char* physaddr = NULL;
    amxb_bus_ctx_t* ctx = NULL;
    amxc_var_t ret;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxc_var_init(&ret);

    /* 1. If there is already an existing object with DeviceReference equal to
     * aliaspath return its index */
    hostobject = amxd_dm_findf(hosts_gmap_handles.hosts_dm, "Hosts.Host.[DeviceReference=='%s']", amxc_string_get(aliaspath, 0));
    when_not_null_status(hostobject, exit, host_index = amxd_object_get_index(hostobject));

    /* 2. Add it if not there */
    ctx = amxb_be_who_has("Devices.Device.");
    when_null_trace(ctx, exit, ERROR, "No context found for Devices.Device.");
    rv = amxb_get(ctx, amxc_string_get(device_path, 0), 0, &ret, 5);
    when_failed_trace(rv, exit, ERROR, "Cannot get MAC address %s", amxc_string_get(device_path, 0));
    physaddr = GETP_CHAR(&ret, "0.0.PhysAddress");
    when_str_empty_trace(physaddr, exit, ERROR, "Fetched MAC address is empty : %s", amxc_string_get(device_path, 0));
    amxd_trans_select_pathf(&trans, "Hosts.Host.");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(cstring_t, &trans, "DeviceReference", amxc_string_get(aliaspath, 0));
    amxd_trans_set_value(cstring_t, &trans, "PhysAddress", physaddr);

    rv = amxd_trans_apply(&trans, hosts_gmap_handles.hosts_dm);
    when_failed_trace(rv, exit, ERROR, "Problem applying data model transaction - %i", rv);

    hostobject = amxd_dm_findf(hosts_gmap_handles.hosts_dm, "Hosts.Host.[DeviceReference=='%s']", amxc_string_get(aliaspath, 0));
    when_null_trace(hostobject, exit, ERROR, "Problem amxd_dm_findf ");

    host_index = amxd_object_get_index(hostobject);
exit:
    amxd_trans_clean(&trans);
    amxc_var_clean(&ret);
    return host_index;
}

static void delete_item(amxc_llist_it_t* it) {
    hosts_sync_info_t* info = amxc_llist_it_get_data(it, hosts_sync_info_t, it);
    if(info != NULL) {
        amxs_sync_ctx_stop_sync(info->ctx);
        amxs_sync_ctx_delete(&info->ctx);
        netmodel_closeQuery(info->query);
        hosts_delete_object(info->host_index);
        free(info->device_path);
        free(info->host_path);
        free(info);
        SAH_TRACEZ_INFO(ME, "Removed a Host");
    }
}

static amxs_status_t sync_param_action_cb(UNUSED const amxs_sync_entry_t* entry, UNUSED amxs_sync_direction_t direction, amxc_var_t* data, UNUSED void* priv) {
    amxd_trans_t trans;
    amxs_status_t status = amxs_status_unknown_error;
    int rv = 0;

    const cstring_t path = GET_CHAR(data, "path");
    amxc_var_t* params = amxc_var_get_first(GET_ARG(data, "parameters"));

    amxd_trans_init(&trans);
    when_null(path, exit);
    when_null(params, exit);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_pathf(&trans, "%s", path);
    amxd_trans_set_param(&trans, amxc_var_key(params), params);

    rv = amxd_trans_apply(&trans, hosts_gmap_handles.hosts_dm);
    when_failed_trace(rv, exit, ERROR, "Problem applying data model transaction - %i %s %s", rv, path, amxc_var_key(params));
    status = amxs_status_ok;
exit:
    amxd_trans_clean(&trans);

    return status;
}

static bool check_tag(const cstring_t tag_list, const cstring_t tag) {
    amxc_var_t ssv_tags;
    bool ret = false;

    amxc_var_init(&ssv_tags);
    amxc_var_set(ssv_string_t, &ssv_tags, tag_list);
    amxc_var_cast(&ssv_tags, AMXC_VAR_ID_LIST);

    amxc_var_for_each(it, &ssv_tags) {
        if(!strcmp(GET_CHAR(it, NULL), tag)) {
            ret = true;
            break;
        }
    }
    amxc_var_clean(&ssv_tags);

    return ret;
}

static amxs_status_t sync_param_action_cb_protected(UNUSED const amxs_sync_entry_t* entry,
                                                    UNUSED amxs_sync_direction_t direction,
                                                    amxc_var_t* data, UNUSED void* priv) {
    amxs_status_t status = amxs_status_unknown_error;
    amxd_trans_t trans;
    amxc_var_t res;
    amxc_string_t protected;
    int rv;
    const cstring_t hostpath = GET_CHAR(data, "path");
    amxc_var_t* params = amxc_var_get_first(GET_ARG(data, "parameters"));
    const cstring_t tmp = GET_CHAR(params, NULL);
    const char* prefix = GET_CHAR(amxo_parser_get_config(hosts_manager_get_parser(), "prefix_"), NULL);

    amxc_string_init(&protected, 0);
    amxc_string_setf(&protected, "%sProtected", prefix);

    amxc_var_init(&res);
    amxc_var_set(bool, &res, check_tag(tmp, "protected"));

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_pathf(&trans, "%s", hostpath);
    amxd_trans_set_param(&trans, amxc_string_get(&protected, 0), &res);

    rv = amxd_trans_apply(&trans, hosts_manager_get_dm());
    when_failed_trace(rv, exit, ERROR, "Problem applying data model transaction - %i", rv);
    status = amxs_status_ok;
exit:
    amxc_var_clean(&res);
    amxd_trans_clean(&trans);
    amxc_string_clean(&protected);

    return status;
}

static amxs_status_t sync_param_action_cb_active (UNUSED const amxs_sync_entry_t* entry, UNUSED amxs_sync_direction_t direction, amxc_var_t* data, UNUSED void* priv) {
    amxd_trans_t trans;
    amxs_status_t status = amxs_status_unknown_error;
    int rv = 0;
    amxc_ts_t now;
    amxc_ts_now(&now);

    const cstring_t path = GET_CHAR(data, "path");
    amxc_var_t* params = amxc_var_get_first(GET_ARG(data, "parameters"));

    when_null(path, exit);
    when_null(params, exit);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_pathf(&trans, "%s", path);
    amxd_trans_set_param(&trans, amxc_var_key(params), params);
    amxd_trans_set_value(amxc_ts_t, &trans, "ActiveLastChange", &now);

    rv = amxd_trans_apply(&trans, hosts_gmap_handles.hosts_dm);
    when_failed_trace(rv, exit, ERROR, "Problem applying data model transaction - %i", rv);
    status = amxs_status_ok;

exit:
    amxd_trans_clean(&trans);
    return status;
}

static const char* convert_path_to_interface_type(const char* path) {
    const char* ret = "Other";
    amxc_string_t str;
    amxc_llist_t list;
    const char* type = NULL;

    amxc_string_init(&str, 0);
    amxc_llist_init(&list);

    when_str_empty_trace(path, exit, ERROR, "Path is empty");

    amxc_string_set(&str, path);
    amxc_string_split_to_llist(&str, &list, '.');
    type = amxc_string_get_text_from_llist(&list, 1);
    when_str_empty_trace(type, exit, ERROR, "Invalid type string found");

    if(strcmp(type, "Ethernet") == 0) {
        ret = "Ethernet";
    } else if(strcmp(type, "WiFi") == 0) {
        ret = "Wi-Fi";
    } else if(strcmp(type, "MoCA") == 0) {
        ret = "MoCA";
    }

exit:
    amxc_string_clean(&str);
    amxc_llist_clean(&list, amxc_string_list_it_free);
    return ret;
}

static void layer3_cb(UNUSED const char* sig_name, const amxc_var_t* data, void* priv) {
    const char* layer3 = GET_CHAR(data, NULL);
    amxd_trans_t trans;
    hosts_sync_info_t* info = (hosts_sync_info_t*) priv;
    int ret = 0;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_null_trace(info, exit, ERROR, "Info pointer is NULL");
    when_null(layer3, exit);

    amxd_trans_select_pathf(&trans, "%s", info->host_path);
    amxd_trans_set_value(cstring_t, &trans, "Layer3Interface", layer3);

    ret = amxd_trans_apply(&trans, hosts_manager_get_dm());
    when_failed_trace(ret, exit, ERROR, "Failed to apply transaction: %d", ret);

exit:
    amxd_trans_clean(&trans);
    return;
}

static amxs_status_t sync_param_action_cb_layer1(UNUSED const amxs_sync_entry_t* entry,
                                                 UNUSED amxs_sync_direction_t direction,
                                                 amxc_var_t* data,
                                                 UNUSED void* priv) {
    amxs_status_t status = amxs_status_unknown_error;
    amxd_trans_t trans;
    int ret = -1;
    const char* type = NULL;
    const char* layer1 = GETP_CHAR(data, "parameters.Layer1Interface");
    const char* path = GET_CHAR(data, "path");
    hosts_sync_info_t* info = NULL;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_null_trace(layer1, exit, ERROR, "Layer1Interface information is missing");
    when_str_empty_trace(path, exit, ERROR, "Path information is missing");

    type = convert_path_to_interface_type(layer1);

    info = hosts_get_sync_info(path, true);
    when_null_trace(info, exit, ERROR, "Failed to get sync info for path %s", path);
    if(info->query != NULL) {
        netmodel_closeQuery(info->query);
        info->query = NULL;
    }
    info->query = netmodel_openQuery_getFirstParameter(layer1, "hosts", "InterfacePath", "ip",
                                                       netmodel_traverse_up, layer3_cb, info);
    when_null_trace(info->query, exit, ERROR, "Failed to open netmodel query to get layer 3 interface");

    amxd_trans_select_pathf(&trans, "%s", path);
    amxd_trans_set_value(cstring_t, &trans, "Layer1Interface", layer1);
    amxd_trans_set_value(cstring_t, &trans, "InterfaceType", type);

    ret = amxd_trans_apply(&trans, hosts_manager_get_dm());
    when_failed_trace(ret, exit, ERROR, "Failed to apply transaction: %d", ret);

    status = amxs_status_ok;

exit:
    amxd_trans_clean(&trans);
    return status;
}

static amxs_status_t sync_param_action_cb_dhcpclient (UNUSED const amxs_sync_entry_t* entry, UNUSED amxs_sync_direction_t direction, amxc_var_t* data, UNUSED void* priv) {
    amxs_status_t status = amxs_status_unknown_error;
    int rv = 0;
    amxc_var_t dhcpclients;
    amxc_var_t device;
    amxc_string_t csv_dhcpclients;
    const cstring_t dhcpclient = NULL;
    const cstring_t dhcpv6client = NULL;
    amxd_trans_t trans;

    const cstring_t hostpath = GET_CHAR(data, "path");
    const cstring_t devicepath = GET_CHAR(data, "opposite_path");
    amxc_var_t* params = amxc_var_get_first(GET_ARG(data, "parameters"));

    amxc_string_init(&csv_dhcpclients, 0);
    amxc_var_init(&dhcpclients);
    amxc_var_init(&device);
    amxd_trans_init(&trans);
    when_null(hostpath, exit);
    when_null(devicepath, exit);
    when_null(params, exit);

    amxc_var_set_type(&dhcpclients, AMXC_VAR_ID_LIST);

    amxb_get(hosts_gmap_handles.ctx, devicepath, 0, &device, 1);

    dhcpclient = GETP_CHAR(&device, "0.0.DHCPv4Client");
    dhcpv6client = GETP_CHAR(&device, "0.0.DHCPv6Client");

    if(!str_empty(dhcpclient)) {
        amxc_var_add(cstring_t, &dhcpclients, dhcpclient);
    }
    if(!str_empty(dhcpv6client)) {
        amxc_var_add(cstring_t, &dhcpclients, dhcpv6client);
    }

    rv = amxc_string_csv_join_var(&csv_dhcpclients, &dhcpclients);
    when_failed_trace(rv, exit, ERROR, "Problem amxc_string_csv_join_var - %i", rv);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_pathf(&trans, "%s", hostpath);

    amxd_trans_set_value(csv_string_t, &trans, amxc_var_key(params), amxc_string_get(&csv_dhcpclients, 0));

    rv = amxd_trans_apply(&trans, hosts_gmap_handles.hosts_dm);
    when_failed_trace(rv, exit, ERROR, "Problem applying data model transaction - %i", rv);
    status = amxs_status_ok;

exit:
    amxd_trans_clean(&trans);
    amxc_string_clean(&csv_dhcpclients);
    amxc_var_clean(&dhcpclients);
    amxc_var_clean(&device);
    return status;
}

static amxs_status_t sync_param_trans_cb(const amxs_sync_entry_t* entry,
                                         amxs_sync_direction_t direction,
                                         const amxc_var_t* input,
                                         amxc_var_t* output,
                                         UNUSED void* priv) {
    amxs_status_t status = amxs_sync_param_copy_trans_cb(entry, direction, input, output, priv);
    const char* path = GET_CHAR(input, "path");
    amxc_var_add_key(cstring_t, output, "opposite_path", path);

    return status;
}

static int hosts_get_device_index(amxc_string_t* aliaspath) {
    int index = 0;
    amxc_var_t* object_var = NULL;
    amxc_var_t result;

    amxc_var_init(&result);
    amxb_describe(hosts_gmap_handles.ctx, amxc_string_get(aliaspath, 0), AMXB_FLAG_OBJECTS, &result, 1);

    object_var = GETI_ARG(&result, 0);
    when_null_trace(object_var, exit, ERROR, "Problem exporting Device Variant %s", amxc_string_get(aliaspath, 0));
    index = GET_INT32(object_var, "index");
exit:
    amxc_var_clean(&result);
    return index;
}

static void hosts_set_sync_parameters(amxs_sync_ctx_t* ctx) {
    amxc_string_t protected;
    amxs_sync_object_t* obj = NULL;
    const char* prefix = GET_CHAR(amxo_parser_get_config(hosts_manager_get_parser(), "prefix_"), NULL);

    amxc_string_init(&protected, 0);
    amxc_string_setf(&protected, "%sProtected", prefix);

    amxs_sync_ctx_add_new_param(ctx, "Name", "HostName", AMXS_SYNC_DEFAULT, amxs_sync_param_copy_trans_cb, sync_param_action_cb, NULL);
    amxs_sync_ctx_add_new_param(ctx, "IPAddress", "IPAddress", AMXS_SYNC_DEFAULT, amxs_sync_param_copy_trans_cb, sync_param_action_cb, NULL);

    amxs_sync_ctx_add_new_param(ctx, "Active", "Active", AMXS_SYNC_DEFAULT, amxs_sync_param_copy_trans_cb, sync_param_action_cb_active, NULL);
    amxs_sync_ctx_add_new_param(ctx, "Layer1Interface", "Layer1Interface", AMXS_SYNC_DEFAULT, amxs_sync_param_copy_trans_cb, sync_param_action_cb_layer1, NULL);

    amxs_sync_ctx_add_new_param(ctx, "DHCPv4Client", "DHCPClient", AMXS_SYNC_DEFAULT, sync_param_trans_cb, sync_param_action_cb_dhcpclient, NULL);
    amxs_sync_ctx_add_new_param(ctx, "DHCPv6Client", "DHCPClient", AMXS_SYNC_DEFAULT, sync_param_trans_cb, sync_param_action_cb_dhcpclient, NULL);

    amxs_sync_ctx_add_new_param(ctx, "Tags", amxc_string_get(&protected, 0), AMXS_SYNC_DEFAULT,
                                amxs_sync_param_copy_trans_cb, sync_param_action_cb_protected, NULL);

    amxs_sync_object_new_copy(&obj, "IPv4Address.", "IPv4Address.", AMXS_SYNC_DEFAULT);
    amxs_sync_object_add_new_copy_param(obj, "Address", "IPAddress", AMXS_SYNC_DEFAULT);
    amxs_sync_ctx_add_object(ctx, obj);

    amxs_sync_object_new_copy(&obj, "IPv6Address.", "IPv6Address.", AMXS_SYNC_DEFAULT);
    amxs_sync_object_add_new_copy_param(obj, "Address", "IPAddress", AMXS_SYNC_DEFAULT);
    amxs_sync_ctx_add_object(ctx, obj);

    amxs_sync_object_new_copy(&obj, "SSWSta.", ".", AMXS_SYNC_DEFAULT);
    amxs_sync_object_add_new_param(obj, "AssociatedDevice", "AssociatedDevice", AMXS_SYNC_DEFAULT, amxs_sync_param_copy_trans_cb, sync_param_action_cb, NULL);
    amxs_sync_ctx_add_object(ctx, obj);

    amxc_string_clean(&protected);
}

/*                      *** TEMPORARY WORKAROUND ***
 ****************************
    START A TIMER TO TRY TO OPEN A GMAP QUERY.
    IT CAN HAPPEN THAT DURING BOOT GMAP-SERVER IS NOT VERY RESPONSIVE

    THIS FUNCTION WILL TRY TO OPEN A GMAP QUERY
    WHEN IT FAILS THE TIMER IS RESTARTED
    THE TIMEOUT WILL BE DOUBLED AS LONG AS IT IS BELOW 20 SECONDS

    A GOOD/BETTER SOLUTION WOULD BE THAT LIBGAMP-CLIENT PROVIDES AN ASYNCHRONOUS
    IMPLEMENTATION OF THE OPEN QUERY FUNCTION.

 */
static void hosts_try_open_gmap_query(UNUSED amxp_timer_t* timer, UNUSED void* priv) {
    static int timeout = 1000;
    when_not_null(query, exit);

    gmap_query_open_ext_2(&query, HOSTS_QUERY_EXPRESSION, "Hosts",
                          GMAP_QUERY_IGNORE_DEVICE_UPDATED, hosts_query_cb, NULL);
    if(query == NULL) {
        if(timeout < 20000) {
            timeout = timeout * 2;
        }
        amxp_timer_start(retry_timer, timeout);
    }

exit:
    return;
}

int hosts_sync_init(amxd_dm_t* dm, amxo_parser_t* parser) {
    int rv = -1;
    hosts_gmap_handles.ctx = NULL;

    hosts_gmap_handles.hosts_dm = dm;
    hosts_gmap_handles.parser = parser;

    rv = amxc_llist_init(&sync_info);
    when_failed_trace(rv, exit, ERROR, "Problem Initializing Hosts sync_info - %i", rv);

    rv = -1;
    when_false_trace(netmodel_initialize(), exit, ERROR, "Failed to initialize libnetmodel");

    hosts_gmap_handles.ctx = amxb_be_who_has("Devices.Device");
    when_null_trace(hosts_gmap_handles.ctx, exit, ERROR, "gMap data model not found 'Devices.Device' - is gmap-server running?");

    gmap_client_init(hosts_gmap_handles.ctx);
    gmap_query_open_ext_2(&query, HOSTS_QUERY_EXPRESSION, "Hosts", GMAP_QUERY_IGNORE_DEVICE_UPDATED, hosts_query_cb, NULL);
    /* *** TEMPORARY WORKAROUND - SEE FUNCTION ABOVE THIS ONE *** */
    if(query == NULL) {
        amxp_timer_new(&retry_timer, hosts_try_open_gmap_query, NULL);
        amxp_timer_start(retry_timer, 10000);
    }

    rv = 0;

exit:
    return rv;
}

int hosts_sync_cleanup(void) {
    gmap_query_close(query);
    netmodel_cleanup();
    amxc_llist_clean(&sync_info, delete_item);
    amxp_timer_delete(&retry_timer);
    return 0;
}

void hosts_query_cb(UNUSED gmap_query_t* lquery, const cstring_t key, UNUSED amxc_var_t* device, gmap_query_action_t action) {
    int rv = 0;
    amxc_string_t aliaspath;

    amxc_string_init(&aliaspath, 0);
    amxc_string_setf(&aliaspath, "Devices.Device.%s.", key);

    if(action == gmap_query_expression_start_matching) {
        rv = hosts_setup_object_sync(amxc_string_get(&aliaspath, 0));
        when_failed_trace(rv, exit, ERROR, "Problem Setting up object sync  - %i", rv);
    } else if(action == gmap_query_expression_stop_matching) {
        rv = hosts_destroy_object_sync(amxc_string_get(&aliaspath, 0));
        when_failed_trace(rv, exit, ERROR, "Problem removing object sync  - %i", rv);
    }
exit:
    amxc_string_clean(&aliaspath);
    return;
}

int hosts_setup_object_sync(const cstring_t path) {
    int rv = -1;
    int host_index = 0;
    int index = 0;
    amxc_string_t indexpath;
    amxc_string_t hostindexpath;
    amxc_string_t aliaspath;
    amxs_sync_ctx_t* ctx = NULL;
    hosts_sync_info_t* info = NULL;

    amxc_string_init(&indexpath, 0);
    amxc_string_init(&hostindexpath, 0);
    amxc_string_init(&aliaspath, 0);

    info = (hosts_sync_info_t*) calloc(1, sizeof(hosts_sync_info_t));
    when_null_trace(info, exit, ERROR, "Calloc hosts_sync_info_t failed");

    amxc_string_setf(&aliaspath, "%s", path);
    index = hosts_get_device_index(&aliaspath);
    when_false_trace((index != 0), clean, ERROR, "Problem hosts_get_device_index - Index: %i", index);
    amxc_string_setf(&indexpath, "Devices.Device.%i.", index);
    host_index = hosts_create_object(&aliaspath, &indexpath);
    when_false_trace((host_index != 0), clean, ERROR, "Problem hosts_create_object - Host Index: %i", host_index);

    amxc_string_setf(&hostindexpath, "Hosts.Host.%i.", host_index);

    info->device_path = amxc_string_take_buffer(&aliaspath);
    info->host_path = amxc_string_take_buffer(&hostindexpath);
    info->host_index = host_index;
    rv = amxc_llist_append(&sync_info, &info->it);
    when_failed_trace(rv, clean, ERROR, "Problem amxc_llist_append - %i", rv);

    amxs_sync_ctx_new(&ctx, amxc_string_get(&indexpath, 0), info->host_path, AMXS_SYNC_ONLY_A_TO_B);
    amxs_sync_ctx_set_local_dm(ctx, NULL, hosts_manager_get_dm());
    when_null_trace(ctx, clean, ERROR, "Problem creating Sync");
    hosts_set_sync_parameters(ctx);
    rv = amxs_sync_ctx_start_sync(ctx);
    when_failed_trace(rv, clean, ERROR, "Problem starting Hosts sync");
    info->ctx = ctx;

    SAH_TRACEZ_INFO(ME, "Found new Host");
    goto exit;

clean:
    if(info != NULL) {
        free(info->device_path);
        free(info->host_path);
        amxc_llist_it_take(&info->it);
    }
    free(info);
    amxs_sync_ctx_delete(&ctx);
exit:
    amxc_string_clean(&indexpath);
    amxc_string_clean(&hostindexpath);
    amxc_string_clean(&aliaspath);
    return rv;
}

int hosts_destroy_object_sync(const cstring_t path) {
    hosts_sync_info_t* info = hosts_get_sync_info(path, false);
    if(info != NULL) {
        amxc_llist_it_clean(&info->it, delete_item);
        return 0;
    }
    return -1;
}
